<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid" ng-controller="GaleriaMultimediaController">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center titulo-dashboard">
                <h3 class="text-themecolor">Configuración</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Configuración</li>
                     <li class="breadcrumb-item active">Galería</li>
                </ol>
            </div>
            
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
         <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{titulo_pagina}}</h4>
                        <h6 class="card-subtitle">{{subtitulo_pagina}}</h6>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                                    <label>Título de la imagen <span class="help"></span></label>
                                    <input name="categorias_descripcion" id="categorias_descripcion" type="text" class="form-control form-control-line" placeholder="Ingrese el título de la imagen" ng-model="galeria.descripcion" required>  
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <label>Categoria <span class="help"></span></label>
                                    <select name="categorias" id="categorias" class="form-control form-control-line" ng-options="option.descripcion for option in categorias track by option.id" ng-model="galeria.categoria" required>
                                        <option value="">--Seleccione una categoria--</option>
                                    </select> 
                                </div>
                                
                                  
                            </div>
                            <div class="row">
                                <!-- -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                                    <!--<label>Título de la imagen <span class="help"></span></label>
                                    <input name="categorias_descripcion" id="categorias_descripcion" type="text" class="form-control form-control-line" placeholder="Ingrese la descripción de la categoría" ng-model="galeria.descripcion" required> --> 
                                    <label> <span class="help"></span></label>
                                    <div id="img-zone">
                                        <div class="form-group">
                                            <input id ="file" name="file" type="file" uploader-model="file" style="display:none">
                                            <!--  uploader-model="file"  ng-file-model="file"-->
                                            <label for="file" style="width: 100%">  
                                                <div id="subir_imagenes" class="col-lg-12 btn btn-block btn-info"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Subir imagen</div>
                                            </label>
                                            <div class="img_load" >
                                                <div id="list" style="padding-bottom:2px;">
                                                    <img id="previa" class="img_preview img-responsive" src="<?=base_url();?>assets/images/precharge.png">
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                                <!-- --> 
                            </div>
                        </div>
                        <div id="div_mensaje"></div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <button id="btn-limpiar" type="button" class="btn waves-effect waves-light btn-block btn-success" ng-click="limpiar_imagen()">Limpiar</button>
                            </div>

                            <div class="col-lg-4 col-md-4">
                                <a href="<?=base_url();?>GaleriaMultimedia/consultarGaleria">
                                    <button id="btn-consultar" type="button" class="btn waves-effect waves-light btn-block btn-danger" >Consultar</button>
                                </a>
                            </div>
                           
                            <div class="col-lg-4 col-md-4">
                               <button id="btn-registrar" type="button" class="btn waves-effect waves-light btn-block btn-info" ng-click="registrarGaleria()">Guardar</button>
                            </div>
                        </div>
                        <!-- ============================================== -->
                    </div>
                </div>
            </div>
        </div>                 
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
</div> 
 <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">       
 <script type="text/javascript">
function archivo(evt) {
    var files = evt.target.files; // FileList object
    
        //Obtenemos la imagen del campo "file". 
    for (var i = 0, f; f = files[i]; i++) {     
         //Solo admitimos imágenes.
         if (!f.type.match('image.*')) {
            continue;
         }
    
         var reader = new FileReader();
        
         reader.onload = (function(theFile) {
           return function(e) {
           // Creamos la imagen.
                  //document.getElementById("list").innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
           
            $("#list").css("display","block");
            $("#list").html(['<img id="previa" class="img_preview img-responsive" src="<?=base_url();?>assets/images/precharge.png" style="display:none"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 divbiblioteca"><img class="imgbiblioteca_principal" style="width:100%;padding-top:15px;" src="', e.target.result,'" title="', escape(theFile.name), '"/></div>'].join('')).fadeIn("slow");
           };
            $("#previa").css("display","none");
         })(f);

         reader.readAsDataURL(f);
       }
}
        
    document.getElementById('file').addEventListener('change', archivo, false);
</script>