<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid" ng-controller="GaleriaMultimediaConsultasController">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center titulo-dashboard">
                <h3 class="text-themecolor">Configuración</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Configuración</li>
                    <li class="breadcrumb-item active">Galería</li>
                    <li class="breadcrumb-item active">Consultar</li>
                </ol>
            </div>
            <div class="col-md-7 col-4 align-self-center">
                <div class="d-flex m-t-10 justify-content-end">
                    <!--
                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i
                                class="ti-settings text-white"></i></button>
                    </div>
                    -->
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{titulo_pagina}}</h4>
                        <h6 class="card-subtitle">{{subtitulo_pagina}}</h6>
                        <!-- ============================================== -->
                        <div class="col-lg-12 mg-b30">
                            <a href="{{base_url}}GaleriaMultimedia">
                                <div class="btn btn-success btn-new">Nuevo</div>
                            </a>
                        </div>
                        <div class="row m-t-40">
                            <div class="col-md-12">
                                <h4 class="card-title">{{titulo_seccion}} {{limite}}</h4>
                                <h6 class="card-subtitle m-b-20 text-muted">{{galeria_img.nombre_categoria}}</h6>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                                    <input name="categorias_descripcion" id="categorias_descripcion" type="text"
                                        class="form-control form-control-line"
                                        placeholder="Ingrese la descripción de la imagen"
                                        ng-model="galeria_filtros.descripcion" ng-change="consultarFiltrosGaleria()">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <select name="categorias" id="categorias" class="form-control form-control-line"
                                        ng-options="option.descripcion for option in categorias track by option.id"
                                        ng-model="galeria_filtros.categoria" ng-change="consultarFiltrosGaleria()">
                                        <option value="">--Seleccione una categoria--</option>
                                    </select>
                                </div>

                            </div><br>
                            <div class="row">

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <select id="limite" name="categorias" id="categorias"
                                        class="form-control form-control-line" ng-model="limite"
                                        ng-click="cambiarLimite()">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="30">30</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    {{desde}} - {{hasta}} de {{galeria.length}}
                                    <button class="btn btn-success btn-galeria1" ng-disabled="currentPage == 0"
                                        ng-click="currentPage=0;desde=1;CalcularHasta();hasta=pageSize">
                                        Inicio
                                    </button>
                                    <button class="btn btn-success btn-galeria1" ng-disabled="currentPage == 0"
                                        ng-click="currentPage=currentPage-1;desde=desde-pageSize;hasta=hasta-pageSize;CalcularHasta()">
                                        Anterior
                                    </button>
                                    <button class="btn btn-success btn-galeria2"
                                        ng-disabled="currentPage >= galeria.length/pageSize - 1"
                                        ng-click="currentPage=currentPage+1; desde=desde+pageSize;hasta=hasta+pageSize; CalcularHasta()">
                                        Siguiente
                                    </button>
                                    <button class="btn btn-success btn-galeria2"
                                        ng-disabled="currentPage >= galeria.length/pageSize - 1"
                                        ng-click="currentPage=numberOfPages()-1; desde=(pageSize*(numberOfPages()-1))+desde;hasta=galeria.length; CalcularHasta()">
                                        Final
                                    </button>

                                </div>
                            </div>
<br>
                            <div class="row">

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 45%;">
                                        Página {{currentPage+1}} de {{numberOfPages()}}


                                    </div>
                                </div>


                        </div>
                        <div class="col-12" id="mensaje_galeria"></div>

                        <div class="contenedor_cards">
                            <div class="card-columns el-element-overlay">
                                <div class="card"
                                    ng-repeat="imagen in galeria | startFrom:currentPage*pageSize | limitTo:pageSize">
                                    <div class="el-card-item">
                                        <div class="el-card-avatar el-overlay-1">
                                            <a class="image-popup-vertical-fit" href="{{base_url}}{{imagen.ruta}}"> <img
                                                    src="{{base_url}}{{imagen.ruta}}" alt="" class="img-galeria" /> </a>
                                        </div>
                                        <div class="el-card-content">
                                            <h3 class="box-title">
                                                {{imagen.titulo}} <i id="btnEliminar{{$index}}"
                                                    name="btnEliminar{{$index}}"
                                                    class="eliminar-img fas fa-times-circle eliminar-img"
                                                    title="Eliminar" data-ng-click="EliminarImagen($event)"
                                                    data="{{imagen.titulo}}|{{imagen.id_categoria}}|{{imagen.id}}"></i>
                                            </h3>
                                            <small>{{imagen.descripcion_categoria}}</small>
                                            <br />
                                        </div>
                                        <div>
                                            <!--<div class="row cuerpo-btn" >
                                                <div class="col-lg-6">
                                                    <button type="button" class="btn waves-effect waves-light btn-block btn-success" id="btnActualizar{{$index}}" data-ng-click="actualizarImagen($event)" data="{{imagen.titulo}}|{{imagen.id_categoria}}|{{imagen.id}}" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">
                                                        <i class="fas fa-pencil-alt"></i>
                                                    </button>
                                                    
                                                </div>
                                                <div class="col-lg-12">
                                                    <button type="button" class="btn waves-effect waves-light btn-block btn-danger">
                                                        <i class="fas fa-times-circle"></i>
                                                    </button>
                                                </div>
                                            </div>-->
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="" ng-if="cero_registro">
                                <div class=" alert alert-danger alert-rounded">No se encuentran registros asociados
                                </div>
                            </div>
                        </div>
                        <!-- ============================================== -->
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel1">Actualizar imagen</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Título</label>
                                <input type="text" class="form-control" id="recipient-name1"
                                    placeholder="Ingrese el título de la imagen" id="tituloImagenActualizar"
                                    name="tituloImagenActualizar" ng-model="tituloImagenModal">
                            </div>
                            <div class="form-group hide">
                                <label for="message-text" class="control-label">Categoria:</label>
                                <select class="form-control form-control-line"
                                    ng-options="option.descripcion for option in categorias_modal track by option.id"
                                    ng-model="actualizarCategoria" id="categoriaImagenActualizar"
                                    name="categoriaImagenActualizar">
                                    <option value="">--Seleccione una categoria--</option>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="cerrarModal" name="cerrarModal" type="button" class="btn btn-default"
                            data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary"
                            ng-click="procesarActualizarImagenModal()">Actualizar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">