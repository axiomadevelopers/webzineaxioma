<div class="page-wrapper">
	<div class="container-fluid" ng-controller="DireccionController">
		<div class="row page-titles">
			<div class="col-md-5 col-8 align-self-center titulo-dashboard">
				<h3 class="text-themecolor">Contactos</h3>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item active">Contactos</li>
					<li class="breadcrumb-item active">Dirección</li>
				</ol>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">{{titulo_pagina}}</h4>
						<h6 class="card-subtitle">{{subtitulo_pagina}}</h6>
						<hr>
						<form class="form-material m-t-40"  name="formularioMarca" id="formularioMarca">
							<div class="row p-20">
								<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 padding0_min " style="padding-top: 5px; ">
									<div class="form-group">
										<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
										<label class="">Idioma:</label>
										<select name="idioma" id="idioma" class="form-control m-bot15 select-picker" data-done-button="true" data-actions-box="true"  data-style="btn-fff " data-live-search="true" ng-model="direccion.id_idioma" data-size="3" ng-change="cargarOrden()">
	                                    	<option value="">--Seleccione un idioma--</option>
	                                    </select>
	                                </div>    
								</div>
								<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
									<div class="form-group">
										<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
										<label class="">Título dirección:</label>
											<div class="controls">
												<input type="text" name="titulo" id="titulo" class="form-control form-control-line" ng-model="direccion.titulo" placeholder="Ingrese el título" onKeyPress="return valida(event,this,18,100)" onBlur="valida2(this,18,100);">
											</div>
									</div>
								</div>
							</div>
							<div class="row p-20" id="super_contenedor">
								<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
									<div class="form-group">
										<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
											<label>Dirección:</label>
											<div class="div_wisig" id="div_descripcion" name="div_descripcion" data-toggle="modal" data-target="#wisiModal" data-whatever="@mdo" ng-click="wisi_modal('1')">
												Pulse aquí para ingresar el contenido
											</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 padding0_min " style="padding-top: 5px; ">
									<div class="form-group">
	                                    <div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
	                                    <label class="">Orden:</label>
	                                    <select name="orden" id="orden" class="form-control m-bot15 select-picker" data-done-button="true" data-actions-box="true"  data-live-search-placeholder="Seleccione el orden del slider" placholder="Seleccione el orden del slider" data-style="btn-fff " data-live-search="true"  ng-model="direccion.orden" data-size="3">
	                                        <option value="">--Seleccione el orden--</option>
	                                    </select>
	                                </div>    
                                </div>
								<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
									<div class="form-group">
										<label class="">Latitud:</label>
											<div class="controls">
												<input type="text" name="titulo" id="titulo" class="form-control form-control-line" ng-model="direccion.latitud" placeholder="Ingrese latitud" onKeyPress="return valida(event,this,23,10)" onBlur="valida2(this,23,10);">
											</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
									<div class="form-group">
										<label class="">Longitud:</label>
											<div class="controls">
												<input type="text" name="titulo" id="titulo" class="form-control form-control-line" ng-model="direccion.longitud" placeholder="Ingrese longitud" onKeyPress="return valida(event,this,23,10)" onBlur="valida2(this,23,10);">
											</div>
									</div>
								</div>
								<div id="fila_0" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
									<div class="form-group">
										<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
										<label>Teléfono #1</label>
										<input type="text" name="telefono_direccion" id="telefono_direccion_0" class="form-control  tlf_direccion" placeholder="Formato:+cod-xxx-xxx-xx-xx" onKeyPress="return valida(event,this,21,20)" onBlur="valida2(this,21,20);" maxlength="50">
							    	</div>
								</div>
							</div>
							<div class="row button-group">
								<div class="col-lg-6">
									<div class="row">
										<div id="div_mensaje"></div>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="row">
										<div class="col-lg-4 col-md-4" ng-if="id_marca!=''">
											<button id="btn-nuevo" type="button" class="btn waves-effect waves-light btn-block btn-primary" ng-click="agregar_telefono_valores('')">Agregar TLF</button>
										</div>
										<div class="col-lg-4 col-md-4" ng-if="id_marca!=''">
											<button id="btn-nuevo" type="button" class="btn waves-effect waves-light btn-block btn-warning" ng-click="quitar_telefono()">Quitar TLF</button>
										</div>
										<div class="col-lg-4 col-md-4" ng-if="id_direccion!=''">
											<a href="<?=base_url();?>cms/direccion">
												<button id="btn-nuevo" type="button" class="btn waves-effect waves-light btn-block btn-success">Nuevo</button>
											</a>
										</div>
										<div class="col-lg-4 col-md-4" ng-if="id_direccion==''">
                                            <button id="btn-limpiar" type="button" class="btn waves-effect waves-light btn-block btn-success" ng-click="limpiar_cajas_direcciones()" >Limpiar</button>
                                        </div>
										<div class="col-lg-4 col-md-4" ng-if="id_marca==''">
											<button id="btn-limpiar" type="button" class="btn waves-effect waves-light btn-block btn-success" ng-click="limpiar_cajas_marcas()">Limpiar</button>
										</div>

										<div class="col-lg-4 col-md-4">
											<a href="<?=base_url();?>cms/direccion/consultarDireccion">
												<button id="btn-consultar" type="button" class="btn waves-effect waves-light btn-block btn-danger" >{{titulo_cons}}</button>
											</a>
										</div>
										<div class="col-lg-4 col-md-4">
												<button id="btn-registrar" type="button" class="btn waves-effect waves-light btn-block btn-info" ng-click="registrarDirecciones()">{{titulo_registrar}}</button>
										</div>
										<input type="hidden" name="id_direccion" id="id_direccion" value="<?php if(isset($id)){echo $id;}?>">
										<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="wisiModal" name="wisiModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel1">Ingrese el contenido</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-group">
											<textarea id="textarea_editor" name="textarea_editor" class="textarea_editor form-control" rows="15" placeholder="Ingrese texto..."></textarea>
                                    </div>
                                </form>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="cerrarModal" type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" ng-click="agregarWisi()">Agregar</button>
                    </div>
                </div>
            </div>
        </div>

		
	</div>
</div>
