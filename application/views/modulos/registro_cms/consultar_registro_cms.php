<div class="page-wrapper">
	<div class="container-fluid" ng-controller="registroCMSConsultasController">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center titulo-dashboard">
                <h3 class="text-themecolor">Configuración</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Configuración</li>
                    <li class="breadcrumb-item active">Consultar Usuarios del CMS</li>
                </ol>
            </div>
        </div>
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">{{titulo_pagina}}</h4>
						<div class="table-responsive m-t-40">
                            <form id="formConsultaPersonas" name="formConsultaPersonas" method="POST" target="_self">
                                <input type="hidden" id="id_personas" name="id_personas" ng-model="id_personas" >
                                 <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                                <div class="col-lg-12 mg-b30">
                                    <a href="{{base_url}}cms/registro_usuario">
                                        <div class="btn btn-success btn-new">Nuevo</div>
                                    </a>
                                </div>
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Acciones</th>
                                            <th>Cedula</th>
                                            <th>Nombre y Apellido</th>
                                            <th>Usuario</th>
                                            <th>Tipo de Usuario</th>
                                            <th>Teléfono</th>
                                            <th>Correo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat = "datos in personas track by $index">
                                            <td class="">{{datos.id}}</td>
                                            <td class="centrado">
                                                <div class="form-group flotar_izquierda">
                                                <div id="ver{{$index}}" ng-click="ver_personas($index)" data="{{datos.id}}" class="btn btn-primary flotar_izquierda2" title="modificar">
                                                        <i class="fas fa-pencil-alt" aria-hidden="true"></i>
                                                    </div>
                                                    <div id="btn_activar{{$index}}_a" name="btn_activar{{$index}}_a" class="btn btn-warning flotar_izquierda2" ng-class="{visible:datos.estatus=='1',invisible:datos.estatus!=1}" title="Inactivar" data-ng-click="activar_registro($event)" data="{{datos.id}}|{{datos.estatus}}">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    </div>
                                                    <div id="btn_activar{{$index}}_b" name="btn_activar{{$index}}_b"  class="btn btn-success flotar_izquierda2" ng-class="{visible:datos.estatus!='1',invisible:datos.estatus==1}" title="Publicar" data-ng-click="activar_registro($event)" data="{{datos.id}}|{{datos.estatus}}">
                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                    </div>
                                                    <div id="btn_eliminar{{$index}}" name="btn_eliminar{{$index}}"  class="btn btn-danger flotar_izquierda2" title="Eliminar" data-ng-click="eliminar_registro($event)" data="{{datos.id}}|2">
                                                    <i class="fa fa-window-close" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="">{{datos.cedula}}</td>
                                            <td class="">{{datos.nombres_apellidos}}</td>
                                            <td class="">{{datos.login}}</td>
                                            <td class="">{{datos.tipo_usuario}}</td>
                                            <td class="">{{datos.telefono}}</td>
                                            <td class="">{{datos.email}}</td>
                                            
                                    </tbody>
                                </table>
                            </form>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
