<div class="page-wrapper" >
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid" ng-controller="ContactosController">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 col-8 align-self-center titulo-dashboard">
                    <h3 class="text-themecolor">Contactos</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Contactos</li>
                        <li class="breadcrumb-item active">Consultar</li>
                    </ol>
                </div>
                <!--<div class="col-md-7 col-4 align-self-center">
                    <div class="d-flex m-t-10 justify-content-end">
                        <div class="">
                            <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i
                                    class="ti-settings text-white"></i></button>
                        </div>
                    </div>
                </div>-->
            </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{titulo_pagina}}</h4>
                        <div class="table-responsive m-t-40">
                            <form id="formConsultaNoticias" method="POST" target="_self">
                                <input type="hidden" id="id_noticias" name="id_noticias" ng-model="id_noticias" >
                                 <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                                <div class="col-lg-12 mg-b30">
                                      
                                </div> 
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Acciones</th>
                                            <th>Nombres</th>
                                            <th>Email</th>
                                            <th>Teléfono</th>
                                            <th style="width:300px;">Mensaje</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat = "contac in contactos track by $index">
                                            <td class="">{{contac.number}}</td>
                                            <td class="centrado">
                                                <div class="form-group">
                                                    <div ng-click="ver_detalle($index)" class="btn btn-danger flotar_izquierda" title="Ver detalle" style="float:left">
                                                        <i class="fa fa-file" aria-hidden="true"></i>
                                                    </div>
                                                </div>  
                                            </td>
                                            <td class="justificado" width="20%"><div>{{contac.nombres}}</div></td>
                                            <td class="">{{contac.email}}</td>
                                            <td class="">{{contac.telefono}}</td>
                                            <td class="">{{contac.mensaje_corto}}</td>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!--Bloque modal del sistema -->
        <div class="modal fade" id="modal_mensaje" name="modal_mensaje" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-lg .modal-sm">
                  <div class="modal-content">
                    <div class="modal-header header_conf">
                        <h2><p id="cabecera_mensaje" name="cabecera_mensaje">{{titulo_mensaje}}</p></h2>
                        <button type="button" id="cerrar_mensaje2" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" id="cuerpo_mensaje" name="cuerpo_mensaje">
                    <!--Modal body -->
                        <div class=row>
                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <h3>
                                    Detalles:
                                </h3>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label>Nombres:</label>
                                    <span>{{detalle.nombres}}</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label>Teléfono:</label>
                                    <span>{{detalle.telefono}}</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label>Email:</label>
                                    <span>{{detalle.email}}</span>
                                </div>
                            </div>
                            
                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <h3>Mensaje:</h3>
                                    <div class="div_wisig" id="div_descripcion">
                                        {{detalle.mensaje}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- modal body-->
                    </div>  
                    <div class="modal-footer footer_conf">
                        <!-- Footter del modal -->
                          <button type="button" name="modal_reporte_salir" id="modal_reporte_salir" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        <!-- Fin footter del modal -->
                    </div>
                  </div>
                </div>
        </div>
        <!-- -->
    </div>
</div>