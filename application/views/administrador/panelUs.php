<!DOCTYPE html>
<html lang="es" ng-app="ContentManagerApp" class=" ">
    <head>

        <!-- 
         * @Package: Complete Admin - Responsive Theme
         * @Subpackage: Bootstrap
         * @Version: 2.2
         * This file is part of Complete Admin Theme.
        -->
        <meta charset="utf-8" />
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <title>Content Manager</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="@gsantucci|github/bitbucket:santu1987" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!--<base href="/it_security/administrador/">-->

        <link rel="shortcut icon" href="assets/cms/images/axioma.ico" type="image/x-icon" />    <!-- Favicon -->
     
        <!-- CORE CSS FRAMEWORK - START -->
        <link href="assets/cms/plugins/pace/pace-theme-flash.min.css" rel="stylesheet" type="text/css" media="screen"/>
        <link href="assets/cms/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/cms/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/cms/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/cms/plugins/calendar/fullcalendar.min.css" rel="stylesheet" type="text/css" media="screen"/>
        <link href="assets/cms/css/animate.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/cms/plugins/perfect-scrollbar/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/cms/plugins/jquery-ui/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" media="screen"/>
        <!--<link href="assets/cms/plugins/timepicker/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css" media="screen"/>-->
        <link href="assets/cms/plugins/datepicker/css/datepicker.css" rel="stylesheet" type="text/css"/>
        <!--<link href="assets/cms/plugins/timepicker/css/timepicker.less" rel="stylesheet" type="text/css" media="screen"/>-->

        <!-- CORE CSS FRAMEWORK - END -->
        <!-- MESSENGER -->
        <link href="assets/cms/plugins/messenger/css/messenger.min.css" rel="stylesheet" type="text/css" media="screen"/>
        <link href="assets/cms/plugins/messenger/css/messenger-theme-future.min.css" rel="stylesheet" type="text/css" media="screen"/>
        <link href="assets/cms/plugins/messenger/css/messenger-theme-flat.min.css" rel="stylesheet" type="text/css" media="screen"/>
        <!-- -->
        <!-- CORE CSS TEMPLATE - START -->
        <link href="assets/cms/css/style.css" rel="stylesheet" type="text/css"/>
        <!--<link href="assets/cms/css/responsive.css" rel="stylesheet" type="text/css"/>-->
        <!-- CORE CSS TEMPLATE - END -->
        <!-- CORE PLUGINS CSS-->
        <!-- Datatables CSS -->
        <link rel="stylesheet" type="text/css" href="assets/cms/plugins/datatable/css/dataTables.bootstrap.min.css">
        <!-- Datatables Editor Addon CSS -->
        <link rel="stylesheet" type="text/css" href="assets/cms/plugins/datatable/css/dataTables.editor.min.css">
        <!-- Datatables ColReorder Addon CSS -->
        <link rel="stylesheet" type="text/css" href="assets/cms/plugins/datatable/css/dataTables.colReorder.min.css">
        <link href="assets/cms/css/bootstrap-select.css" rel="stylesheet" type="text/css"/>
        <link href="assets/cms/plugins/trumbowyg/dist/ui/trumbowyg.min.css" rel="stylesheet" type="text/css" media="screen"/>
         <!-- Revolution Slider -->
        <link rel="stylesheet" href="assets/cms/plugins/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
        <link rel="stylesheet" href="assets/cms/plugins/revolution-slider/revolution/css/settings.css">
        <link rel="stylesheet" href="assets/cms/plugins/revolution-slider/revolution/css/layers.css">
        <link rel="stylesheet" href="assets/cms/plugins/revolution-slider/revolution/css/navigation.css"> <!-- Revolution Slider -->
        <link rel="stylesheet" href="assets/cms/plugins/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
        <link rel="stylesheet" href="assets/cms/plugins/revolution-slider/revolution/css/settings.css">
        <link rel="stylesheet" href="assets/cms/plugins/revolution-slider/revolution/css/layers.css">
        <link rel="stylesheet" href="assets/cms/plugins/revolution-slider/revolution/css/navigation.css">
        <!-- CORE -->
        <link href="assets/cms/css/carousel.css" rel="stylesheet" type="text/css">
        <link href="assets/cms/css/index.css" rel="stylesheet" type="text/css">
        <!-- -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body><!-- START TOPBAR -->
    <!-- START TOPBAR -->
    <div id="contenedor_principal" ng-view>
    </div>
    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


<!-- CORE JS FRAMEWORK - START --> 
<script src="assets/cms/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
<script src="assets/cms/js/jquery.easing.min.js" type="text/javascript"></script>
<script src="assets/cms/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/cms/plugins/pace/pace.min.js" type="text/javascript"></script>  
<script src="assets/cms/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
<script src="assets/cms/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
<!-- Timepicker -->
<!--<script src="assets/cms/plugins/timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>-->
<script src="assets/cms/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
<script>window.jQuery||document.write('<script src="assets/cms/js/jquery-1.11.2.min.js"><\/script>');</script>
<!-- CORE JS FRAMEWORK - END --> 
<!-- -->
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


<!-- CORE TEMPLATE JS - START --> 
<!-- Boostrap select -->
<script type="text/javascript" src="assets/cms/js/bootstrap-select.min.js"></script> 
<!-- Calendar -->
<script src="assets/cms/plugins/calendar/moment.min.js" type="text/javascript"></script>
<script src="assets/cms/plugins/calendar/fullcalendar.min.js" type="text/javascript"></script>
<!-- Messagges-->
<script src="assets/cms/plugins/messenger/js/messenger.min.js" type="text/javascript"></script>
<script src="assets/cms/plugins/messenger/js/messenger-theme-future.min.js" type="text/javascript"></script>
<script src="assets/cms/plugins/messenger/js/messenger-theme-flat.min.js" type="text/javascript"></script>
<script src="assets/cms/plugins/messenger/js/messenger.js" type="text/javascript"></script>
<!-- Datatables -->
<script src="assets/cms/plugins/datatable/js/jquery.dataTables.min.js"></script>
<!-- Datatables Tabletools addon -->
<script src="assets/cms/plugins/datatable/js/dataTables.tableTools.min.js"></script>
<!-- Datatables ColReorder addon -->
<script src="assets/cms/plugins/datatable/js/dataTables.colReorder.min.js"></script>
<!-- Datatables Bootstrap Modifications  -->
<script src="assets/cms/plugins/datatable/js/dataTables.bootstrap.min.js"></script>
<script src="assets/cms/plugins/jquery_mask/jquery.mask.min.js"></script>
<!-- -->
<!--<script src="assets/cms/plugins/datatable/js/dataTables.bootstrap.js"></script>-->
<!--    CORE SCRIPT -->
<script src="assets/cms/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>

<script src="assets/cms/js/scripts.js" type="text/javascript"></script> 
<!--     -->
<script src="assets/cms/plugins/trumbowyg/dist/trumbowyg.min.js" type="text/javascript"></script> 
<script src="assets/cms/js/masonry.pkgd.min.js" type="text/javascript"></script> 
<!-- END CORE TEMPLATE JS - END --> 
<!--Funciones basic -->
<script type="text/javascript" src="assets/cms/js/fbasic.js"></script>

<!-- -->
<!-- CORE ANGULAR SCRIPTS -->
<script type="text/javascript" src="assets/cms/js/angular/angular.min.js"></script>
<script type="text/javascript" src="assets/cms/js/angular/angular-route.min.js"></script>
<script type="text/javascript" src="assets/cms/js/angular/angular-sanitize.min.js"></script>
<script type="text/javascript" src="assets/cms/js/angular/app.js"></script>
<script type="text/javascript" src="assets/cms/js/angular/directivas/directives.js"></script>
<script type="text/javascript" src="assets/cms/js/angular/servicios/services.js"></script>
<!--Cuerpo de controladores -->
<script type="text/javascript" src="assets/cms/js/angular/controladores/MainController.js"></script>
<!-- -->