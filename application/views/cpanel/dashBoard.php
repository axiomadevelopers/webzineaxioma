<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="">
                        <!-- Logo icon --><b>
                            <!-- Light Logo icon -->
                            <!--<img src="<?=base_url();?>/assets/images/rocket.png" alt="homepage" class="light-logo icono-principal" />
                            <img src="<?=base_url();?>/assets/images/logo_oks.jpg" alt="homepage" class="light-logo icono-principal" />-->
                            <img src="<?=base_url();?>/assets/images/logo_oks_blanco.png" alt="homepage" class="light-logo icono-principal" /> 
                            <!--
                            <i class="fa fa-rocket texto-blanco" aria-hidden="true"></i>-->
                            
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span  class="titulo-page">
                            <!--
                            <img src="<?=base_url();?>/assets/images/oks_blanco.png" alt="homepage" class="light-logo icono-oks" /> 
                            <b class="titulo-uno"> C.M.S</b>-->
                            
                            <b class="titulo-uno">Content</b>Manager 
                        </span>
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark"
                                href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark"
                                href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                       
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="index.html" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <img src="<?=base_url().$ruta_imagen;?>"  alt="user"
                                    class="profile-pic" />
                            </a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                           <div class="u-img">
                                                <img src="<?=base_url().$ruta_imagen;?>" alt="user">
                                           </div>
                                            <div class="u-text">
                                                <h4><?=$login?></h4>
                                                <p class="text-muted"><?=$nombre_persona?></p>
                                            </div>
                                            <div class="dropdown-user">
                                                <li role="separator" class="divider"></li>
                                                 <li><a href="<?=base_url();?>Login/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->