
<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        <div class="user-profile" style="background: url(<?=base_url();?>assets/images/background/cms-info2.jpg) no-repeat;">
            <!-- User profile image -->
            <div class="profile-img"> <img src="<?=base_url().$ruta_imagen;?>"  alt="user" /> </div>

            <!-- User profile text-->
            <div class="profile-text"> 
                <a href="index.html#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" style="color: #fff!important;"
                    role="button" aria-haspopup="true" aria-expanded="true">
                    <?=$login?>
                </a>
                <div class="dropdown-menu animated flipInY"> 
                    
                    <a href="" class="dropdown-item"><i class="ti-user"></i>
                        Perfil</a>
                    
                    <div class="dropdown-divider"></div> <a href="<?=base_url();?>Login/logout" class="dropdown-item"><i class="fa fa-power-off"></i>
                            Logout</a>
                </div>
                <div style="display: none;" class="chartist-tooltip">
                </div>
            </div>
        </div>
        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap">MENÚ</li>
                    <li id="li_empresa" class="li-menu">
                        <a id="empresa" class="a-menu" href="<?=base_url();?>cms/empresa">
                            <i class="fa fa-university" aria-hidden="true"></i>
                            Empresa
                        </a>
                    </li>
                    <li  id="li_noticias" class="li-menu"> 
                        <a class="has-arrow waves-effect waves-dark"  aria-expanded="false"><i class="mdi mdi-newspaper"></i>
                        <span class="hide-menu">Noticias </span>
                        </a>
                        <ul aria-expanded="false" class="collapse">
                            <li>
                                <a id="categorias_noticias" class="a-menu" href="<?=base_url();?>cms/categorias_noticias">
                                    <span class="hide-menu">Categorías noticias </span>
                                </a>
                            </li>
                            <li>
                                <a id="noticias" class="a-menu" href="<?=base_url();?>cms/noticias">
                                    <span class="hide-menu">Registro noticias </span>
                                </a>
                            </li>                        
                        </ul>
                    </li> 
                <li id="li_publicidad" class="li-menu">
                    <a id="publicidad" class="a-menu" href="<?=base_url();?>cms/publicidad">
                        <i class="fa fa-paper-plane" aria-hidden="true"></i>
                        Publicidad
                    </a>
                </li>
       
                <li  id="li_contactos" class="li-menu"> <a class="has-arrow waves-effect waves-dark"  aria-expanded="false"><i class="mdi mdi-account-box"></i>
                        <span class="hide-menu">Contactos </span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a id="contactos" class="a-menu" href="<?=base_url();?>cms/contactos">Contactos</a></li>
                        <li><a id="banco" class="a-menu" href="<?=base_url();?>cms/banco">Banco</a></li>
                        <li><a id="cuenta_bancaria" class="a-menu" href="<?=base_url();?>cms/cuenta_bancaria">Cuentas Bancarias </a></li>
                        <li><a id="redes_sociales" class="a-menu" href="<?=base_url();?>cms/redes_sociales">Redes Sociales</a></li>
                        <li><a id="direccion" class="a-menu" href="<?=base_url();?>cms/direccion">Dirección</a></li>
                        <li><a id="footer" class="a-menu" href="<?=base_url();?>cms/footer">Footer</a></li>
                    </ul>
                </li>
                <li  id="li_configuracion" class="li-menu"> <a class="has-arrow waves-effect waves-dark"  aria-expanded="false"><i class="ti-settings"></i>

                <span class="" ng-class="{active:categorias_menu==='1',hide:categorias_menu===''}">Configuracion  {{categorias_menu}}</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a id="categoria" class="a-menu" href="<?=base_url();?>cms/categorias">Categoría</a></li>
                        <li><a id="galeria" class="a-menu" href="<?=base_url();?>cms/galeriaMultimedia">Galería</a></li>
                        <li><a id="carga_pdf" class="a-menu" href="<?=base_url();?>cms/cargar_pdf">Carga de PDF</a></li>
                        <!-- Solo para usuario administrador -->
                        <?php if($tipo_usuario==1){?>
                        <li><a id="registro_cms" class="a-menu" href="<?=base_url();?>cms/registro_usuario">Usuarios</a></li>
                        <?php } ?>
                        <!-- -->
                        <li><a id="meta_description" class="a-menu" href="<?=base_url();?>cms/descripcion">Meta Descripción</a></li>
                        <li><a id="palabras_claves" class="a-menu" href="<?=base_url();?>cms/palabras_claves">Palabras Claves</a></li>
                        <!-- Solo para usuario administrador -->
                        <?php if($tipo_usuario==1){?>
                        <li><a id="auditoria" class="a-menu" href="<?=base_url();?>cms/auditoria">Auditoria</a></li>
                        <?php } ?>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
    <!-- Bottom points-->
    <div class="sidebar-footer">
        <!-- item-->
        <!--<a href="index.html" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>-->
        <!-- item-->
        <!--<a href="index.html" class="link" data-toggle="tooltip" title="Settings"><i class="mdi mdi-comment-text"></i></a>-->
        <!-- item-->
        <!--<a href="<?=base_url();?>/Login/logout" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>-->
    </div>
    <!-- End Bottom points-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
