<div id="divInicio" class="container" ng-controller="mainController">
	<section>
		<div class="row">
			<!-- Editorial -->
  			<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
  				<div class="container mb-5">
  					<!-- -->
					<div class="col-md-12">
						<div class="row">
							<div class="thumbnail wow fadeInLeft">
								<div class="col-md-12 order-1">
									<h3 class="h3-title">{{editorial.titulo}}</h3>
								</div>
								<img ng-src="{{base_url}}{{editorial.ruta}}" alt="blog-image">
								<div class="caption cuerpo-tab">
									<div class="row">
										<div class="col-md-12 order-1">
											<p id="autor" name="autor" class="autor">
												<i class="fa fa-user-o" aria-hidden="true"></i>
												{{editorial.autor}}
											</p>
											<p id="calendario" name="calendario" class="autor">	
												<i class="fa fa-calendar"></i>
												{{editorial.fecha}}
											</p>
											<p id="calendario" name="calendario" class="autor">	
												<i class="fa fa-tasks"></i>
												{{editorial.categoria_noticia}}
											</p>
										</div>	
										<div class="col-md-12 order-1">
											<p id="descripcion_detalle_producto" name="descripcion_detalle_producto" class="texto-parrafos" ng-bind-html ="editorial.descripcion.substr(0,600)">
											</p>
											<a href="{{base_url}}editorial/{{editorial.slug}}">
											<p>
												Continuar Leyendo...
											</p>
											</a>

										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
  				</div>
  			</div>
  			<!-- Editorial -->
  			<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 ocultar-publicidad">
  				<div class="container">
  					<busca-dor></busca-dor>
  					<ca-tegorias></ca-tegorias>
  					<div class="col-lg-12">
  						<h3 class="h3-title">Publicidad</h3>
  					</div>
  					
  					<publi-cidad ng-repeat="publicidad in publicidades track by $index"></publi-cidad>
  				</div>
  			</div>
		</div>
	</section>		
	<!--Post -->
	<section class="cuerpo-post mt-5">
		<div class="col-md-12" ng-repeat="categoria in categorias_blog track by $index">
			<div class="card-header {{categoria.color}}" role="tab" id="headingFour" >
				<h3 class="mb-0">
					{{categoria.descripcion}}
				</h3>
			</div>
			<div class="row">
				<!-- -->
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 mt-5  fadeInUp wow " ng-repeat="post in posts track by $index" ng-if="categoria.id==post.id_categoria && $index<6">
		            <div  class="thumbnail">
		                <div class="imageWrapper">
		                    <a href="{{base_url}}publicaciones/{{post.slug}}">
		                      <img ng-src="{{base_url}}{{post.ruta}}" alt="article-image">
		                    </a>
		                    <div class="date-holder">
		                      <p>{{post.dias}}</p>
		                      <span>{{post.mes}}</span>
		                    </div>
		                </div>  
		                <a href="{{base_url}}publicaciones/{{post.slug}}">
			                <h4 class="card-title title-news">
								{{post.titulo}}
							</h4>
		            	</a>
		                <span class="meta"> 
		                	<i class="fa fa-user-o" aria-hidden="true"></i>
		                	<a class="pr-1 user" href="{{post.slug}}">{{post.autor}}</a> 
		                </span>
		                <div class="caption2">
		                  <p ng-bind-html="post.descripcion.substr(0,200)"></p>
	                	</div>
	                	<a href="{{base_url}}publicaciones/{{post.slug}}"
							class="btn btn-brand btn-block mt-3">Leer mas</a>
	              	</div>
	              	
	            </div>
				<!-- -->
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-5">
	              	<div class="row mt-5">
			          <div class="col-md-12">
			            <p class="text-left">
			              <a href="{{base_url}}buscador/categorias/{{categoria.id}}" class="mas_post" ng-click="cargar_mas_post()"><i class="fa fa-plus-square" aria-hidden="true"></i> Más Post
			              </a>
			            </p>
			          </div>
			          <div style="clear: both;"></div>
			        </div>
			    </div>    
	            <!-- -->
			</div>	
		</div>
	</section>
	<!-- Post -->
	<!--Publicidad movil-->
	<section class="publicidad-movil" id="publicidad-movil">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<busca-dor></busca-dor>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">  	<ca-tegorias></ca-tegorias>
				</div>	
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h3 class="h3-title">Publicidad</h3>
					<publi-cidad ng-repeat="publicidad in publicidades track by $index"></publi-cidad>
				</div>
			</div>		
		</div>
	</section>
	<!--Publicidad movil-->		
</div>
