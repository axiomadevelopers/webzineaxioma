<li class="bg-dark bg-dark-30">
	<img src="<?=base_url();?>assets/web/images/slider/slider1.png" alt="">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6" style="text-align: center">
						<div class="col-lg-12">
							<h1 class="h1 wow fadeInDown titulo_uniseguros titulo1" id="titulo_uniseguros" data-wow-delay="0.5s" style="" >Sabemos  </h1>
						</div>
						<div class="col-lg-12 mobile-title">
							<div class="m-b-40 wow fadeInDown" data-wow-delay="0.7s">
								
								<span class="subtitulo_slide titulo_slide1">
								</span>
								
								<span class="subtitulo_uniseguros titulo_uniseguros">
									
								</span> 
							</div>
						</div>	
						<div class="col-lg-12">
							<p class="m-b-40 wow fadeInDown subtitulo_slide titulo_slide1 subtitulo_slide_grande" data-wow-delay="0.7s" style="" >lo que es importante para ti</p>
						</div>
						<div class="col-lg-12">
							<div class="m-b-10  wow fadeInDown" ><a class="btn btn-circle btn-lg btn-brand" href="">Contactanos</a></div>
						</div>
					</div>
					
					<div class="col-md-4 bloque-subtitulo-tu-factoring" style="text-align: left">		
						<h3 class="h1 m-t-20 wow fadeInDown subtitulo_uniseguros titulo_uniseguros" data-wow-delay="0.5s" id="titulo_uniseguros">  </h3>
					</div>
				</div>		
			
			</div>
		</div>
	</div>
</li>
<!--Slider 2 -->
<li class="bg-dark bg-dark-30">
	<img src="<?=base_url();?>assets/web/images/slider/slider2.png" alt="">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6" style="text-align: center">
						<div class="col-lg-12">
							<h1 class="h1 wow fadeInDown titulo_uniseguros titulo2" id="titulo_uniseguros" data-wow-delay="0.5s" style="" >Ahora  </h1>
						</div>
						<div class="col-lg-12 mobile-title">
							<div class="m-b-40 wow fadeInDown" data-wow-delay="0.7s">
								
								<span class="subtitulo_slide titulo_slide1">
								
								</span>
								
								<span class="subtitulo_uniseguros titulo_uniseguros">
									
								</span> 
							</div>
						</div>	
						<div class="col-lg-12">
							<p class="m-b-40 wow fadeInDown subtitulo_slide titulo_slide subtitulo_slide_grande" data-wow-delay="0.7s" style="" >tu vehículo estará asegurado </p>
						</div>
						<div class="col-lg-12">
							<div class="m-b-10  wow fadeInDown"><a class="btn btn-circle btn-lg btn-brand" href="">Ver 	Servicios</a></div>
						</div>
					</div>
					
					<div class="col-md-4 bloque-subtitulo-tu-factoring" style="text-align: left">		
						<h3 class="h1 m-t-20 wow fadeInDown subtitulo_uniseguros titulo_uniseguros" data-wow-delay="0.5s" id="titulo_uniseguros">  </h3>
					</div>
				</div>		
				
			</div>
		</div>
	</div>
</li>
<!--Slider 3 -->
<li class="bg-dark bg-dark-30">
	<img src="<?=base_url();?>assets/web/images/slider/slider3.png" alt="">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6" style="text-align: center">
						<div class="col-lg-12">
							<h1 class="h1 m-b-20 wow fadeInDown titulo_uniseguros" id="titulo_uniseguros" data-wow-delay="0.5s" style="" >Con Uniseguros </h1>
						</div>
						<div class="col-lg-12 mobile-title">
							<div class="m-b-40 wow fadeInDown" data-wow-delay="0.7s">
								
								<span class="subtitulo_slide titulo_slide1">
									
								</span>
								|
								<span class="subtitulo_uniseguros titulo_uniseguros">
									
								</span> 
							</div>
						</div>	
						<div class="col-lg-12">
							<p class="m-b-40 wow fadeInDown subtitulo_slide titulo_slide1 subtitulo_slide_grande" data-wow-delay="0.7s" style="" >Protegerte está a tu alcance</p>
						</div>
						<div class="col-lg-12">
							<div class="m-b-10 wow fadeInDown"><a class="btn btn-circle btn-lg btn-brand" href="">Ver 	Productos</a></div>
						</div>
					</div>
					<h4 style="" class="separador"></h4>
					<div class="col-md-4 bloque-subtitulo-tu-factoring" style="text-align: left">		
						<h3 class="h1 m-t-20 wow fadeInDown subtitulo_uniseguros titulo_uniseguros" data-wow-delay="0.5s" id="titulo_uniseguros"> </h3>
					</div>
				</div>		
			</div>
		</div>
	</div>
</li>
			