<div id="divDetalle" class="container" ng-controller="detalleController">
	<input type="hidden" id="slug" name="slug" ng-model="slug" value="<?php echo $slug;?>">
	<input type="hidden" id="editorial" name="editorial" ng-model="slug" value="<?php echo $editorial;?>">
	<section>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 mt-5 mb-5">
				<a class="btn btn-brand btn-sm mt-3 btn-inicio" href="{{base_url}}">
					<i class="fa fa-arrow-circle-left" aria-hidden="true"></i>Inicio
				</a>	
			</div>
  			<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
  				<div class="container mb-5">
  					<!-- -->
  					
					<div class="col-md-12">

						<div class="row">
							<div class="thumbnail wow fadeInLeft">
								<div class="col-md-12 order-1 ">
									<h3 class="h3-title">{{detalle_posts.titulo}}</h3>
								</div>
								<section id="section_img" name="section_img" ng-if="detalle_posts.link_youtube==''">
									<img ng-src="{{base_url}}{{detalle_posts.ruta}}" alt="blog-image">
								</section>
								<section id="section_youtube" name="section_youtube" class="" ng-if="detalle_posts.link_youtube!=''">
									<!-- -->
									<div id="contenedor_vi" name="contenedor_vi">  
										<div class="col-lg-12 col-md-12 col-xs-12 col-md-12 padding0">
										    <div class="fadeInUp wow">
										      	<div class="col-lg-12 padding0" style="cursor: pointer" >
											      	<div id="contenedor_vi" name="contenedor_vi">
											       		<div class="content_video">
												          <div class="content_iframe_video centrado">
												            <iframe class="centrado" id="reproductor" width="100%" style="height: 400px" src="" frameborder="0" allowfullscreen></iframe>
												            <div style="clear: both"></div>
												          </div>
												          <div style="clear: both"></div>
												        </div>
												        <div style="clear: both;"></div>
											      	</div>
										      	</div>
										      	<div style="clear:both"></div>
										    </div>
										</div>
									</div>
								    <!-- -->
								</section>
								<section id="section_podcast" name="section_podcast" ng-if="detalle_posts.link_soundcloud!=''" class="mt-5">
									<div>
										<iframe id="iframe"
				                         class="iframe"
				                         width="100%"
				                         height="200"
				                         scrolling="no"
				                         frameborder="no"
				                         >
				                    	</iframe>

									</div>
								</section>
								<section>
									<div class="caption cuerpo-tab">
										<div class="row cuerpo-detalle">
											<div class="col-md-12 order-1">
												<p id="autor" name="autor" class="autor">
													<i class="fa fa-user-o" aria-hidden="true"></i>
													{{detalle_posts.autor}}
												</p>
												<p id="calendario" name="calendario" class="autor">	
													<i class="fa fa-calendar"></i>
													{{detalle_posts.fecha}}
												</p>
												<p id="categoria" name="categoria" class="autor">	
													<i class="fa fa-tasks" aria-hidden="true"></i>
													{{detalle_posts.categoria_noticia}}
												</p>
												
											</div>
											<div class="col-md-12 order-1">
												<p id="descripcion_detalle_producto" name="descripcion_detalle_producto" class="texto-parrafos" ng-bind-html="detalle_posts.descripcion">
												</p>
											</div>
										</div>
									</div>
								</section>
							</div>
						</div>	
					</div>
					
					
  				</div>
  			</div>
  			<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 cuerpo-detalle-publi">
  				<div class="container">
  					
  					<busca-dor></busca-dor>

  					<ca-tegorias></ca-tegorias>

  					<div class="col-lg-12">
  						<h3 class="h3-title">Publicidad</h3>
  					</div>
  					
  					<publi-cidad ng-repeat="publicidad in publicidades track by $index"></publi-cidad>
  				</div>
  			</div>
		<div>
	</section>
	<section id="cuerpo-disquss" class="mt-5">
	<div id="disqus_thread"></div>
	<script>

	/**
	*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
	*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
	/*
	var disqus_config = function () {
	this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
	this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
	};
	*/
	(function() { // DON'T EDIT BELOW THIS LINE
	var d = document, s = d.createElement('script');
	s.src = 'https://axiomadevelopers-com-ve.disqus.com/embed.js';
	s.setAttribute('data-timestamp', +new Date());
	(d.head || d.body).appendChild(s);
	})();
	</script>
	<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>               
	</section>
	<!-- -->
	<section id="otrosPostDetalles">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="container">
					<h3 class="h3-title">Otros Post</h3>
					<div class="row">
						<div class="pt-20 col-lg-4 col-md-6 col-xs-12 col-sm-12 mb-5"
						ng-repeat="post in post_limites track by $index">
						<div class="card card-body text-center wow fadeInUp">
							<div class="card-ribbon card-ribbon-top card-ribbon-right bg-faded ">
								<div class="icon-box-icon">
									<img class="img-responsive img-servicios"
									ng-src="{{base_url}}{{post.ruta}}">
								</div>
							</div>
							<div class="col-md-12">
								<h4 class="card-title">
									{{post.titulo}}
								</h4>
							</div>
							<div class="col-lg-12">		
								<p id="autor" name="autor" class="autor">
									<i class="fa fa-user-o" aria-hidden="true"></i>
									{{post.autor}}
								</p>
							</div>
							<div class="col-lg-12">	
								<p id="calendario_post" name="calendario_post" class="autor">	
									<i class="fa fa-calendar"></i>
									{{post.fecha}}
								</p>
								<p id="categoria_post" name="categoria_post" class="autor">	
									<i class="fa fa-tasks"></i>
									{{post.categoria_noticia}}
								</p>
							</div>	
							<p ng-bind-html="post.descripcion_sin_html">
							</p>
							<a href="{{base_url}}publicaciones/{{post.slug}}"
							class="btn btn-brand btn-sm mt-3">Leer mas</a>
						</div>
					</div>
					</div>
				</div>
		</div>	
  	</section>
  	<!-- -->
  	<section class="cuerpo-detalle-publi-movil">
  		<!-- -->
  		<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 ">
			<div class="container">
				
				<busca-dor></busca-dor>

				<ca-tegorias></ca-tegorias>

				<div class="col-lg-12">
					<h3 class="h3-title">Publicidad</h3>
				</div>
				
				<publi-cidad ng-repeat="publicidad in publicidades track by $index"></publi-cidad>
			</div>
		</div>
  		<!-- -->
  	</section>	
</div>		