<!-- Fin de wrapper -->
</div>
<!-- FIn de box -->
</div>
       
<!-- Parallax de servicios-->
    <div name="id_idioma" id="id_idioma" style="display: none"><?php echo $id_idioma;?></div>
    <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>"> 		
		<!-- Footer-->
	<footer class="dark-bg fadeInUp wow" id="footer" ng-controller="footerController">
        <div class="container inner">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-6 inner parrafos parrafos-footer">
                    <div>
                        <!--<img src="<?=base_url();?>assets/images/logo_peque.png" class="logo img-intext" alt="">-->
                        <img src="<?=base_url();?>assets/web/images/titulo_axioma_fff.png" class="logo" alt="">
                    </div>
                    <p ng-bind-html="footer.descripcion"></p>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-6  inner texto-footer">
                    <h4 class="subtitulos">SERVICIOS</h4>
                    <div>
                        <ul class="padding0 parrafos parrafos-footer">
                            <li><span class="icono_serv"><i class="icon-code"></i></span>Desarrollo web</li>
                            <li><span class="icono_serv"><i class="icon-basket"></i></span>E-comerce</li>
                            <li><span class="icono_serv"><i class="icon-cog-alt"></i></span>Desarrollo de sistemas</li>                            
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-6  inner texto-footer">
                    <h4 class="subtitulos">{{direccion.titulo}}</h4>
                    <ul class="contacts parrafos parrafos-footer">
                        <li>
                            <p>
                            <i class="icon-location contact"></i>
                            <span ng-bind-html="direccion.descripcion"></span>
                            </p>
                        </li>
                        <li><i class="icon-mobile contact"></i>{{direccion.telefono[0]}}</li>
                        <li><a href="mailto:{{footer.correo}}" target="_blank"><i class="icon-mail-1 contact correo-mail"></i>{{footer.correo}}</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-6  inner texto-footer">
                    <h4 class="subtitulos">REDES SOCIALES</h4>
                        <ul class="social parrafos parrafos-footer redes-f" style="text-align: center">
                            <li class="left social_li2"><a href="{{facebook}}" target="_blank"><i class="icon-facebook-circled"></i></a></li>
                            <li class="left social_li2"><a href="{{twitter}}" target="_blank"><i class="icon-twitter-circled"></i></a></li>
                            <li class="left social_li2"><a href="{{linkedin}}" target="_blank"><i class="icon-linkedin-circled"></i></a></li>
                            <li class="left social_li2"><a href="{{instagram}}" target="_blank"><i class="icon-s-instagram"></i></a></li>
                        </ul>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container inner">
                <p class="pull-center footer">© 2020 Axioma Developers Derechos Reservados.</p>
            </div>
        </div>
    </footer>
		<!-- Footer end-->

		<div class="scroll-top" onclick="subir_top()"><i class="fa fa-angle-up"></i></div>
	</div>
	<!-- Wrapper end-->
</div>
		<!-- Layout end-->

<!-- Off canvas-->
<div class="off-canvas-sidebar">
	<div class="off-canvas-sidebar-wrapper">
		<div class="off-canvas-header"><a class="close-offcanvas" href="index-21.html#"><span class="arrows arrows-arrows-remove"></span></a></div>
		
	</div>
</div>
<!-- Off canvas end-->
</body>
<!-- Scripts-->
<script src="<?=base_url();?>assets/web/jquery-2.2.4.min.js"></script>
<script src="<?=base_url();?>assets/web/popper.min.js"></script>
<script src="<?=base_url();?>assets/web/bootstrap/js/bootstrap.min.js"></script>
<!--
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
-->

<!--
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDlYNNs7VBO71qiKFMNiD0R9sd8hOt0wD4"></script>
-->
<script src="<?=base_url();?>assets/web/js/plugins.min.js"></script>
<script src="<?=base_url();?>assets/web/js/custom.js"></script>

<!--Plugins -->
<script src="<?=base_url();?>assets/web/plugins/dzsparallaxer/dzsparallaxer.js"></script>
<script src="<?=base_url();?>assets/web/plugins/dzsparallaxer/dzsscroller/scroller.js"></script>
<script src="<?=base_url();?>assets/web/plugins/dzsparallaxer/advancedscroller/plugin.js"></script>
<script src="<?=base_url();?>assets/web/plugins/wow/wow.min.js"></script>
<script src="<?=base_url();?>assets/web/plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="<?=base_url();?>assets/web/plugins/slick/js/slick.js"></script>
<script src="<?=base_url();?>assets/web/js/fbasic.js"></script>
<script type="text/javascript" src="http://w.soundcloud.com/player/api.js"></script>

<!--Angular Core -->
<!--
<!--Core Angular JS -->
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/angular.js"></script>
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/angular-route.js"></script>
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/angular-sanitize.min.js"></script>
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/app.js"></script>
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/directivas/directives.js"></script>
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/servicios/services.js"></script>
<!--Controladores Ng -->
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/mainController.js"></script>
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/inicioController.js"></script>
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/detalleController.js"></script>
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/buscadorController.js"></script>
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/footerController.js"></script>

</html>
