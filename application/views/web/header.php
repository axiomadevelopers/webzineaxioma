<!DOCTYPE html>
<html lang="es" ng-app="WebZineApp">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="author" content="Axioma DVLP @santu1987">
    	<meta name="format-detection" content="telephone=no">
    	<meta name="og:tilte" content="WebZine">
	    <meta name="og:description" content="">
	    <meta name="og:url" content="">
	    <meta name="og:site_name" content="Uniseguros">
	    <meta name="og:type" content="website">
	    <meta name="og:image" content="">
		<title>Web Zine|Axioma</title>
		<!-- Favicons-->
		<link rel="shortcut icon" href="<?=base_url();?>assets/web/images/rocket2.png">
		<link rel="apple-touch-icon" href="<?=base_url();?>assets/web/images/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?=base_url();?>assets/web/images/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?=base_url();?>assets/web/images/apple-touch-icon-114x114.png">
		<!-- Web Fonts-->
		<link href="<?=base_url();?>assets/web/css/fontsgoogleapis.css" rel="stylesheet">
		<!-- Bootstrap core CSS-->
		<link href="<?=base_url();?>assets/web/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- Plugins and Icon Fonts-->
		<link href="<?=base_url();?>assets/web/css/plugins.min.css" rel="stylesheet">
		<!-- Template core CSS-->
		<link href="<?=base_url();?>assets/web/css/template.min.css" rel="stylesheet">
		<!-- Plugins -->
	    <link rel="stylesheet" href="<?=base_url();?>assets/web/plugins/dzsparallaxer/dzsparallaxer.css">
	    <link rel="stylesheet" href="<?=base_url();?>assets/web/plugins/dzsparallaxer/dzsscroller/scroller.css">
	    <link rel="stylesheet" href="<?=base_url();?>assets/web/plugins/dzsparallaxer/advancedscroller/plugin.css">
		<link rel="stylesheet" href="<?=base_url();?>assets/web/css/index.css">
		<link rel="stylesheet" href="<?=base_url();?>assets/web/fonts/font-awesome/css/font-awesome.min.css" type="text/css"/>
	    <link rel="stylesheet" href="<?=base_url();?>assets/web/plugins/preloader/preloader-ax.css">
    	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/web/plugins/slick/css/slick.css" media="screen">
    	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/web/plugins/slick/css/slick-theme.css" media="screen">
    	<link href="<?=base_url();?>assets/web/plugins/fancybox/jquery.fancybox.min.css" rel="stylesheet">
    	<!--
    	<link href="<?=base_url();?>assets/web/css/main.min.css" rel="stylesheet">
    	-->
    	<link href="<?=base_url();?>assets/web/fonts/fontello.css" rel="stylesheet">
	</head>
	<body >
