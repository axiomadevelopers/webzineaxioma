<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CategoriasNoticias extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->model('CategoriasNoticias_model');
      $this->load->model('Auditoria_model');
      $this->load->library('session');
      
      //var_dump($this->session->userdata("login"));die();
        $cms = $_SESSION["cms"];

        if (!$cms["login"]) {  
              redirect(base_url());
        }
    }

    public function index(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
        //--
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
          $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/noticias/categorias_noticias');
        $this->load->view('cpanel/footer');
    }
    public function consultarCategorias(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);

        //--
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
        $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/noticias/consultar_categorias');
        $this->load->view('cpanel/footer');
    }

    public function registrarCategorias(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
          'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
          'estatus' => '1'
        );
        $respuesta = $this->CategoriasNoticias_model->guardarCategoria($data);
        if($respuesta==true){
            $mensajes["mensaje"] = "registro_procesado";
            //-----------------------------------------------------
            //Bloque de auditoria:
            $id = $this->Auditoria_model->consultar_max_id("categorias");
            $accion = "Registro categtorias cms id: ".$id;
            $cms = $_SESSION["cms"];
            $data_auditoria = array(
                                    "id_usuario"=>(integer)$cms["id"],
                                    "modulo"=>'1',
                                    "accion"=>$accion,
                                    "ip"=>$this->Auditoria_model->get_client_ip(),
                                    "fecha_hora"=> date("Y-m-d H:i:00")
            );
            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
            //-----------------------------------------------------
        }else{
            $mensajes["mensaje"] = "no_registro";
        }
        die(json_encode($mensajes));
    }

    public function consultarCategoriasTodas(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->CategoriasNoticias_model->consultarCategoria($datos);
        $res = [];
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $valor->descripcion = mb_strtoupper($value->descripcion);
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }

    public function categoriasNoticiasVer(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);

        //--
        $datos["id"] = $this->input->post('id_categoria');
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
        $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/noticias/categorias_noticias',$datos);
        $this->load->view('cpanel/footer');
    }

    public function modificarCategorias(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
          'id' =>$datos['id'],  
          'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
        );
        //verifico si existe la categoria con ese nombre
        $existe = $this->CategoriasNoticias_model->existeCategoria("",$data['descripcion']);
        if(count($existe)>0){
            if($existe[0]->id != $data["id"]){
                $mensajes["mensaje"] = "existe_nombre";
                die(json_encode($mensajes));
            }else
                $respuesta = $this->CategoriasNoticias_model->modificarCategoria($data);
        }else{
            $respuesta = $this->CategoriasNoticias_model->modificarCategoria($data); 
        }

        if($respuesta==true){
            $mensajes["mensaje"] = "modificacion_procesada";
            //-----------------------------------------------------
            //Bloque de auditoria:
            $accion = "Modificar categtorias cms id: ".$data["id"];
            $cms = $_SESSION["cms"];
            $data_auditoria = array(
                                    "id_usuario"=>(integer)$cms["id"],
                                    "modulo"=>'1',
                                    "accion"=>$accion,
                                    "ip"=>$this->Auditoria_model->get_client_ip(),
                                    "fecha_hora"=> date("Y-m-d H:i:00")
            );
            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
            //-----------------------------------------------------
        }else{
            $mensajes["mensaje"] = "no_modifico";
        }  
       
        die(json_encode($mensajes));
    }

    public function modificarCategoriasEstatus(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
          'id' =>$datos['id'],  
          'estatus' => $datos['estatus'],
        );
        $respuesta = $this->CategoriasNoticias_model->modificarCategoria($data);

        if($respuesta==true){
            $mensajes["mensaje"] = "modificacion_procesada";
            //----------------------------------------------------
            //--Bloque Auditoria 
            switch ($data["estatus"]) {
                case '0':
                    $accion="Inactivar categorias: ".$datos['id'];
                    break;
                case '1':
                    $accion="Activar categorias: ".$datos['id'];
                    break;
                case '2':
                    $accion="Eliminar categorias: ".$datos['id'];
                    break;
            }
            $cms = $_SESSION["cms"];
            $data_auditoria = array(
                                    "id_usuario"=>(integer)$cms["id"],
                                    "modulo"=>'1',
                                    "accion"=>$accion,
                                    "ip"=>$this->Auditoria_model->get_client_ip(),
                                    "fecha_hora"=> date("Y-m-d H:i:00")
            );
            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
            //-----------------------------------------------------
        }else{
            $mensajes["mensaje"] = "no_modifico";
        }  
       
        die(json_encode($mensajes));
    }
}    