<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class GaleriaMultimedia extends CI_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model('GaleriaMultimedia_model');
        $this->load->model('Categorias_model');
        $this->load->model('Auditoria_model');
        $cms = $_SESSION["cms"];
        if (!$cms["login"]) {
                redirect(base_url());
        }

    }

    public function index(){
      //--- Datos de usuario
      $cms = $_SESSION["cms"];
      $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
      //--
      $this->load->view('cpanel/header');
      $this->load->view('cpanel/dashBoard',$data);
      $this->load->view('cpanel/menu',$data);
      $this->load->view('modulos/galeria/galeria_multimedia');
      $this->load->view('cpanel/footer');
    }

    public function upload(){

        /*-----------------------------------------------*/
        /*var_dump($this->input->post());
        var_dump($_FILES["file"]);
        die('');*/
        if($this->input->post()){
            //---------------------------------------------
            $file = $_FILES["file"]["name"];
            $filetype = $_FILES["file"]["type"];
            $filesize = $_FILES["file"]["size"];
            $id_categoria = $_POST["categoria"];
            $tipo = explode("/", $filetype);
            $nombre_archivo = $this->input->post("nombre_archivo").".".$tipo[1];
            $data["id_categoria"] = $this->input->post("categoria");
            $rs_categorias = $this->Categorias_model->consultarCategoria($data);
            $categoria = strtolower($rs_categorias[0]->descripcion);
            $ruta ="assets/images/archivos/".$categoria."/";
            if(!is_dir("assets/images/archivos/".$categoria."/")){
                mkdir($ruta,0777);
            }
            $config['upload_path']          = $ruta;
            $config['allowed_types']        = 'gif|jpg|png|jpeg|JPEG';
            $config['max_size']             = 10000;
            $config['max_width']            = 6000;
            $config['max_height']           = 6000;
            $config['file_name'] = $nombre_archivo;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('file')){
                $error = array('error' => $this->upload->display_errors());
                die(json_encode($error));
            }
            else{
                $data_img = array('upload_data' => $this->upload->data());
                $archivo1 = str_replace(" ","_",$nombre_archivo);
                $data2 = array(
                  'ruta' => $ruta.$archivo1,
                  'id_categoria' => $this->input->post("categoria"),
                  'titulo' => $this->input->post("nombre_archivo"),
                  'estatus' => '1'
                );
                $respuesta = $this->GaleriaMultimedia_model->guardarGaleria($data2);

                if($respuesta==true){
                    $mensajes["mensaje"] = "registro_procesado";
                    //-----------------------------------------------------
                    //Bloque de auditoria:
                    $id = $this->Auditoria_model->consultar_max_id("galeria");
                    $accion = "Registro galeria  id: ".$id;
                    $cms = $_SESSION["cms"]; 
                    $data_auditoria = array(
                                            "id_usuario"=>(integer)$cms["id"],
                                            "modulo"=>'1',
                                            "accion"=>$accion,
                                            "ip"=>$this->Auditoria_model->get_client_ip(),
                                            "fecha_hora"=> date("Y-m-d H:i:00")
                    );
                    $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
                    //-----------------------------------------------------
                }else{
                    $mensajes["mensaje"] = "error";
                }

                die(json_encode($mensajes));
            }
        //---------------------------------------------
        }
        /*-----------------------------------------------*/
    }

    public function consultarGaleria(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
        $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/galeria/consultar_galeria_multimedia');
        $this->load->view('cpanel/footer');
    }

    public function consultarGaleriaCategoria(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->GaleriaMultimedia_model->consultarGaleria($datos);
        $listado = [];
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $vector_url = explode("/",$value->ruta);
            $vector_url2 = explode(".",$vector_url[4]);
            $valor->titulo = strtoupper($vector_url2[0]);
            $listado[]=$valor;
        }
        $listado2 = $listado;
        $total = count($listado2);
        //var_dump($total);die;
        die(json_encode($listado2));
    }

    public function actualizarImagen(){

        $datos= json_decode(file_get_contents('php://input'), TRUE);

        $data2 = array(
                  'titulo' => $datos["descripcion"],
        );

        $respuesta = $this->GaleriaMultimedia_model->actualizarGaleria($data2,$datos["id"]);

        if($respuesta==true){
            $mensajes["mensaje"] = "modificacion_procesada";
        }else{
            $mensajes["mensaje"] = "error";
        }

        die(json_encode($mensajes));
    }

    public function eliminarImagen(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);

        $data2 = array(
                  'estatus' => $datos["estatus"],
        );

        ///$respuesta = $this->GaleriaMultimedia_model->actualizarGaleria($data2,$datos["id"]);
        /*
        *   Eliminar el archivo en la tabla correspondiente
        */

        $categoria = $this->GaleriaMultimedia_model->consultar_categoria ($datos["id"]);
        ($categoria)? $id_categoria = $categoria[0]->id_categoria: $id_categoria ="";
        switch ($id_categoria) {
            /*Portafolio*/
            case '8':
                $tabla_eliminar = "portafolio_imag";
                $this->GaleriaMultimedia_model->eliminarGaleriaHijo($tabla_eliminar,$datos["id"]);
                break;
            case '9':
                $tabla_eliminar ="galeria_clientes";  
                $this->GaleriaMultimedia_model->eliminarGaleriaHijo($tabla_eliminar,$datos["id"]);  
            case '22':
                $tabla_eliminar = "detalle_productos_imag";
                $this->GaleriaMultimedia_model->eliminarGaleriaHijo($tabla_eliminar,$datos["id"]);
            case '4':
                $tabla_eliminar = "seccion_noticias";
                $this->GaleriaMultimedia_model->eliminarGaleriaHijoEnTabla($tabla_eliminar,$datos["id"]);
            case '3':
                $tabla_eliminar = "nosotros";
                $this->GaleriaMultimedia_model->eliminarGaleriaHijoEnTabla($tabla_eliminar,$datos["id"]);   
            case '23':
                $tabla_eliminar = "directiva";
                $this->GaleriaMultimedia_model->eliminarGaleriaHijoEnTabla($tabla_eliminar,$datos["id"]); 
            case '24':
                $tabla_eliminar = "reaseguradoras";
                $this->GaleriaMultimedia_model->eliminarGaleriaHijoEnTabla($tabla_eliminar,$datos["id"]);        
            case '25':
                $tabla_eliminar = "usuarios";
                $this->GaleriaMultimedia_model->eliminarGaleriaHijoEnTabla($tabla_eliminar,$datos["id"]);  
            //Productos    
            case '26':
                $tabla_eliminar = "productos";
                $this->GaleriaMultimedia_model->eliminarGaleriaHijoEnTabla($tabla_eliminar,$datos["id"]);
            //Tipo Productos    
            case '27':
                $tabla_eliminar = "tipo_producto";
                $this->GaleriaMultimedia_model->eliminarGaleriaHijoEnTabla($tabla_eliminar,$datos["id"]);
                //Productos    
            case '7':
                $tabla_eliminar = "servicios";
                $this->GaleriaMultimedia_model->eliminarGaleriaHijoEnTabla($tabla_eliminar,$datos["id"]);
            //Tipo Productos    
            case '28':
                $tabla_eliminar = "tipo_servicios";
                $this->GaleriaMultimedia_model->eliminarGaleriaHijoEnTabla($tabla_eliminar,$datos["id"]);             
            default:
                # code...
                break;
        }
        /*
        *   Eliminar galería multimedia
        */
        $respuesta = $this->GaleriaMultimedia_model->deleteGaleria($datos["id"]);
        if($respuesta==true){
           
            $mensajes["mensaje"] = "eliminacion_procesada";
            //-----------------------------------------------------
            //Bloque de auditoria:
            $accion = "Eliminar galeria  id: ".$datos["id"];
            $cms = $_SESSION["cms"]; 
            $data_auditoria = array(
                                    "id_usuario"=>(integer)$cms["id"],
                                    "modulo"=>'1',
                                    "accion"=>$accion,
                                    "ip"=>$this->Auditoria_model->get_client_ip(),
                                    "fecha_hora"=> date("Y-m-d H:i:00")
            );
            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
            //-----------------------------------------------------
        }else{
            $mensajes["mensaje"] = "error";
        }

        die(json_encode($mensajes));
    }
    
}
