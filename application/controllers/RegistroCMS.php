<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class RegistroCMS extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('RegistroCMS_model');
			$this->load->model('Auditoria_model');
			$cms = $_SESSION["cms"];
			if (!$cms["login"]) {
						redirect(base_url());
			}
			//---Valido que solo el admin pueda ingresar
		    if ($cms["tipo_usuario"]!="1") {
		    	$url_error = base_url()."cms/no_tiene_permisos";
		        redirect($url_error);
		    }
		    //---
		}
		
		public function index(){
			//--- Datos de usuario
      		$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        	//,"ruta_imagen"=>$cms["ruta_imagen"]
      		//--
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard',$data);
      		$this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/registro_cms/registro_cms');
	        $this->load->view('cpanel/footer');
	    }

		public function registrarPersonas(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			$existe1 = $this->RegistroCMS_model->existePersonas($datos['cedula'],$datos['id']);
			$existe2 = $this->RegistroCMS_model->existeUsuarios($datos['login'],$datos['id']);
			//var_dump($existe2,$existe1);die;
			if(!$existe1 and !$existe2){
			$data = array(
				'cedula' => $datos['cedula'],
				'nombres_apellidos' => trim(mb_strtoupper($datos['nombres_apellidos'])) ,
				'estatus' => '1',
				'telefono' => $datos['telefono'],
				'email' => $datos['email'],
				/* 'id_idioma' => sha1($datos['clave']), */
              );
				$respuesta = $this->RegistroCMS_model->guardarPersonas($data);

				if($respuesta==true){
				 $id = $this->RegistroCMS_model->tomarID($datos['cedula']);

					$data1 = array(
						'id_persona' => $id[0]->id,
						'id_tipo_usuario' => $datos['id_tipo_usuario'],
						'login' => $datos['login'],
						'clave' => sha1($datos['clave']),
						'estatus' => '1',
						'id_imagen'=>$datos["id_imagen"],
			  );				
			  //var_dump($data1);die;
			  $respuesta1 = $this->RegistroCMS_model->guardarUsuarios($data1);
			}

				if($respuesta1==true){
					$mensajes["mensaje"] = "registro_procesado";
					//-----------------------------------------------------
		            //Bloque de auditoria:
		            $id = $this->Auditoria_model->consultar_max_id("usuarios");
		            $accion = "Registro usuarios id: ".$id;
		            $cms = $_SESSION["cms"]; 
		            $data_auditoria = array(
		                                    "id_usuario"=>(integer)$cms["id"],
		                                    "modulo"=>'1',
		                                    "accion"=>$accion,
		                                    "ip"=>$this->Auditoria_model->get_client_ip(),
		                                    "fecha_hora"=> date("Y-m-d H:i:00")
		            );
		            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
		            //-----------------------------------------------------
				}else{
					$mensajes["mensaje"] = "no_registro";
				}
			}
			//var_dump($existe1,$existe2);die;

			if($existe1 or $existe2 ){
				if($existe1){
					$mensajes["mensaje"] = "cedula";
				}
				if($existe2){

					$mensajes["mensaje"] = "login";
				}
				if($existe1 and $existe2){
					$mensajes["mensaje"] = "ambos";
				}
		}

			die(json_encode($mensajes));
		}

		public function consultar_usuarios(){
			//--- Datos de usuario
      		$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        	//,"ruta_imagen"=>$cms["ruta_imagen"]
      		//--
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard',$data);
      		$this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/registro_cms/consultar_registro_cms');
	        $this->load->view('cpanel/footer');
	    }

		
	   public function consultarUsuariosTodos(){
		$datos= json_decode(file_get_contents('php://input'), TRUE);
		$respuesta = $this->RegistroCMS_model->consultar_personas($datos);
		//var_dump($respuesta);die;
		foreach ($respuesta as $key => $value) {
			$usuario = $this->RegistroCMS_model->consultar_usuarios($value->id);
			$valor[] = array(
				"id" => $value->id,
				"cedula" => $value->cedula, 
				"nombres_apellidos" => trim(mb_strtoupper($value->nombres_apellidos)),
				"telefono" => $value->telefono,
				"email" => $value->email,
				"login" => $usuario[0]->login,
				"tipo_usuario" => $usuario[0]->tipo_usuario,
				"id_tipo_usuario" => $usuario[0]->id_tipo_usuario,
				"ruta"=> $usuario[0]->ruta,
				"estatus" => $usuario[0]->estatus,
				"id_imagen" => $usuario[0]->id_imagen,
			);

		}
		$listado = (object)$valor;
		//var_dump((object)$valor);die;
		die(json_encode($listado));
	}

	   public function usuariosVer(){
		   $datos["id"] = $this->input->post('id_personas');
		   //--- Datos de usuario
      	   $cms = $_SESSION["cms"];
           $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
		   //,"ruta_imagen"=>$cms["ruta_imagen"]
      	   //--
			//var_dump($datos);die;
		   $this->load->view('cpanel/header');
		   $this->load->view('cpanel/dashBoard',$data);
      	   $this->load->view('cpanel/menu',$data);
		   $this->load->view('modulos/registro_cms/registro_cms',$datos);
		   $this->load->view('cpanel/footer');
	   }

	   public function modificarUsuario(){
		$datos= json_decode(file_get_contents('php://input'), TRUE);
		$existe1 = $this->RegistroCMS_model->existeModificarPersonas($datos['cedula'],$datos['id']);
		$existe2 = $this->RegistroCMS_model->existeModificarUsuarios($datos['login'],$datos['id']);
		//var_dump($existe1,$existe2);die;
			if(!$existe1 and !$existe2){
			$data = array(
				'id' => $datos['id'],
				'cedula' => $datos['cedula'],
				'nombres_apellidos' => trim(mb_strtoupper($datos['nombres_apellidos'])) ,
				'telefono' => $datos['telefono'],
				'email' => $datos['email'],
			);
			$respuesta = $this->RegistroCMS_model->modificarPersonas($data);
			if($respuesta==true){
					$data1 = array(
						'id_persona' => $datos['id'],
						'login' =>$datos['login'],
						'id_imagen'=>$datos["id_imagen"],
					);
			}
			if((isset($datos["clave"]))&&($datos["clave"]!="")){
				$data1["clave"] = sha1($datos['clave']);
			}
			$respuesta1 = $this->RegistroCMS_model->modificarUsuarios($data1);
			if($respuesta1==true){
				$mensajes["mensaje"] = "modificacion_procesada";
				//-----------------------------------------------------
	            //Bloque de auditoria:
	            $accion = "Modificar usuarios id: ".$datos['id'];
	            $cms = $_SESSION["cms"];
	            $data_auditoria = array(
	                                    "id_usuario"=>(integer)$cms["id"],
	                                    "modulo"=>'1',
	                                    "accion"=>$accion,
	                                    "ip"=>$this->Auditoria_model->get_client_ip(),
	                                    "fecha_hora"=> date("Y-m-d H:i:00")
	            );
	            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
	            //-----------------------------------------------------
			}else{
				$mensajes["mensaje"] = "no_registro";
			}
		}

		if($existe1 or $existe2 ){
			if($existe1){
				$mensajes["mensaje"] = "cedula";
			}
			if($existe2){

				$mensajes["mensaje"] = "login";
			}
			if($existe1 and $existe2){
				$mensajes["mensaje"] = "ambos";
			}
		}
		die(json_encode($mensajes));
		}

	   public function modificarUsuarioEstatus(){
		   $datos= json_decode(file_get_contents('php://input'), TRUE);
		   $data = array(
			 'id' =>$datos['id'],
			 'estatus' => $datos['estatus'],
		   );
		  // var_dump($datos);die;
		  $respuesta1 = $this->RegistroCMS_model->modificarUsuarioEstatusUsuario($data);
		  $respuesta = $this->RegistroCMS_model->modificarUsuarioEstatusPersona($data);

		   if($respuesta==true){
			    $mensajes["mensaje"] = "modificacion_procesada";
			    //----------------------------------------------------
	            //--Bloque Auditoria 
	            switch ($data["estatus"]) {
	                case '0':
	                    $accion="Inactivar usuarios: ".$datos['id'];
	                    break;
	                case '1':
	                    $accion="Activar usuarios: ".$datos['id'];
	                    break;
	                case '2':
	                    $accion="Eliminar usuarios: ".$datos['id'];
	                    break;
	            }
	            $cms = $_SESSION["cms"];
	            $data_auditoria = array(
	                                    "id_usuario"=>(integer)$cms["id"],
	                                    "modulo"=>'1',
	                                    "accion"=>$accion,
	                                    "ip"=>$this->Auditoria_model->get_client_ip(),
	                                    "fecha_hora"=> date("Y-m-d H:i:00")
	            );
	            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
	            //-----------------------------------------------------
		   }else{
			   $mensajes["mensaje"] = "no_modifico";
		   }

		   die(json_encode($mensajes));
	   }

	
	   public function consultar_tipos_usuarios(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);

			$respuesta = $this->RegistroCMS_model->consultarTiposUsuarios($datos);

			die(json_encode($respuesta));
		
	   }
	    /*
	    *	Auditoria
	    */
	   	public function auditoria(){
			//--- Datos de usuario
        	$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        	//--
			$this->load->view('cpanel/header');
			$this->load->view('cpanel/dashBoard',$data);
        	$this->load->view('cpanel/menu',$data);
			$this->load->view('modulos/registro_cms/auditoria');
			$this->load->view('cpanel/footer');
		}
		/*
		*	Consultar Auditoria
		*/
		public function consultar_auditoria(){
			$respuesta = $this->Auditoria_model->consultar_auditoria();
			$number = 0;
			foreach ($respuesta as $key => $value) {
				$number = $number+1;
	            $valor[] = array(
								"number" => $number,
								"id" => $value->id,
								"nombre_usuario" => $value->nombre_usuario,
								"modulo" => $value->modulo,
								"accion" => $value->accion,
								"ip" => $value->ip,
								"fecha_hora" => $value->fecha_hora,
				);
	        }
	        $listado = (object)$valor;
	        die(json_encode($listado));
		}

	}
?>
