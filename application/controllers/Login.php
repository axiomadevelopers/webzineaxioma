<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('Login_model');
      $this->load->model('Auditoria_model');
    }

    public function index(){
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoardLogin');
        $this->load->view('modulos/login/login');
        $this->load->view('cpanel/footer');
    }

    public function iniciarSession(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        #Validar campos
        if(($datos["usuario"])&&($datos["clave"])){
            $recordset = $this->Login_model->iniciar_sesion($datos["usuario"],sha1($datos["clave"]));
            if($recordset > 0){
                #Consulto al usuario
                $record_usuario = $this->Login_model->consultar_usuario($datos["usuario"],sha1($datos["clave"]));
                $mensajes["mensajes"] = "inicio_exitoso";
                //--Imagen de usuario
                $id_imagen = $record_usuario[0]->id_imagen;
                if($id_imagen==0){
                    $ruta_imagen = "assets/images/users/user.png";
                }else{
                    $recordImagen = $this->Login_model->consultarImagen($id_imagen);
                    $ruta_imagen = $recordImagen[0]->ruta;
                }
                $data = array(
                    'cms' => array( 
                        'login' => $datos["usuario"],
                        'activo' => true,
                        'id' => $record_usuario[0]->id_persona,
                        'nombre_persona' => $record_usuario[0]->nombres_apellidos,
                        'tipo_usuario'=>$record_usuario[0]->id_tipo_usuario,
                        'ruta_imagen'=>$ruta_imagen
                    )
                );    
                $this->session->set_userdata($data);
                //------------------------------------------------------------
                //--Bloque Auditoria            
                $data_auditoria = array(
                                        "id_usuario"=>(integer)$record_usuario[0]->id_persona,
                                        "modulo"=>'1',
                                        "accion"=>"Inicio de sesion",
                                        "ip"=>$this->Auditoria_model->get_client_ip(),
                                        "fecha_hora"=> date("Y-m-d H:i:00")
                );
                
                $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
                //------------------------------------------------------------
            }else{
                $mensajes["mensajes"] = "error_datos";
            }
            die(json_encode($mensajes));      
        }
    }
    
    public function logout(){
        //------------------------------------------------------------
        //--Bloque Auditoria 
        $cms = $_SESSION["cms"];            
        $data_auditoria = array(
                                "id_usuario"=>(integer)$cms["id"],
                                "modulo"=>'1',
                                "accion"=>"Cerrar de sesion",
                                "ip"=>$this->Auditoria_model->get_client_ip(),
                                "fecha_hora"=> date("Y-m-d H:i:00")
        );
        
        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
        //------------------------------------------------------------
        $this->session->unset_userdata("cms");
        redirect(base_url()."cms");
    }   
}  