<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Inicio extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('inicio_model');
      $cms = $_SESSION["cms"];
      if (!$cms["login"]) {      
          redirect(base_url());
      }
    }

    public function index(){

     // var_dump($_SESSION);die;
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
            
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
        $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/login/inicio');
        $this->load->view('cpanel/footer');
    }
}  