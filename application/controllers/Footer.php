<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Footer extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('Contactos_model');
			$this->load->model('Auditoria_model');
			$cms = $_SESSION["cms"];
			if (!$cms["login"]) {
					redirect(base_url());
			}
		}

		public function index(){
			 //--- Datos de usuario
        	$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        	//--
			$this->load->view('cpanel/header');
			$this->load->view('cpanel/dashBoard',$data);
        	$this->load->view('cpanel/menu',$data);
			$this->load->view('modulos/footer/footer');
			$this->load->view('cpanel/footer');
		}

		public function registrarFooter(){
	        $datos= json_decode(file_get_contents('php://input'), TRUE);
	        $data = array(
	          'correo' => trim(strtolower($datos['correo'])),
	          'descripcion' => trim($datos['descripcion']),
	          'telefono' => $datos['telefono'],
	          'estatus' => '1',
	          'id_idioma' => $datos['id_idioma'],
	        );
	        $respuesta = $this->Contactos_model->guardarFooter($data);
	        if($respuesta==true){
	            $mensajes["mensaje"] = "registro_procesado";
	            //-----------------------------------------------------
	            //Bloque de auditoria:
	            $id = $this->Auditoria_model->consultar_max_id("footer");
	            $accion = "Registro footer id: ".$id;
	            $cms = $_SESSION["cms"];
	            $data_auditoria = array(
	                                    "id_usuario"=>(integer)$cms["id"],
	                                    "modulo"=>'1',
	                                    "accion"=>$accion,
	                                    "ip"=>$this->Auditoria_model->get_client_ip(),
	                                    "fecha_hora"=> date("Y-m-d H:i:00")
	            );
	            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
	            //-----------------------------------------------------
	        }else{
	            $mensajes["mensaje"] = "no_registro";
	        }
	        die(json_encode($mensajes));
	    }

	    public function modificarFooter(){
	    	$datos = json_decode(file_get_contents('php://input'), TRUE);
	        //-Verifico si existe una noticia con ese titulo....
	        $existe_footer = $this->Contactos_model->consultarExiste($datos["id"]);
	        if($existe_footer>0){
	            $data = array(
	              'id' =>  $datos['id'],
	              'correo' => trim(strtolower($datos['correo'])),
	              'descripcion' => trim($datos['descripcion']),
	              'telefono' => $datos['telefono'],
	              'estatus' => '1',
	              'id_idioma' => $datos['id_idioma']
	            );
	            $respuesta = $this->Contactos_model->modificarFooter($data);
	            if($respuesta==true){
	                $mensajes["mensaje"] = "registro_procesado";
	                 //-----------------------------------------------------
		            //Bloque de auditoria:
		            $id = $this->Auditoria_model->consultar_max_id("footer");
		            $accion = "Actualizar footer id: ".$id;
		            $cms = $_SESSION["cms"];
		            $data_auditoria = array(
		                                    "id_usuario"=>(integer)$cms["id"],
		                                    "modulo"=>'1',
		                                    "accion"=>$accion,
		                                    "ip"=>$this->Auditoria_model->get_client_ip(),
		                                    "fecha_hora"=> date("Y-m-d H:i:00")
		            );
		            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
		            //-----------------------------------------------------
	            }else{
	                $mensajes["mensaje"] = "no_registro";
	            }
	        }else{
	             $mensajes["mensaje"] = "no_existe";
	        }
        	//--
       		 die(json_encode($mensajes));
	    }

	    public function modificarFooterEstatus(){
	    	$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $data = array(
	          'id' =>$datos['id'],  
	          'estatus' => $datos['estatus'],
	        );
	        $respuesta = $this->Contactos_model->modificarFooter($data);

	        if($respuesta==true){
	            $mensajes["mensaje"] = "modificacion_procesada";
	            //----------------------------------------------------
	            //--Bloque Auditoria 
	            switch ($data["estatus"]) {
	                case '0':
	                    $accion="Inactivar footer: ".$datos['id'];
	                    break;
	                case '1':
	                    $accion="Activar footer: ".$datos['id'];
	                    break;
	                case '2':
	                    $accion="Eliminar footer: ".$datos['id'];
	                    break;
	            }
	            $cms = $_SESSION["cms"];
	            $data_auditoria = array(
	                                    "id_usuario"=>(integer)$cms["id"],
	                                    "modulo"=>'1',
	                                    "accion"=>$accion,
	                                    "ip"=>$this->Auditoria_model->get_client_ip(),
	                                    "fecha_hora"=> date("Y-m-d H:i:00")
	            );
	            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
	            //-----------------------------------------------------
	        }else{
	            $mensajes["mensaje"] = "no_modifico";
	        }  
	        die(json_encode($mensajes));
	    }
	    
	    public function consultarFooter(){
	    	 //--- Datos de usuario
        	$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        	//--
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard',$data);
       		$this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/footer/consultar_footer');
	        $this->load->view('cpanel/footer');
	    }
	    
	    public function consultarFooterTodas(){
	    	$res = [];
	        $datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Contactos_model->consultarFooter($datos);
	        foreach ($respuesta as $key => $value) {
	            $valor = $value;
	            //$valor->descripcion_sin_html = strip_tags($value->descripcion);
	            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
	            $res[] = $valor;
	        }
	        $listado = (object)$res;
	        die(json_encode($listado));
	    }

	    public function footerVer(){
	    	//--- Datos de usuario
        	$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
       	 	//--
	        $datos["id"] = $this->input->post('id_footer');
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard',$data);
        	$this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/footer/footer',$datos);
	        $this->load->view('cpanel/footer');
	    }

	}	