<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Empresa extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->model('Empresa_model');
      $this->load->model('Auditoria_model');
      $this->load->library('session');
      
      //var_dump($this->session->userdata("login"));die();
        $cms = $_SESSION["cms"];

        if (!$cms["login"]) {  
              redirect(base_url());
        }
    }

    public function index(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
        //--
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
          $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/empresa/empresa');
        $this->load->view('cpanel/footer');
    }

    public function registrarEmpresa(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $cms = $_SESSION["cms"]; 
        $data = array(
          'titulo' => trim(mb_strtoupper($datos['titulo'])),
          'subtitulo' => trim(mb_strtoupper($datos['subtitulo'])),
          'id_imagen' => $datos['id_imagen'],
          'descripcion' => trim($datos['descripcion']),
          'estatus' => '1',
          'id_idioma' => $datos['id_idioma'],
        );
        $respuesta = $this->Empresa_model->guardarEmpresa($data);
        if($respuesta==true){
            $mensajes["mensaje"] = "registro_procesado";
            //-----------------------------------------------------
            //Bloque de auditoria:
            $id = $this->Auditoria_model->consultar_max_id("empresa");
            $accion = "Registro empresa id: ".$id." titulo: ".trim(mb_strtoupper($datos['titulo']));
            $cms = $_SESSION["cms"]; 
            $data_auditoria = array(
                                    "id_usuario"=>(integer)$cms["id"],
                                    "modulo"=>'1',
                                    "accion"=>$accion,
                                    "ip"=>$this->Auditoria_model->get_client_ip(),
                                    "fecha_hora"=> date("Y-m-d H:i:00")
            );
            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
            //-----------------------------------------------------
        }else{
            $mensajes["mensaje"] = "no_registro";
        }
        die(json_encode($mensajes));
    }

    public function modificarEmpresa(){
        $datos = json_decode(file_get_contents('php://input'), TRUE);
        //-Verifico si existe una noticia con ese titulo....
        $existe_titulo = $this->Empresa_model->consultarExisteTitulo($datos["id"],$datos["titulo"]);
        if($existe_titulo==0){
            $data = array(
              'id' =>  $datos['id'],
              'titulo' => trim(mb_strtoupper($datos['titulo'])),
              'subtitulo' => trim(mb_strtoupper($datos['subtitulo'])),
              'id_imagen' => $datos['id_imagen'],
              'descripcion' => trim($datos['descripcion']),
              'estatus' => '1',
              'id_idioma' => $datos['id_idioma'],
            );
            $respuesta = $this->Empresa_model->modificarEmpresa($data);
            if($respuesta==true){
                $mensajes["mensaje"] = "registro_procesado";
                //-----------------------------------------------------
                //Bloque de auditoria:
                $accion = "Actualizar empresa id: ".$datos['id']." titulo: ".trim(mb_strtoupper($datos['titulo']));
                $cms = $_SESSION["cms"]; 
                $data_auditoria = array(
                                        "id_usuario"=>(integer)$cms["id"],
                                        "modulo"=>'1',
                                        "accion"=>$accion,
                                        "ip"=>$this->Auditoria_model->get_client_ip(),
                                        "fecha_hora"=> date("Y-m-d H:i:00")
                );
                $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
                //-----------------------------------------------------
            }else{
                $mensajes["mensaje"] = "no_registro";
            }
        }else{
             $mensajes["mensaje"] = "existe";
        }
        //--
        die(json_encode($mensajes));
    }

    public function consultarEmpresa(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
        $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/empresa/consultar_empresa');
        $this->load->view('cpanel/footer');
    }

    public function consultarEmpresaTodas(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->Empresa_model->consultarEmpresa($datos);
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }

    public function normaliza ($cadena){
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        //$cadena = utf8_decode($cadena);
        //$cadena = strtr($cadena, utf8_decode($originales), $modificadas);
        $cadena = strtr($cadena, $originales, $modificadas);
        $cadena = strtolower($cadena);
        //return utf8_encode($cadena);
        return $cadena;
    }

    public function modificarEmpresaEstatus(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
          'id' =>$datos['id'],  
          'estatus' => $datos['estatus'],
        );
        $respuesta = $this->Empresa_model->modificarEmpresa($data);

        if($respuesta==true){
            $mensajes["mensaje"] = "modificacion_procesada";
            //----------------------------------------------------
            //--Bloque Auditoria 
            switch ($data["estatus"]) {
                case '0':
                    $accion="Inactivar empresa: ".$datos['id'];
                    break;
                case '1':
                    $accion="Activar empresa: ".$datos['id'];
                    break;
                case '2':
                    $accion="Eliminar empresa: ".$datos['id'];
                    break;
            }
            $cms = $_SESSION["cms"]; 
            $data_auditoria = array(
                                    "id_usuario"=>(integer)$cms["id"],
                                    "modulo"=>'1',
                                    "accion"=>$accion,
                                    "ip"=>$this->Auditoria_model->get_client_ip(),
                                    "fecha_hora"=> date("Y-m-d H:i:00")
            );
            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
            //-----------------------------------------------------
        }else{
            $mensajes["mensaje"] = "no_modifico";
        }  
       
        die(json_encode($mensajes));
    }

    public function empresaVer(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
        $datos["id"] = $this->input->post('id_empresa');
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
        $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/empresa/empresa',$datos);
        $this->load->view('cpanel/footer');
    }
   
}    
