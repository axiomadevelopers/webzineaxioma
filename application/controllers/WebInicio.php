<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebInicio extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->model('WebInicio_model');
    }
    /*
    * Index
    */
    public function index(){
      $idioma = 1;
      $datos= [];
      //---
      $this->load->view('/web/header');
      $this->load->view('/web/menu');
      $this->load->view('/web/inicio',$datos);
      $this->load->view('/web/footer');
    }
    
    /*
    * registrarContactos
    */
    public function registrarContactos(){
        $datos = json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
            "nombres" => $datos["nombres"],
            "telefono" => $datos["telefono"],
            "email" => $datos["email"],
            "mensaje" => $datos["mensaje"]
        );
        $respuesta = $this->WebInicio_model->guardarContactos($data);
        if($respuesta==true){
            $mensajes["mensaje"] = "registro_procesado";
        }else{
            $mensajes["mensaje"] = "no_registro";
        }
        die(json_encode($mensajes));
    }
    
    /***/
    public function consultarPublicidad(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebInicio_model->consultarPublicidad($datos);
        foreach ($respuesta as $key => $value) {
    
            $valor = $value;
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,130)."...";
    
            $res[] = $valor;
        }
        $listado = (object)$res;
    
        die(json_encode($listado));
    } 
    /***/
    public function consultarCategorias(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebInicio_model->consultarCategorias($datos);
        $n = 1;
        foreach ($respuesta as $key => $value) {
            $value->color = "header-bloque".$n;
            $valor = $value;
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,130)."...";
    
            $res[] = $valor;
            $n++;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }
    /*
    *   COnsulta de datos de la empresa
    */
    public function consultarEmpresa(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebInicio_model->consultarEmpresa($datos);
        $listado = (object)$respuesta;
        die(json_encode($listado));
    }
    /***/
    public function consultarPortada(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebInicio_model->consultarPortada($datos);
        $listado = (object)$respuesta;
        die(json_encode($listado)); 
    }
    public function consultarFooter(){
      $res = [];
      $datos= json_decode(file_get_contents('php://input'), TRUE);
      $respuesta = $this->WebInicio_model->consultarFooter($datos);
      foreach ($respuesta as $key => $value) {
          $valor = $value;
          //$valor->descripcion_sin_html = strip_tags($value->descripcion);
          $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
          $res[] = $valor;
      }
      $listado = (object)$res;
      die(json_encode($listado));
    }
    /*
    * consultarRedes
    */
    public function consultarRedes(){
        $datos = json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebInicio_model->consultarRedes($datos);
        $listado = (object)$respuesta;
        die(json_encode($listado));
    }
    /*
    *   consultarMetaTag
    */
    public function consultarMetaTag(){
        $datos = json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = array("descripcion"=>"","palabras_claves"=>"");
        $arr_descripcion = $this->WebInicio_model->consultarDescripcion();
        $arr_palabras_claves = $this->WebInicio_model->consultarPalabrasClaves();
        
        if(isset($arr_descripcion[0]->descripcion))
             $respuesta["descripcion"]= $arr_descripcion[0]->descripcion;
        
        if(isset($arr_palabras_claves[0]->palabras_claves)) 
            $respuesta["palabras_claves"]= $arr_palabras_claves[0]->palabras_claves;
        
        //$respuesta["lineas"] = $this->WebInicio_model->consultarLinea($datos);
        $listado = (object)$respuesta;
        die(json_encode($listado));
    }
    /*
    * consultarDireccion
    */
    public function consultarDireccion(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebInicio_model->consultarDireccion($datos);
        $a = 1;
        foreach ($respuesta as $key => $value) {
         $valor = $value;
         $telefonos = $this->WebInicio_model->consultarTelefonos($value->id);
         $valor->telefono = "";
         if($telefonos){
            foreach ($telefonos as $key_tlf => $value_tlf) {
              $tlf_arr[] = $value_tlf->telefono; 
            }
            $valor->telefono = $tlf_arr;
            $tlf_arr = [];
         }

         //$valor->descripcion_sin_html = strip_tags($value->descripcion);
         $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

         $res[] = $valor;
         $a++;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }
    /*
    *   error404
    */
    public function error404(){
        $idioma = 1;
        //---
        $datos="";
        $this->load->view('/web/header');
        $this->load->view('/web/error404',$datos);
        $this->load->view('/web/footer');
    }
}    