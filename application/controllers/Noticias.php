<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Noticias extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->model('Noticias_model');
      $this->load->model('Auditoria_model');
      $this->load->library('session');
      
      //var_dump($this->session->userdata("login"));die();
        $cms = $_SESSION["cms"];

        if (!$cms["login"]) {  
              redirect(base_url());
        }
    }

    public function index(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
        //--
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
          $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/noticias/noticias');
        $this->load->view('cpanel/footer');
    }

    public function registrarNoticias(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $cms = $_SESSION["cms"]; 
        $fecha = strtotime($datos['fecha']);
        if ($fecha === FALSE) {
          $fecha = strtotime(str_replace('/', '-', $datos['fecha']));
        }
        $data = array(
          'titulo' => trim(mb_strtoupper($datos['titulo'])),
          'id_imagen' => $datos['id_imagen'],
          'descripcion' => trim($datos['descripcion']),
          'estatus' => '1',
          'fecha' => date('Y-m-d',$fecha),
          'id_usuario' => $cms["id"],
          'slug' =>$this->generarSlug($datos["titulo"]),
          'id_idioma' => $datos['id_idioma'],
          'id_categoria'=>$datos["id_categoria"],
          'link_youtube'=>$datos["link_youtube"],
          'link_soundcloud'=>$datos["link_soundcloud"],
          'editorial'=>$datos["editorial"],
          'miniatura'=>$datos["miniatura"]
        );
        $respuesta = $this->Noticias_model->guardarNoticias($data);
        if($respuesta==true){
            $mensajes["mensaje"] = "registro_procesado";
            //-----------------------------------------------------
            //Bloque de auditoria:
            $id = $this->Auditoria_model->consultar_max_id("seccion_noticias");
            $accion = "Registro noticia id: ".$id." titulo: ".trim(mb_strtoupper($datos['titulo']));
            $cms = $_SESSION["cms"]; 
            $data_auditoria = array(
                                    "id_usuario"=>(integer)$cms["id"],
                                    "modulo"=>'1',
                                    "accion"=>$accion,
                                    "ip"=>$this->Auditoria_model->get_client_ip(),
                                    "fecha_hora"=> date("Y-m-d H:i:00")
            );
            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
            //-----------------------------------------------------
        }else{
            $mensajes["mensaje"] = "no_registro";
        }
        die(json_encode($mensajes));
    }

    public function modificarNoticias(){
        $datos = json_decode(file_get_contents('php://input'), TRUE);
        //-Verifico si existe una noticia con ese titulo....
        $existe_titulo = $this->Noticias_model->consultarExisteTitulo($datos["id"],$datos["titulo"]);
        if($existe_titulo==0){
            $data = array(
              'id' =>  $datos['id'],
              'titulo' => trim(mb_strtoupper($datos['titulo'])),
              'id_imagen' => $datos['id_imagen'],
              'descripcion' => trim($datos['descripcion']),
              'estatus' => '1',
              'fecha' => date("Y-m-d"),
              'slug' =>$this->generarSlug($datos["titulo"]),
              'id_idioma' => $datos['id_idioma'],
              'id_categoria'=>$datos["id_categoria"],
              'link_youtube'=>$datos["link_youtube"],
              'link_soundcloud'=>$datos["link_soundcloud"],
              'editorial'=>$datos["editorial"],
              'miniatura'=>$datos["miniatura"]
            );
            $respuesta = $this->Noticias_model->modificarNoticias($data);
            if($respuesta==true){
                $mensajes["mensaje"] = "registro_procesado";
                //-----------------------------------------------------
                //Bloque de auditoria:
                $accion = "Actualizar noticia id: ".$datos['id']." titulo: ".trim(mb_strtoupper($datos['titulo']));
                $cms = $_SESSION["cms"]; 
                $data_auditoria = array(
                                        "id_usuario"=>(integer)$cms["id"],
                                        "modulo"=>'1',
                                        "accion"=>$accion,
                                        "ip"=>$this->Auditoria_model->get_client_ip(),
                                        "fecha_hora"=> date("Y-m-d H:i:00")
                );
                $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
                //-----------------------------------------------------
            }else{
                $mensajes["mensaje"] = "no_registro";
            }
        }else{
             $mensajes["mensaje"] = "existe";
        }
        //--
        die(json_encode($mensajes));
    }

    public function consultarNoticias(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
        $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/noticias/consultar_noticias');
        $this->load->view('cpanel/footer');
    }

    public function consultarNoticiasTodas(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->Noticias_model->consultarNoticias($datos);
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            if($valor->editorial=="1"){
                $valor->tipo = "Editorial";
            }else{
                $valor->tipo = "Post";
            }
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            $vector_fecha = explode("-", $valor->fecha);
            $valor->fecha = $vector_fecha[2]."/".$vector_fecha[1]."/".$vector_fecha[0];
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }

    public function generarSlug($cadena){
        $titulo_min = strtolower($this->normaliza($cadena));
        $slug_noticias = str_replace(" ","-",$titulo_min);
        $slug_noticias = preg_replace("/[^a-zA-Z0-9_-]+/", "", $slug_noticias);
        return $slug_noticias;
    }

    public function normaliza ($cadena){
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        //$cadena = utf8_decode($cadena);
        //$cadena = strtr($cadena, utf8_decode($originales), $modificadas);
        $cadena = strtr($cadena, $originales, $modificadas);
        $cadena = strtolower($cadena);
        //return utf8_encode($cadena);
        return $cadena;
    }

    public function modificarNoticiasEstatus(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
          'id' =>$datos['id'],  
          'estatus' => $datos['estatus'],
        );
        $respuesta = $this->Noticias_model->modificarNoticias($data);

        if($respuesta==true){
            $mensajes["mensaje"] = "modificacion_procesada";
            //----------------------------------------------------
            //--Bloque Auditoria 
            switch ($data["estatus"]) {
                case '0':
                    $accion="Inactivar noticia: ".$datos['id'];
                    break;
                case '1':
                    $accion="Activar noticia: ".$datos['id'];
                    break;
                case '2':
                    $accion="Eliminar noticia: ".$datos['id'];
                    break;
            }
            $cms = $_SESSION["cms"]; 
            $data_auditoria = array(
                                    "id_usuario"=>(integer)$cms["id"],
                                    "modulo"=>'1',
                                    "accion"=>$accion,
                                    "ip"=>$this->Auditoria_model->get_client_ip(),
                                    "fecha_hora"=> date("Y-m-d H:i:00")
            );
            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
            //-----------------------------------------------------
        }else{
            $mensajes["mensaje"] = "no_modifico";
        }  
       
        die(json_encode($mensajes));
    }

    public function noticiasVer(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
        $datos["id"] = $this->input->post('id_noticias');
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
        $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/noticias/noticias',$datos);
        $this->load->view('cpanel/footer');
    }
   
}    
