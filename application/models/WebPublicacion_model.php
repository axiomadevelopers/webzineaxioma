<?php

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class WebPublicacion_model extends CI_Model{
	
	function consultarNoticias($data){
		//var_dump($data);Die;
		$this->db->order_by('a.id', 'DESC');

		if($data["editorial"]=="1"){
			$this->db-> limit(1);
			$this->db->where('a.editorial',1);
		}else{
			$this->db->where('a.editorial!=',1);
		}
		
		if($data["slug"]!=""){
			$this->db->where('a.slug',$data["slug"]);
		}

		$this->db-> where('a.estatus',1);
    	$this->db-> select('a.*, b.ruta as ruta, b.id as id_imagen, c.login as autor, e.descripcion as categoria_noticia');
    	$this->db-> from('seccion_noticias a');
		$this->db->join('idioma d', 'd.id = a.id_idioma');
    	$this->db-> join('galeria b', 'b.id = a.id_imagen');
        $this->db-> join('usuarios c', 'c.id = a.id_usuario');
        $this->db-> join('categorias_noticias e', 'e.id = a.id_categoria');

		

		$res = $this->db-> get();
        
        //var_dump($this->db->last_query());echo"<br>";

        if($res) {
			return $res-> result();
		} else {
			return false;
		}
	}
	/*
	*	consultarNoticiasFiltros
	*/
	function consultarNoticiasFiltros($data){
		//var_dump($data);Die;
		$this->db->order_by('a.id', 'DESC');

		$this->db-> limit($data["offset"],$data["limit"]);
		$this->db->where('a.id!=',$data["no_id"]);
		$this->db->where('a.editorial!=',1);
		$this->db-> where('a.estatus',1);
    	$this->db-> select('a.*, b.ruta as ruta, b.id as id_imagen, c.login as autor, e.descripcion as categoria_noticia');
    	$this->db-> from('seccion_noticias a');
		$this->db->join('idioma d', 'd.id = a.id_idioma');
    	$this->db-> join('galeria b', 'b.id = a.id_imagen');
        $this->db-> join('usuarios c', 'c.id = a.id_usuario');
        $this->db-> join('categorias_noticias e', 'e.id = a.id_categoria');


		$res = $this->db-> get();
        
        //var_dump($this->db->last_query());echo"<br>";

        if($res) {
			return $res-> result();
		} else {
			return false;
		}
	}
	/***/
	public function consultarNoticiasBuscador($datos){
		
		//var_dump($data);Die;
		$this->db->order_by('a.id', 'DESC');

		if($datos["tipo"]=="categorias"){
			$this->db->where('a.id_categoria',$datos["id"]);
		}

		if($datos["tipo"]=="titulos"){
			$this->db->like('LOWER(a.titulo)',strtolower($datos["id"]));
		}
		$this->db-> limit($datos["limit"],$datos["start"]);
		$this->db-> where('a.estatus',1);
    	$this->db-> select('a.*, b.ruta as ruta, b.id as id_imagen, c.login as autor, e.descripcion as categoria_noticia');
    	$this->db-> from('seccion_noticias a');
		$this->db->join('idioma d', 'd.id = a.id_idioma');
    	$this->db-> join('galeria b', 'b.id = a.id_imagen');
        $this->db-> join('usuarios c', 'c.id = a.id_usuario');
        $this->db-> join('categorias_noticias e', 'e.id = a.id_categoria');


		$res = $this->db-> get();
        
        //var_dump($this->db->last_query());echo"<br>";

        if($res) {
			return $res-> result();
		} else {
			return false;
		}
	}
	/*
	*	Slug existe
	*/
	public function consultarPublicacionSlugExiste($slug){
		if($slug!=""){
			$this->db->where('a.slug',$slug);
		}
		
		$this->db->where('a.estatus=',1);
		$this->db->select('a.*');
		$this->db->from('seccion_noticias a');
		$this->db->join('idioma d', 'd.id = a.id_idioma');
    	$this->db-> join('galeria b', 'b.id = a.id_imagen');
        $this->db-> join('usuarios c', 'c.id = a.id_usuario');
        $this->db-> join('categorias_noticias e', 'e.id = a.id_categoria');

		return $this->db->count_all_results();
		//$res = $this->db->get();
		//print_r($this->db->last_query());die;
	}
}