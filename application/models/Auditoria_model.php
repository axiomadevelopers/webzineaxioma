<?php 

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class Auditoria_model extends CI_Model{

	/*
	*	Para guardar la auditoria
	*/
	public function guardarAuditoria($data){
		if ($this->db->insert("auditoria", $data)){
			return true;
		}else{
		 	return false;
		}
	}

	/*
	*	Obtengo la ip del equipo
	*/
	public function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
	/*
    *   Consulta max id de la tabla
    */
    public function consultar_max_id($tabla){
        $this->db->select_max('id');
        $res1 = $this->db->get($tabla);
        if ($res1->num_rows() > 0){
            $res2 = $res1->result_array();
            $result = $res2[0]['id'];
            return $result;
        }else{
            return 0;
        }
            
    }
    /*
    *   Consultar auditoria
    */
    public function consultar_auditoria(){
        $this->db->order_by('a.id','DESC');
        $this->db->select(' a.id, 
                            b.login as nombre_usuario,
                            c.descripcion as modulo,
                            a.accion,
                            a.ip,
                            a.fecha_hora');
        $this->db->from('auditoria a');
        $this->db->join('usuarios b', 'b.id = a.id_usuario');
        $this->db->join('modulos c', 'c.id = a.modulo');
        //$this->db->join('galeria c', 'c.id = a.id_imagen');
        $res = $this->db->get();

        if($res){
            return $res->result();
        }else{
            return false;
        }
    }
}