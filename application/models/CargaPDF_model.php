<?php 

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class CargaPDF_model extends CI_Model{

	public function guardarPdf	($data){
		if($this->db->insert("carga_pdfs", $data)){
			return true;
		}else{
			return false;
		}
	}

	public function consultarExisteTitulo($id,$titulo){
		$this->db->where('n.id_categoria',$id);
		$this->db->where('n.titulo',$titulo);
		$this->db->where('estatus','1');
		$this->db->select('*');
		$this->db->from('carga_pdfs n');
		return $this->db->count_all_results();
	}

	public function consultarExiste($id,$id_categoria,$titulo){
		$this->db->where('n.id!=',$id);
		$this->db->where('n.id_categoria',$id_categoria);
		$this->db->where('n.titulo',$titulo);
		$this->db->where('estatus','1');
		$this->db->select('*');
		$this->db->from('carga_pdfs n');
		return $this->db->count_all_results();
	}

	public function consultarPDF($data){
		if($data["id_pdf"]!=""){
			$this->db->where('a.id', $data["id_pdf"]);
		}
        $this->db->where('a.estatus!=',2);
        $this->db->order_by('a.id','ASC');
		$this->db->select('a.id,a.ruta,a.estatus,a.id_categoria,a.descripcion,a.titulo,c.descripcion as descripcion_categoria');
		$this->db->from('carga_pdfs a');
		$this->db->join('categorias c', 'a.id_categoria = c.id');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	public function modificarPDF($data){
		$this->db->where('id', $data["id"]);
        if($this->db->update("carga_pdfs", $data)){
        	return true;
        }else{
        	return false;
        }
	}
	public function deleteGaleria($id){
		$this->db->where('id', $id);
		$this->db->delete('galeria');
		return true;
	}
	public function consultar_categoria($id){
		$this->db->where('a.id',$id);
        $this->db->order_by('a.id','DESC');
		$this->db->select('a.id_categoria');
		$this->db->from('galeria a');
		$res = $this->db->get();
        //print_r($this->db->last_query());die;

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	Eliminar tabla de galeria hijos
	*/
	public function eliminarGaleriaHijo($tabla,$id_imagen){
		$this->db->where('id_imagen', $id_imagen);
		$this->db->delete($tabla);
		return true;
	}
	/*
	*	Elimino la imagen en tabla colocando el id imagen como 0
	*/
	public function eliminarGaleriaHijoEnTabla($tabla,$id_imagen){

		$data =array(
                  'id_imagen' => 0,
        );

		$this->db->where('id_imagen', $id_imagen);
        $this->db->update($tabla, $data);
        return true;
	}
	/***/
}	