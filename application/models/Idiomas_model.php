<?php 

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class Idiomas_model extends CI_Model{

	public function consultarIdiomas($data){
		$this->db->order_by('a.id');
		$this->db->select('a.id,a.descripcion');
		$this->db->from('idioma a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
		
	}
}	