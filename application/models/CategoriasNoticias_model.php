<?php 

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class CategoriasNoticias_model extends CI_Model{
	
	public function iniciar_sesion($login,$clave){
		$this->db->where('login',$login);
		$this->db->where('clave',$clave);
		$this->db->where('estatus','1');
		$this->db->select('*');
		$this->db->from(' usuarios u');
		return $this->db->count_all_results();
	}

	public function guardarCategoria($data){
		if($this->db->insert("categorias_noticias", $data)){
			return true;
		}else{
			return false;
		}
	}

	public function modificarCategoria($data){
		$this->db->where('id', $data["id"]);
        if($this->db->update("categorias_noticias", $data)){
        	return true;
        }else{
        	return false;
        }

	}

	public function consultarCategoria($data){

		if($data["id_categoria"]!=""){
			$this->db->where('a.id', $data["id_categoria"]);
		}
		$this->db->order_by('a.id');
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.id,a.descripcion,a.estatus');
		$this->db->from('categorias_noticias a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}

	}

	public function existeCategoria($id,$descripcion){

		if($id!=""){
			$this->db->where('a.id', $id);
		}

		if($descripcion!=""){
			$this->db->where('a.descripcion', $descripcion);
		}

		$this->db->select('a.id,a.descripcion,a.estatus');
		$this->db->from('categorias_noticias a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
}