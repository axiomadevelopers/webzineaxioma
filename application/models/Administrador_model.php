<?php 

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class Administrador_model extends CI_Model{

	public function iniciar_sesion($login,$clave){
		$this->db->where('login',$login);
		$this->db->where('clave',$clave);
		$this->db->where('estatus','1');
		$this->db->select('*');
		$this->db->from(' usuarios u');
		return $this->db->count_all_results();
	}
}