<?php
	if (!defined('BASEPATH')) exit ('No direct script access allowed');

	class Direccion_model extends CI_Model{

		public function guardarDireccion($data){
			//print_r ($data);die;
			if($this->db->insert("direcciones",$data)){
				return true;
			}else{
				return false;
			}
		}
		/*
		*	Existe telefono
		*/
		public function existe_telefono_direccion($id_direccion,$telefono){
			$this->db->where('id_direccion',$id_direccion);
			$this->db->where('telefono',$telefono);
			$this->db->select('*');
			$this->db->from(' telefonos a');
			return $this->db->count_all_results();
		}
		/*
		*	Para registrar telefonos
		*/
		public function registrar_telefono($data){

			if($this->db->insert("telefonos",$data)){
				return true;
			}else{
				return false;
			}
		}
		/*
		*
		*/
		public function consultarDireccion($data){
			if($data["id_direccion"]!=""){
				$this->db->where('a.id', $data["id_direccion"]);
			}
			$this->db->order_by('a.id','DESC');
	        $this->db->where('a.estatus!=',2);
			//$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
			$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma');
			$this->db->from('direcciones a');
			$this->db->join('idioma b', 'b.id = a.id_idioma');
	        //$this->db->join('galeria c', 'c.id = a.id_imagen');
			$res = $this->db->get();

			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultarExiste($id){
			if($id!=""){
				$this->db->where('a.id', $id);
			}
			$this->db->select('a.*');
			$this->db->from('direcciones a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function modificarDireccion($data){
			$this->db->where('id', $data["id"]);
	        if($this->db->update("direcciones", $data)){
	        	return true;
	        }else{
	        	return false;
	        }
		}

		public function modificarDireccionEstatus($data){
			$this->db->where('id', $data["id"]);
	        if($this->db->update("direcciones", $data)){
	        	//------------------------------------
	        	if(isset($data["estatus"])){
	        		if($data["estatus"]=="2"){
		        		$rs_idioma = $this->consultarIdiomaRegistro($data["id"]);
		        		$id_idioma = (integer)$rs_idioma[0]->id_idioma;
		        		$this->reiniciar_orden($id_idioma);
		        	}
	        	}
	        	//------------------------------------
	        	return true;
	        }else{
	        	return false;
	        }
		}

		public function consultarDireccion_idioma($data){
			if($data["id_idioma"]!=""){
				$this->db->where('a.id_idioma', $data["id_idioma"]);
			}
			$this->db->order_by('a.id','DESC');
	        $this->db->where('a.estatus!=',2);
			$this->db->select('a.id, a.descripcion, a.titulo');
			$this->db->from('marcas a');
			$res = $this->db->get();

			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		//-----------------------------------------------------------
		public function consultarTelefonos($id_direccion){
			$this->db->order_by('a.id','ASC');
	        $this->db->where('a.id_direccion',$id_direccion);
			$this->db->select('a.*');
			$this->db->from('telefonos a');
			
			$res = $this->db->get();
			
			//print_r($this->db->last_query());die;
			
			if($res->num_rows()>0){
				return $res->result();
			}else{
				return false;
			}
		}
		//------------------------------------------------------------
		public function consultarOrden($data){
		
			if($data["id_idioma"]!=""){
				$this->db->where('a.id_idioma', $data["id_idioma"]);
			}
			
			$this->db->order_by('a.orden','ASC');
	        $this->db->where('a.estatus!=',2);
			$this->db->select('a.orden');
			$this->db->from('direcciones a');
			$this->db->join('idioma b', 'b.id = a.id_idioma');
			
			$res = $this->db->get();
			
			//print_r($this->db->last_query());die;
			
			if($res->num_rows()>0){
				return $res->result();
			}else{
				return false;
			}
		}
		/*
		*	Consultar idioma de registro
		*/
		public function consultarIdiomaRegistro($id){
	        $this->db->where('a.id',$id);
			$this->db->select('a.id_idioma');
			$this->db->from('direcciones a');
			$res = $this->db->get();
			$recordset = $res->result();
			return $recordset;
		}
		/*
		*	Reiniciar Orden
		*/
		public function reiniciar_orden($id_idioma){
			/*
			*	Consulto los slider que esten activos y reinicio el orden
			*/

			$this->db->order_by('a.orden','ASC');
	        $this->db->where('a.estatus!=',2);
	        $this->db->where('a.id_idioma',$id_idioma);
			$this->db->select('a.*');
			$this->db->from('direcciones a');
			$this->db->join('idioma b', 'b.id = a.id_idioma');
			$res = $this->db->get();
			$recordset = $res->result();
			if($recordset){
				$contador = 1;
				foreach ($recordset as $clave => $valor) {
					$data2 = array("orden"=>$contador);
					$this->db->where('id', $valor->id);
	        		$a = $this->db->update("direcciones", $data2);
	        		$contador++;	
				}
			}
			/***/
		}
		//----------------------------------------------------------------
		public function posicionar_modulos($posicionar){
			//---
			if($posicionar['tipo'] == 'insert'){

	            $this->db->where('orden >= ' . $posicionar['posicion']);

	            $this->db->where('id_idioma = ' . $posicionar['id_idioma']);

	            $resultados = $this->db->get("direcciones");

       			//print_r($this->db->last_query());die;

	            if($resultados->num_rows() > 0){

	                foreach ($resultados->result() as $row){

	                    $datos=array(
	                        'orden' => $row->orden + 1,
	                    );

	                    $this->db->where('id', $row->id);

	                    $this->db->update("direcciones",$datos);
	                }

	            }
	        }else if($posicionar['tipo'] == 'update'){

	            if($posicionar['final'] > $posicionar['inicial']){

	                $this->db->where('orden > ' . $posicionar['inicial'] . ' AND orden <= ' . $posicionar['final']);

       	            $this->db->where('id_idioma = ' . $posicionar['id_idioma']);

	                $resultados = $this->db->get("direcciones");
	                if($resultados){
		                if($resultados->num_rows() > 0){
		                    foreach ($resultados->result() as $row){
		                        $datos=array(
		                            'orden' => $row->orden - 1,
		                        );
		                        $this->db->where('id', $row->id);
		                        $this->db->update("direcciones", $datos);
		                    }
		                }
		            }

	            }else if($posicionar['final'] < $posicionar['inicial']){

		                $this->db->where('orden >= ' . $posicionar['final'] . ' AND orden < ' . $posicionar['inicial']);

		                $this->db->where('id_idioma = ' . $posicionar['id_idioma']);

		                $resultados = $this->db->get("direcciones");
		                if($resultados){
		                	if($resultados->num_rows() > 0){
			                    foreach ($resultados->result() as $row){
			                    	$datos=array(
			                            'orden' => $row->orden + 1,
			                        );
			                        $this->db->where('id', $row->id);
			                        $this->db->update("direcciones", $datos);
			                    }
			                }
		                }

			    }
	        //---
			}
		//--
		}
		//---
		/*
		*	Eliminar telefonos
		*/
		public function eliminar_telefonos($id_direccion){
			$this->db->where('id_direccion', $id_direccion);
			$this->db->delete("telefonos");
			return true;
		}
		//-----------------------------------------------------------------------
	}

?>
