<?php 

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class Empresa_model extends CI_Model{

	public function iniciar_sesion($login,$clave){
		$this->db->where('login',$login);
		$this->db->where('clave',$clave);
		$this->db->where('estatus','1');
		$this->db->select('*');
		$this->db->from(' usuarios u');
		return $this->db->count_all_results();
	}

	public function guardarEmpresa($data){
		if($this->db->insert("empresa",$data)){
			return true;
		}else{
			return false;
		}
	}

	public function consultarEmpresa($data){
   		$this->db->order_by('a.id','desc');
		if($data["id_empresa"]!=""){
			$this->db->where('a.id', $data["id_empresa"]);
		}
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
		$this->db->from('empresa a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$this->db->join('galeria c', 'c.id = a.id_imagen');
		$res = $this->db->get();
        //print_r($this->db->last_query());die;
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function modificarEmpresa($data){
		$this->db->where('id', $data["id"]);
        if($this->db->update("empresa", $data)){
        	return true;
        }else{
        	return false;
        }
	}

	public function consultarExisteTitulo($id,$titulo){
		$this->db->where('n.id !=',$id);
		$this->db->where('n.titulo',$titulo);
		$this->db->where('estatus','1');
		$this->db->select('*');
		$this->db->from(' empresa n');
		return $this->db->count_all_results();
	}

}