angular.module("ContentManagerApp")
	.controller("DireccionController", function($scope,$compile,$http,$location,serverDataMensajes,sesionFactory,direccionFactory,idiomaFactory,ordenFactory){

		$(".li-menu").removeClass("active");
		$("#li_contactos").addClass("active");
		$(".a-menu").removeClass("active");
		$("#direccion").addClass("active");

		$scope.titulo_pagina = "Dirección";
		$scope.subtitulo_pagina = "Registro";
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_cons = "Consultar";
		$scope.id_direccion = "";
		$scope.base_url = $("#base_url").val();
		$scope.titulo_text = "Pulse aquí para ingresar el contenido"
		$scope.activo_img = "inactivo";
		$scope.searchMarcas = []
		$scope.borrar_imagen = []
		$scope.cuantos = 1;
		$scope.direccion = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'titulo' : '',
						'descripcion' : '',
						'telefono' : [],
						'latitud':'',
						'longitud':'',
						'estatus' : '',
						'orden':'',
		}

		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;

				$.each( $scope.idioma, function( indice, elemento ){
				  	agregarOptions("#idioma", elemento.id, elemento.descripcion)
				});
			});
		}
		$scope.agregar_telefono_valores = function(valor){
			$scope.cuantos = $scope.cuantos+1;

			if($scope.cuantos==1){
				$("#telefono_direccion_0").val(valor)
			}else{
				//---	
				number = $scope.cuantos
				number2 = number
				var bloque_telefono = '<div id="fila_'+number+'" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 tlf_direccion_cuadro">\
										<div class="form-group">\
											<div class="asterisco_rojo"><i class="fa fa-asterisk" aria-hidden="true"></i></div>\
											<label>Teléfono #'+number2+'</label>\
											<input type="text" name="telefono_direccion" id="telefono_direccion" class="form-control  tlf_direccion" placeholder="Formato:+cod-xxx-xxx-xx-xx" onKeyPress="return valida(event,this,21,20)" onBlur="valida2(this,21,20);" maxlength="50" value="'+valor+'">\
								    	</div>\
									</div>'
			    $("#super_contenedor").append($compile(bloque_telefono)($scope)).fadeIn("slow");  	
				//---
			}
		}
		$scope.quitar_telefono = function(){
			if($scope.cuantos>1){
				var ene = $scope.cuantos
				$("#fila_"+ene).remove()
				$scope.cuantos = parseInt($scope.cuantos)-1;
			}else{
				mostrar_notificacion("Campos no validos","Debe agregar al menos 2 teléfonos","warning");
			}	
		}
		//-------------------------------------------------------------
		$scope.cargarOrden = function(){
			ordenFactory.asignar_valores($scope.direccion.id_idioma,$scope.base_url,"1")
			ordenFactory.cargar_orden_direccion(function(data){
				$scope.ordenes=data;
				//console.log($scope.ordenes);
				//-Elimino el select
				eliminarOptions("orden")
				$.each( $scope.ordenes, function( indice, elemento ){
				  	agregarOptions("#orden", elemento.orden, elemento.orden)
				});
				$('#orden > option[value=""]').prop('selected', true);

			});
		}
		//-------------------------------------------------------------
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.direccion.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.direccion.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.direccion.descripcion)
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.limpiar_cajas_direcciones = function(){
			$scope.direccion = {
									'id': '',
									'idioma': '',
									'id_idioma' : '',
									'titulo' : '',
									'descripcion' : '',
									'telefono' : [],
									'latitud':'',
									'longitud':'',
									'estatus' : '',
									'orden':'',
			}
			$scope.cuantos = 1;
			$(".tlf_direccion").val("");
			$(".tlf_direccion_cuadro").remove();
			$("#div_descripcion").html($scope.titulo_text);
			$('#textarea_editor').data("wysihtml5").editor.clear();
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.registrarDirecciones = function(){
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			$scope.direccion.orden = $("#orden").val();
			$scope.recorrer_form_telefono()

			if($scope.validar_form()==true){
				//Para guardar
				if(($scope.direccion.id!="")&&($scope.direccion.id!=undefined)){
					$scope.modificar_direcciones();
				}else{
					//console.log($scope.direccion)
					$scope.insertar_direcciones();
				}

			}

			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.validar_form = function(){
			
			var validado_telefono = 0;

			var a = 0;

			$( ".tlf_direccion" ).each(function( index ) {
			   if($( this ).val()==""){
			   	  a = index +1;
			   	  mostrar_notificacion("Campos no validos","Debe ingresar el valor del teléfonos #"+a,"warning");
			   	  return false;	
			   }else{
			   		validado_telefono = validado_telefono+1;	
			   }
			});

			if(($scope.direccion.id_idioma=="")||($scope.direccion.id_idioma=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
				return false;
			}else if($scope.direccion.titulo==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
				return false;
			}else if($scope.direccion.descripcion==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
				return false;
			}else if($scope.direccion.orden==""){
				mostrar_notificacion("Campos no validos","Debe seleccionar el orden","warning");
				return false;
			}else if(validado_telefono==$scope.cuantos){
				return true;
			}else{
				//alert(validado_telefono+"-"+$scope.cuantos)
				mostrar_notificacion("Campos no validos","Debe ingresar al menos un número de teléfono","warning");
				return false;
			}
			//else if(($scope.marcas.id_imagen=="NULL")||($scope.marcas.id_imagen=="")){
			// 	mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
			// 	return false;
			// }
		}
		//--
		$scope.recorrer_form_telefono = function(){
			

			var data
		
			var telefono_vector = new Array()
			$scope.cuantos = 0
			//--Recorro telefono
			$(".tlf_direccion").each(function(index){
				telefono = $(this).val()
				telefono_vector.push(telefono)
				$scope.cuantos++
			});
			//--
			$scope.direccion.telefono = telefono_vector
			//console.log($scope.direccion)
		}
		//--
		$scope.insertar_direcciones = function(){
			$http.post($scope.base_url+"/Direccion/registrarDireccion",
			{
				'id': $scope.direccion.id,
				'titulo':$scope.direccion.titulo,
				'descripcion':$scope.direccion.descripcion,
				'telefono':$scope.direccion.telefono,
				'orden':$scope.direccion.orden,
				'id_idioma':$scope.direccion.id_idioma,
				'latitud':$scope.direccion.latitud,
				'longitud':$scope.direccion.longitud,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_direcciones();
					desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
					desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
					desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultarDireccionIndividual = function(){
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			direccionFactory.asignar_valores("",$scope.id_direccion,$scope.base_url)
			direccionFactory.cargar_direccion(function(data){
				$scope.direccion=data[0];
				//console.log(data[0]);
				//--
				ordenFactory.asignar_valores($scope.direccion.id_idioma,$scope.base_url,"2")
				ordenFactory.cargar_orden(function(data){
					$scope.ordenes=data;
					//console.log($scope.ordenes)
					//-Elimino el select
					eliminarOptions("orden")
					$.each( $scope.ordenes, function( indice, elemento ){
					  	agregarOptions("#orden", elemento.orden, elemento.orden)
					});
				});
				//--
				$scope.cuantos = 0;
				//console.log($scope.direccion.telefono)
				$.each($scope.direccion.telefono, function( index, value ) {
				  	$scope.agregar_telefono_valores(value)
				});
				//--
				$("#div_descripcion").html($scope.direccion.descripcion)
				$scope.activo_img = "activo"
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar dirección";
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.direccion.id_idioma+'"]').prop('selected', true);
					$('#orden > option[value="'+$scope.direccion.orden+'"]').prop('selected', true);
					$scope.direccion.inicial = $scope.direccion.orden;
				},300);
				$("#idioma").prop('disabled', true);
			});
			desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.modificar_direcciones = function(){
			$http.post($scope.base_url+"/Direccion/modificarDireccion",
			{
				'id': $scope.direccion.id,
				'titulo':$scope.direccion.titulo,
				'descripcion':$scope.direccion.descripcion,
				'telefono':$scope.direccion.telefono,
				'orden':$scope.direccion.orden,
				'id_idioma':$scope.direccion.id_idioma,
				'latitud':$scope.direccion.latitud,
				'longitud':$scope.direccion.longitud,
				'inicial'	: $scope.direccion.inicial,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
					//$scope.limpiar_cajas_marcas();
					$scope.direccion.inicial = $scope.direccion.orden
					desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
				}else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
					desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
				}
			}).error(function(data,estatus){
				//console.log(data);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_idioma();

		$scope.id_direccion = $("#id_direccion").val();
			if($scope.id_direccion){
				$scope.consultarDireccionIndividual();
			}else{
				$("#idioma").removeAttr("disabled");
			}
	})
