angular.module("ContentManagerApp")
	.controller("PublicidadController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,publicidadFactory,ordenFactory){
		//---------------------------------------------------------------------
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_publicidad").addClass("active");
        $(".a-menu").removeClass("active");
        $("#publicidad").addClass("active");
		$scope.titulo_pagina = "Publicidad";
		$scope.subtitulo_pagina  = "Registrar Publicidad";
		$scope.activo_img = "inactivo";

		$scope.publicidad = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'titulo' : '',
						'descripcion' : '',
						'boton' : '',
						'url' : '',
						'estatus' : '',
						'id_imagen' : '',
						'imagen' : 'assets/images/logo_peque.png',
						'orden':''
		}
		
		
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_cons = "Consultar";
		$scope.borrar_imagen = []
		$scope.currentTab = 'datos_basicos'
		$scope.titulo_text = "Pulse aquí para ingresar la descripción de la publicidad"

		$scope.opcion = ''
		$scope.seccion = ''
		$scope.base_url = $("#base_url").val();
		
		//alert($scope.base_url);
		//Cuerpo de metodos
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;

				$.each( $scope.idioma, function( indice, elemento ){
				  	agregarOptions("#idioma", elemento.id, elemento.descripcion)
				});
				$('#idioma > option[value="'+$scope.publicidad.id_idioma+'"]').prop('selected', true);
			});
		}
		//---------------------------------
		//
		$scope.cargarOrden = function(){
			ordenFactory.asignar_valores($scope.publicidad.id_idioma,$scope.base_url,"1")
			ordenFactory.cargar_orden_publicidad(function(data){
				$scope.ordenes=data;
				console.log($scope.ordenes);
				//-Elimino el select
				eliminarOptions("orden")
				$.each( $scope.ordenes, function( indice, elemento ){
				  	agregarOptions("#orden", elemento.orden, elemento.orden)
				});
				$('#orden > option[value=""]').prop('selected', true);

			});
		}
		//WISIMODAL
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.publicidad.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.publicidad.descripcion)
			$("#cerrarModal").click();
		}
		//--
		
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.publicidad.descripcion)
		}
		//--
		$scope.agregar_contenido = function(){
			if ($scope.publicidad==undefined) {
					$scope.publicidad = {
									'id': '',
									'idioma': '',
									'id_idioma' : '',
									'titulo' : '',
									'descripcion' : '',
									'boton' : '',
									'url' : '',
									'estatus' : '',
									'id_imagen' : '',
									'imagen' : 'assets/images/logo_peque.png',
									'orden':'',
					}
			}

		}
		//------------------------------------------CONSULTA DE IMAGEN SEGUN ID DE CATEGORIA
		$scope.consultar_galeria_img = function(){
			galeriaFactory.asignar_valores('35','',$scope.base_url);//ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galery=data;
				console.log($scope.galery);
			});
		}
		//MODAL DE IMG
		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");
		}
		//PARA SELECCIONAR UNA IMAGEN Y MOSTRARLA EN EL CAMPO DE IMG
		$scope.seleccionar_imagen = function(event){
				var imagen = event.target.id;//Para capturar id
				console.log(imagen);
				var vec = $("#"+imagen).attr("data");
				var vector_data = vec.split("|")
				var id_imagen = vector_data[0];
				var ruta = vector_data[1];

				$scope.borrar_imagen.push(id_imagen);
				$scope.activo_img = "activo"
				$scope.publicidad.id_imagen = id_imagen
				$scope.publicidad.imagen = ruta
				//alert($scope.publicidad.id_imagen);
				//--
				$("#modal_img1").modal("hide");
				//--
		}
		/////////////////////////////////////////////////////////////////////////////////////
		$scope.registrarPublicidad = function(){
			$scope.publicidad.orden = $("#orden").val();
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			if($scope.validar_form()==true){
				//Para guardar
				if(($scope.publicidad.id!="")&&($scope.publicidad.id!=undefined)){
					$scope.modificar_publicidad();
				}else{
					$scope.insertar_publicidad();
				}
			}
			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		///////////////////////////////////////////////////////////////////////////////////
		$scope.insertar_publicidad = function(){
			$http.post($scope.base_url+"/Publicidad/registrarPublicidad",
			{
				'id' 	     : $scope.publicidad.id,
				'id_idioma'  : $scope.publicidad.id_idioma,
				'titulo'     : $scope.publicidad.titulo,
				'descripcion': $scope.publicidad.descripcion,
				'id_imagen'  : $scope.publicidad.id_imagen,
				'boton'      : $scope.publicidad.boton,
				'url'        : $scope.publicidad.url,
				'id_imagen'  :$scope.publicidad.id_imagen,
				'direccion' : $scope.publicidad.direccion,
				'orden':  $scope.publicidad.orden
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_publicidad();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}

			}).error(function(data,estatus){
				console.log(data);
			});
		}
		//////////////////////////
		$scope.modificar_publicidad = function(){
			$scope.publicidad.orden = $("#orden").val();
			$http.post($scope.base_url+"/Publicidad/modificarPublicidad",
			{
				'id' 	     : $scope.publicidad.id,
				'id_idioma'  : $scope.publicidad.id_idioma,
				'titulo'     : $scope.publicidad.titulo,
				'descripcion': $scope.publicidad.descripcion,
				'id_imagen'  : $scope.publicidad.id_imagen,
				'boton'      : $scope.publicidad.boton,
				'url'        : $scope.publicidad.url,
				'direccion' : $scope.publicidad.direccion,
				'orden' 	: $scope.publicidad.orden,
				'inicial'	: $scope.publicidad.inicial
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
					//$scope.limpiar_cajas_publicidad();
					$scope.publicidad.inicial = $scope.publicidad.orden 
					desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

				}else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
					desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

				}
				//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			}).error(function(data,estatus){
				//console.log(data);
			});
		}
		//////////////////////////
		$scope.validar_form = function(){
			if(($scope.publicidad.id_idioma=="")||($scope.publicidad.id_idioma=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
				return false;
			}else if(($scope.publicidad.direccion=="")||($scope.publicidad.direccion=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar la dirección del texto del slide","warning");
				return false;
			} else if($scope.publicidad.titulo==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
				return false;
			}
			else if(($scope.publicidad.orden=="NULL")||($scope.publicidad.orden=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el orden","warning");
				return false;
			}
			else if(($scope.publicidad.id_imagen=="NULL")||($scope.publicidad.id_imagen=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
				return false;
			}
			else{
				return true;
			}
		}
		/////////////////////////
		$scope.limpiar_cajas_publicidad = function(){
			$scope.publicidad = {
							'id': '',
							'idioma': '',
							'id_idioma' : '',
							'titulo' : '',
							'descripcion' : '',
							'boton' : '',
							'url' : '',
							'estatus' : '',
							'id_imagen' : '',
							'imagen' : 'assets/images/logo_peque.png'
		}

			$scope.activo_img = "inactivo";
			$("#div_descripcion").html($scope.titulo_text);
			$('#textarea_editor').data("wysihtml5").editor.clear();
			$("#idioma").removeAttr("disabled");
			$scope.titulo_registrar = "Registrar";
			$scope.subtitulo_pagina  = "Registrar publicidad";
			$("#nuevo").css({"display":"none"})
		}
		//--
		$scope.consultarPublicidadIndividual = function(){
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			publicidadFactory.asignar_valores("",$scope.id_publicidad,$scope.base_url)
			publicidadFactory.cargar_publicidad(function(data){
				
				$scope.publicidad=data[0];
								
				
				//alert($scope.publicidad.orden)
				$scope.publicidad=data[0];
				//--
				ordenFactory.asignar_valores($scope.publicidad.id_idioma,$scope.base_url,"2")
				ordenFactory.cargar_orden_publicidad(function(data){
					$scope.ordenes=data;
					console.log($scope.ordenes)
					//-Elimino el select
					eliminarOptions("orden")
					$.each( $scope.ordenes, function( indice, elemento ){
					  	agregarOptions("#orden", elemento.orden, elemento.orden)
					});
				});
				//--
				//console.log(data[0]);
				$("#div_descripcion").html($scope.publicidad.descripcion)
				$scope.borrar_imagen.push($scope.publicidad.id_imagen);
				$scope.activo_img = "activo"
				$scope.publicidad.imagen = $scope.publicidad.ruta
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar publicidad";
			
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.publicidad.id_idioma+'"]').prop('selected', true);
					$('#orden > option[value="'+$scope.publicidad.orden+'"]').prop('selected', true);
					$scope.publicidad.inicial = $scope.publicidad.orden;
				},500);
				
				//$("#idioma").attr("disabled");
				$("#idioma").prop('disabled', true);
			
				/*setTimeout(function(){
					$('#idioma > option[value="'+$scope.publicidad.id_idioma+'"]').prop('selected', true);
					$('#direccion_slide > option[value="'+$scope.publicidad.direccion+'"]').prop('selected', true);

				},300);*/
				//$("#idioma").attr("disabled");
				//$("#idioma").prop('disabled', true);
				desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			});
		}
		//---
		///////////////////////////////////////////////////
		$scope.consultar_idioma();
		$scope.consultar_galeria_img();

		$scope.id_publicidad = $("#id_publicidad").val();
			if($scope.id_publicidad){
				$scope.consultarPublicidadIndividual();
			}else{
				$("#idioma").removeAttr("disabled");
			}
		//-----------------------------------------------------------------------
	});	