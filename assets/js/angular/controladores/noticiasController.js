angular.module("ContentManagerApp")
	.controller("noticiasController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,noticiasFactory,categoriasNoticiasFactory){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_noticias").addClass("active");	
        $(".a-menu").removeClass("active");
        $("#noticias").addClass("active");	
		$scope.titulo_pagina = "Noticias";
		$scope.subtitulo_pagina  = "Registrar noticias";
		$scope.activo_img = "inactivo";
		$scope.noticias = {
								'id':'',
								'id_idioma':'',
								'titulo':'',
								'descripcion':'',
								'estatus':'',
								'id_imagen':'',
								'youtube':'',
								'soundcloud':'',
								'editorial':'',
								'miniatura':'',
								'link_soundcloud':'',
								'link_youtube':''
		}
		$scope.id_noticias = ""
		$scope.searchNoticias = []
		$scope.borrar_imagen = []
		$scope.currentTab = 'datos_basicos'
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_text = "Pulse aquí para ingresar el contenido  de la noticia"
		$scope.base_url = $("#base_url").val();
		SuperFecha("fechaNoticia")

		//--------------------------------------------------------
		//Cuerpo de metodos
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;
				$.each( $scope.idioma, function( indice, elemento ){
				  	agregarOptions("#idioma", elemento.id, elemento.descripcion)
				});
				$('#idioma > option[value="0"]').prop('selected', true);

				console.log($scope.idioma);
			});
		}
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.noticias.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.noticias.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.noticias.descripcion)
		}
		//--
		$scope.consultar_galeria_img = function(){
			galeriaFactory.asignar_valores('4','',$scope.base_url);//ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galery=data;
				//console.log($scope.galery);
			});
		}

		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");
		}

		$scope.seleccionar_imagen = function(event){
			var imagen = event.target.id;//Para capturar id
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];

			$scope.borrar_imagen.push(id_imagen);
			$scope.activo_img = "activo"
			$scope.noticias.id_imagen = id_imagen
			$scope.noticias.imagen = ruta
			//--
			$("#modal_img1").modal("hide");
			//--
		}

		$scope.registrarNoticias = function(){
			
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			if($scope.validar_form()==true){
				//Para guardar
				if(($scope.noticias.id!="")&&($scope.noticias.id!=undefined)){
					$scope.modificar_noticias();
				}else{
					$scope.insertar_noticias();
				}

			}
		}

		$scope.insertar_noticias = function(){
			///--------------------------------
			if(!$("#check_youtube").prop("checked")){
				$scope.noticias.link_youtube = "";
			}
			if(!$("#check_soundcloud").prop("checked")){
				$scope.noticias.link_soundcloud = "";
			}
			//---
			if(!$("#check_editorial").prop("checked")){
				$scope.noticias.editorial = "";
			}else{
				$scope.noticias.editorial = true;
			}
			if(!$("#check_miniatura").prop("checked")){
				$scope.noticias.miniatura = "";
			}else{
				$scope.noticias.miniatura = true;
			}
			//---
			//---------------------------------
			$http.post($scope.base_url+"/Noticias/registrarNoticias",
			{
				'id'	     : $scope.noticias.id,
				'titulo'     : $scope.noticias.titulo,
				'id_imagen'  : $scope.noticias.id_imagen,
				'descripcion': $scope.noticias.descripcion,
				'id_idioma'  : $scope.noticias.id_idioma,
				'fecha': $scope.noticias.fecha,
				'id_categoria': $scope.noticias.id_categoria,
				'link_youtube':$scope.noticias.link_youtube,
				'link_soundcloud':$scope.noticias.link_soundcloud,
				'editorial':$scope.noticias.editorial,
				'miniatura':$scope.noticias.miniatura
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_noticias();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.modificar_noticias = function(){
			///--------------------------------
			if(!$("#check_youtube").prop("checked")){
				$scope.noticias.link_youtube = "";
			}
			if(!$("#check_soundcloud").prop("checked")){
				$scope.noticias.link_soundcloud = "";
			}
			//---
			if(!$("#check_editorial").prop("checked")){
				$scope.noticias.editorial = "";
			}else{
				$scope.noticias.editorial = true;
			}
			if(!$("#check_miniatura").prop("checked")){
				$scope.noticias.miniatura = "";
			}else{
				$scope.noticias.miniatura = true;
			}
			//---
			$http.post($scope.base_url+"/Noticias/modificarNoticias",
			{
				'id':$scope.noticias.id,
				'titulo': $scope.noticias.titulo,
				'id_imagen':$scope.noticias.id_imagen,
				'descripcion':$scope.noticias.descripcion,
				'id_idioma':$scope.noticias.id_idioma,
				'fecha': $scope.noticias.fecha,
				'id_categoria': $scope.noticias.id_categoria,
				'link_youtube':$scope.noticias.link_youtube,
				'link_soundcloud':$scope.noticias.link_soundcloud,
				'editorial':$scope.noticias.editorial,
				'miniatura':$scope.noticias.miniatura
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.validar_form = function(){
			//console.log($scope.noticias)
			if(($scope.noticias.id_idioma=="NULL")||($scope.noticias.id_idioma=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
				return false;
			} else if($scope.noticias.titulo==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
				return false;
			}else if($scope.noticias.descripcion==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
				return false;
			}else if(($scope.noticias.id_categoria=="NULL")||($scope.noticias.id_categoria=="")||($scope.noticias.id_categoria==undefined)){
				mostrar_notificacion("Campos no validos","Debe seleccionar la categoría","warning");
				return false;
			}else if(($scope.noticias.fecha=="")||($scope.noticias.fecha==undefined)){
				mostrar_notificacion("Campos no validos","Debe ingresar la fecha","warning");
				return false;
			}
			//--Valido links de youtube 
			else if($("#check_youtube").prop("checked")&&($scope.noticias.link_youtube=="")){
					mostrar_notificacion("Campos no validos","Debe ingresar el link de youtube","warning");
					return false;
			}else if($("#check_soundcloud").prop("checked")&&($scope.noticias.link_soundcloud=="")){
					mostrar_notificacion("Campos no validos","Debe ingresar el link de soundcloud","warning");
					return false;
			}else if(($scope.noticias.id_imagen=="NULL")||($scope.noticias.id_imagen=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
				return false;
			}
			else{
				return true;
			}
		}

		$scope.limpiar_cajas_noticias = function(){
			$scope.noticias = {
								'id':'',
								'id_idioma':'',
								'titulo':'',
								'descripcion':'',
								'estatus':'',
								'id_imagen':'',
								'youtube':'',
								'soundcloud':'',
								'editorial':'',
								'miniatura':'',
								'link_soundcloud':'',
								'link_youtube':''
			}

			$scope.activo_img = "inactivo";

			$('#text_editor').data("wysihtml5").editor.clear();
			$("#div_descripcion").html($scope.titulo_text);
			$(checkbox).prop('checked',false)

		}
		//--
		$scope.consultarNoticiasIndividual = function(){

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			noticiasFactory.asignar_valores("",$scope.id_noticias,$scope.base_url)
			noticiasFactory.cargar_noticias(function(data){
				$scope.noticias=data[0];
				console.log(data[0]);
				$("#div_descripcion").html($scope.noticias.descripcion)
				$scope.borrar_imagen.push($scope.noticias.id_imagen);
				$scope.activo_img = "activo"
				//-Cambio 10022020
				//Asigno valor a los check
				if($scope.noticias.link_youtube!=""){
					$("#check_youtube").prop("checked",true)
					$scope.noticias.youtube = true
				}else{
					$scope.noticias.youtube = ""
				}
				if($scope.noticias.link_soundcloud!=""){
					$("#check_soundcloud").prop("checked",true)
					$scope.noticias.soundcloud = true
				}else{
					$scope.noticias.soundcloud = ""
				}
				if(($scope.noticias.editorial!="")&&($scope.noticias.editorial!="0")){
					$("#check_editorial").prop("checked",true)
				}
				//---
				$scope.noticias.imagen = $scope.noticias.ruta
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar noticias";
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.noticias.id_idioma+'"]').prop('selected', true);
				},300);
				//--Consulta de idioma
				categoriasNoticiasFactory.asignar_valores("","",$scope.base_url)
				categoriasNoticiasFactory.cargar_categorias(function(data){
					$scope.categoriasNoticias=data;
					$.when(
						$.each( $scope.categoriasNoticias, function( indice, elemento ){
						  	agregarOptions("#categoriaNoticia", elemento.id, elemento.descripcion)
						})
					).then(function(){
						$('#categoriaNoticia > option[value="'+$scope.noticias.id_categoria+'"]').prop('selected', true)
					})	
					console.log($scope.categoriasNoticias);
				});
				//---
				$("#idioma").prop('disabled', true);
			});

			desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		//---
		$scope.consultar_categorias = function(){
			categoriasNoticiasFactory.asignar_valores("","",$scope.base_url)
			categoriasNoticiasFactory.cargar_categorias(function(data){
				$scope.categoriasNoticias=data;
				$.each( $scope.categoriasNoticias, function( indice, elemento ){
				  	agregarOptions("#categoriaNoticia", elemento.id, elemento.descripcion)
				});
				$('#categoriaNoticia > option[value="0"]').prop('selected', true);
				console.log($scope.categoriasNoticias);
			});
		}
		//---
		$scope.activarCheck = function(valor){
			//--
			switch(valor){
				case '1':
					if($("#check_youtube").prop("checked")){
					$scope.noticias.youtube = true
					}else{
						$scope.noticias.youtube = false
					}
				break;
				case '2':
					if($("#check_soundcloud").prop("checked")){
					$scope.noticias.soundcloud = true
					}else{
						$scope.noticias.soundcloud = false
					}
				break;
				case '3':
					if($("#check_editorial").prop("checked")){
					$scope.noticias.editorial = true
					}else{
						$scope.noticias.editorial = false
					}
				break;
				case '4':
					if($("#check_miniatura").prop("checked")){
					$scope.noticias.miniatura = true
					}else{
						$scope.noticias.miniatura = false
					}
				break;
			}
			
			//--
		}
		//---
		//--------------------------------------------------------
		//Cuerpo de llamados
		$scope.consultar_idioma();
		$scope.consultar_galeria_img();
		$scope.consultar_categorias();
		//--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
		$scope.id_noticias  = $("#id_noticias").val();
		if($scope.id_noticias){
			$scope.consultarNoticiasIndividual();
		}
		//--------------------------------------------------------

	});
