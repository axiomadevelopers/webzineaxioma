angular.module("ContentManagerApp")
	.controller("categoriasNoticiasController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,categoriasNoticiasFactory){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_noticias").addClass("active");
        $(".a-menu").removeClass("active");
        $("#categoria_noticias").addClass("active");

		$scope.titulo_pagina = "Categorias Noticias";
		$scope.subtitulo_pagina  = "Registrar categorias";
		$scope.activo_img = "inactivo";
		$scope.categorias = {
								'id':'',
								'descripcion':'',
								'estatus':''
		}
		$scope.titulo_registrar = "Registrar";
		$scope.categorias_menu = "1";
		$scope.base_url = $("#base_url").val();
		//Cuerpo de metodos
		//---------------------------------------------------------------
		$scope.limpiar_cajas_categorias = function(){
			$scope.categorias = {
								'id':'',
								'descripcion':'',
								'estatus':''
			}
		}
		//--
		$scope.validar_form = function(){
			if($scope.categorias.descripcion==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la descripcion","warning");
				return false;
			}else{
				return true;
			}
		}
		//--
		$scope.consultarCategoriaIndividual = function(){
			
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			categoriasNoticiasFactory.asignar_valores("",$scope.id_categoria,$scope.base_url)
			categoriasNoticiasFactory.cargar_categorias(function(data){
				$scope.categorias=data[0];
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar categorías";
			});

			desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		//--
		$scope.registrarCategorias = function(){

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			if($scope.validar_form()==true){
				//Para guardar
				//alert("id_personas:"+$scope.doctor.id_personas);
				//alert("id_doctor:"+$scope.doctor.id);
				$scope.categorias.id = $scope.id_categoria;
				if(($scope.categorias.id!=undefined)&&($scope.categorias.id!="")){
					$scope.modificar_categorias();
				}else{
					$scope.insertar_categorias();
				}
			}
			
			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
		
		}
		//---
		$scope.insertar_categorias = function(){
			$http.post($scope.base_url+"/CategoriasNoticias/registrarCategorias",
			{
				 'descripcion':$scope.categorias.descripcion,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_categorias();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una categoría con esa descripción","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		//---
		$scope.modificar_categorias = function(){
			$http.post($scope.base_url+"/CategoriasNoticias/modificarCategorias",
			{
				 'id':$scope.id_categoria,
				 'descripcion':$scope.categorias.descripcion,

			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
				}else if($scope.mensajes.mensaje == "existe_nombre"){
					mostrar_notificacion("Mensaje","Ya existe una categoria con ese nombre","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrió un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$(".tst1").click(function(){


     });
	//---------------------------------------------------------------
	//Cuerpo de Llamados a metodos
	//--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
	$scope.id_categoria  = $("#id_categoria").val();
	if($scope.id_categoria){
		$scope.consultarCategoriaIndividual();
	}
	//---------------------------------------------------------
	});
