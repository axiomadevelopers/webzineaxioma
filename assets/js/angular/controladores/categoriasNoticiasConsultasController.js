angular.module("ContentManagerApp")
	.controller("categoriasNoticiasConsultasController", function($scope,$http,$location,serverDataMensajes,categoriasNoticiasFactory,sesionFactory,idiomaFactory){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_noticias").addClass("active");
        $(".a-menu").removeClass("active");
        $("#categorias_noticias").addClass("active");
		$scope.titulo_pagina = "Consulta de Categorías Noticias";
		$scope.activo_img = "inactivo";
		$scope.categorias = {
								'id':'',
								'descripcion':'',
								'estatus':''
		}
		$scope.categorias_menu = "1";
		$scope.id_categoria = "";
		$scope.base_url = $("#base_url").val();
		//-----------------------------------------------------
		//--Cuerpo de metodos  --/
		$scope.iniciar_datatable = function(){
			//--Iniciar DataTable
			$('#myTable').DataTable({
			    language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
   			  "order": [[ 0, "desc" ]]	
			});
			//console.log($scope.categorias);
		}
		$scope.consultar_categorias = function(){
			categoriasNoticiasFactory.asignar_valores("","",$scope.base_url)
			categoriasNoticiasFactory.cargar_categorias(function(data){
				$scope.categorias=data;
			});
		}
		//---------------------------------------------------------------
		$scope.ver_categoria = function(valor){
			var id = $("#ver"+valor).attr("data");
			$scope.id_categoria = id;
			$("#id_categoria").val($scope.id_categoria)
			let form = document.getElementById('formConsultaCategorias');
			form.action = "./categoriasNoticiasVer";
			form.submit();
		}
		//----------------------------------------------------------------
		$scope.activar_registro = function(event){
			$scope.id_seleccionado_categorias = []
			$scope.estatus_seleccionado_categorias = []
			var caja = event.currentTarget.id;
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_categorias = arreglo_atributos[0];
			$scope.estatus_seleccionado_categorias = arreglo_atributos[1];
			$("#cabecera_mensaje").text("Información:");
			//--
			if ($scope.estatus_seleccionado_categorias==0){
				mensaje = "Desea modificar el estatus de este registro a publicado? ";
				$scope.estatus_seleccionado_categorias=1;
			}else{
				mensaje = "Desea modificar el estatus de este registro a inactivo? ";
				$scope.estatus_seleccionado_categorias=0
			}
			//($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

			//--
			$scope.modificar_estatus("activar_inactivar",mensaje)
		}
		//----------------------------------------------------------------
		$scope.modificar_estatus = function(opcion,mensaje){
			 swal({
				  title: 'Esta seguro?',
				  text: mensaje,
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si!',
				  cancelButtonText: 'No',
				}).then((result) => {
					  if (result.value) {
					  	$scope.accion_estatus()
					  }
				})
		}
		//----------------------------------------------------------------
		$scope.accion_estatus = function(){

			$http.post($scope.base_url+"/CategoriasNoticias/modificarCategoriasEstatus",
			{
				 'id':$scope.id_seleccionado_categorias,
				 'estatus':$scope.estatus_seleccionado_categorias,

			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					Swal(
					      'Realizado!',
					      'El proceso fue ejecutado.',
					      'success'
				    ).then((result) => {
						  if (result.value) {
							    let form = document.getElementById('formConsultaCategorias');
								form.action = "./consultarCategoriasNoticias";
								form.submit();
						  }

					});
				}else{
					Swal(
					      'No realizado!',
					      'El proceso no pudo ser ejecutado.',
					      'warning'
				    )
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		//----------------------------------------------------------------
		$scope.eliminar_registro = function(event){
			$scope.id_seleccionado_categorias = []
			$scope.estatus_seleccionado_categorias = []
			var caja = event.currentTarget.id;
			//alert(caja);
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_categorias = arreglo_atributos[0];
			$scope.estatus_seleccionado_categorias = arreglo_atributos[1];
			//console.log($scope.estatus_seleccionado_categorias);
			$("#cabecera_mensaje").text("Información:");
			if ($scope.estatus_seleccionado_categorias==2){
				mensaje = "Desea eliminar este registro? ";
			}
			$scope.modificar_estatus("eliminar",mensaje)
		}
		//----------------------------------------------------------------
		//-- Cuerpo de funciones--/
		setTimeout(function(){
				$scope.iniciar_datatable();
		},500);
		$scope.consultar_categorias();

		//-----------------------------------------------------
	});
