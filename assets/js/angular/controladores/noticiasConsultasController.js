angular.module("ContentManagerApp")
	.controller("noticiasConsultasController", function($scope,$http,$location,serverDataMensajes,noticiasFactory,sesionFactory,idiomaFactory){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_noticias").addClass("active");	
        $(".a-menu").removeClass("active");
        $("#noticias").addClass("active");	
		$scope.titulo_pagina = "Consulta de Noticias";
		$scope.activo_img = "inactivo";
		$scope.categorias = {
								'id':'',
								'descripcion':'',
								'estatus':''
		}
		$scope.categorias_menu = "1";
		$scope.id_categoria = "";
		$scope.base_url = $("#base_url").val();
		//-----------------------------------------------------
		//--Cuerpo de metodos  --/
		$scope.iniciar_datatable = function(){
			//--Iniciar DataTable
			$('#myTable').DataTable({
			    language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
			    "order": [[ 0, "desc" ]]
			});
			//console.log($scope.categorias);
		}
		$scope.consultar_noticias = function(){
			noticiasFactory.asignar_valores("","",$scope.base_url)
			noticiasFactory.cargar_noticias(function(data){
				$.when(
                     $scope.noticias=data)
                .then(function(){
                    $scope.iniciar_datatable()
                });
				//console.log($scope.noticias);
				$(".sorting_desc").click();				
			});
		}
		//---------------------------------------------------------------
		$scope.ver_noticia = function(valor){
			var id = $("#ver"+valor).attr("data");
			$scope.id_noticias = id;	
			$("#id_noticias").val($scope.id_noticias)
			let form = document.getElementById('formConsultaNoticias');
			form.action = "./noticiasVer";
			form.submit();
		}
		//----------------------------------------------------------------
		$scope.activar_registro = function(event){
			$scope.id_seleccionado_noticias = []
			$scope.estatus_seleccionado_noticias = []
			var caja = event.currentTarget.id;
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_noticias = arreglo_atributos[0];
			$scope.estatus_seleccionado_noticias = arreglo_atributos[1];
			$("#cabecera_mensaje").text("Información:");			
			//--
			if ($scope.estatus_seleccionado_noticias==0){
				mensaje = "Desea modificar el estatus de este registro a publicado? ";
				$scope.estatus_seleccionado_noticias=1;
			}else{
				mensaje = "Desea modificar el estatus de este registro a inactivo? ";
				$scope.estatus_seleccionado_noticias=0
			}
			//($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

			//--
			$scope.modificar_estatus("activar_inactivar",mensaje)
		}
		//----------------------------------------------------------------
		$scope.modificar_estatus = function(opcion,mensaje){
			 swal({
				  title: 'Esta seguro?',
				  text: mensaje,
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si!',
				  cancelButtonText: 'No',
				}).then((result) => {
					  if (result.value) {
					  	$scope.accion_estatus()
					  }
				})
		}
		//----------------------------------------------------------------
		$scope.accion_estatus = function(){

			$http.post($scope.base_url+"/Noticias/modificarNoticiasEstatus",
			{
				 'id':$scope.id_seleccionado_noticias,	
				 'estatus':$scope.estatus_seleccionado_noticias, 	

			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					Swal(
					      'Realizado!',
					      'El proceso fue ejecutado.',
					      'success'
				    ).then((result) => {
						  if (result.value) {
							    let form = document.getElementById('formConsultaNoticias');
								form.action = "./consultarNoticias";
								form.submit();
						  }
				    
					});
				}else{
					Swal(
					      'No realizado!',
					      'El proceso no pudo ser ejecutado.',
					      'warning'
				    )
				}
			}).error(function(data,estatus){
				console.log(data);
			});	
		}
		//----------------------------------------------------------------
		$scope.eliminar_registro = function(event){
			$scope.id_seleccionado_noticias = []
			$scope.estatus_seleccionado_noticias = []
			var caja = event.currentTarget.id;
			//alert(caja);
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_noticias = arreglo_atributos[0];
			$scope.estatus_seleccionado_noticias = arreglo_atributos[1];
			//console.log($scope.estatus_seleccionado_noticias);
			$("#cabecera_mensaje").text("Información:");
			if ($scope.estatus_seleccionado_noticias==2){
				mensaje = "Desea eliminar este registro? ";
			}
			$scope.modificar_estatus("eliminar",mensaje)
		}
		//----------------------------------------------------------------
		//-- Cuerpo de funciones--/
		$scope.consultar_noticias();
		
		//-----------------------------------------------------
	});	