angular.module("ContentManagerApp")
	.controller("empresaController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,empresaFactory,){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_empresa").addClass("active");	
        $(".a-menu").removeClass("active");
        $("#empresa").addClass("active");	
		$scope.titulo_pagina = "Empresa";
		$scope.subtitulo_pagina  = "Registrar empresa";
		$scope.activo_img = "inactivo";
		$scope.empresa = {
								'id':'',
								'id_idioma':'',
								'titulo':'',
								'subtitulo':'',
								'descripcion':'',
								'estatus':'',
								'id_imagen':'',
		}
		$scope.id_empresa = ""
		$scope.searchEmpresa = []
		$scope.borrar_imagen = []
		$scope.currentTab = 'datos_basicos'
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_text = "Pulse aquí para ingresar el contenido"
		$scope.base_url = $("#base_url").val();

		//--------------------------------------------------------
		//Cuerpo de metodos
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;
				$.each( $scope.idioma, function( indice, elemento ){
				  	agregarOptions("#idioma", elemento.id, elemento.descripcion)
				});
				$('#idioma > option[value="0"]').prop('selected', true);

				console.log($scope.idioma);
			});
		}
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.empresa.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.empresa.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.empresa.descripcion)
		}
		//--
		$scope.consultar_galeria_img = function(){
			galeriaFactory.asignar_valores('36','',$scope.base_url);//ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galery=data;
				//console.log($scope.galery);
			});
		}

		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");
		}

		$scope.seleccionar_imagen = function(event){
			var imagen = event.target.id;//Para capturar id
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];

			$scope.borrar_imagen.push(id_imagen);
			$scope.activo_img = "activo"
			$scope.empresa.id_imagen = id_imagen
			$scope.empresa.imagen = ruta
			//--
			$("#modal_img1").modal("hide");
			//--
		}

		$scope.registrarempresa = function(){
			
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			if($scope.validar_form()==true){
				//Para guardar
				if(($scope.empresa.id!="")&&($scope.empresa.id!=undefined)){
					$scope.modificar_empresa();
				}else{
					$scope.insertar_empresa();
				}

			}
		}

		$scope.insertar_empresa = function(){
			///--------------------------------
			$http.post($scope.base_url+"/empresa/registrarEmpresa",
			{
				'id'	     : $scope.empresa.id,
				'titulo'     : $scope.empresa.titulo,
				'subtitulo'  : $scope.empresa.subtitulo,
				'id_imagen'  : $scope.empresa.id_imagen,
				'descripcion': $scope.empresa.descripcion,
				'id_idioma'  : $scope.empresa.id_idioma,
				
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_empresa();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.modificar_empresa = function(){
			///--------------------------------
			$http.post($scope.base_url+"/empresa/modificarempresa",
			{
				'id':$scope.empresa.id,
				'titulo': $scope.empresa.titulo,
				'subtitulo'  : $scope.empresa.subtitulo,
				'id_imagen':$scope.empresa.id_imagen,
				'descripcion':$scope.empresa.descripcion,
				'id_idioma':$scope.empresa.id_idioma,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.validar_form = function(){
			//console.log($scope.empresa)
			if(($scope.empresa.id_idioma=="NULL")||($scope.empresa.id_idioma=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
				return false;
			} else if($scope.empresa.titulo==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
				return false;
			}else if($scope.empresa.subtitulo==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el subtitulo","warning");
				return false;
			}else if($scope.empresa.descripcion==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
				return false;
			}else if(($scope.empresa.id_imagen=="NULL")||($scope.empresa.id_imagen=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
				return false;
			}
			else{
				return true;
			}
		}

		$scope.limpiar_cajas_empresa = function(){
			$scope.empresa = {
								'id':'',
								'id_idioma':'',
								'titulo':'',
								'subtitulo':'',
								'descripcion':'',
								'id_imagen':''
			}

			$scope.activo_img = "inactivo";

			$('#text_editor').data("wysihtml5").editor.clear();
			$("#div_descripcion").html($scope.titulo_text);
			$(checkbox).prop('checked',false)

		}
		//--
		$scope.consultarempresaIndividual = function(){

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			empresaFactory.asignar_valores("",$scope.id_empresa,$scope.base_url)
			empresaFactory.cargar_empresa(function(data){
				$scope.empresa=data[0];
				console.log(data[0]);
				$("#div_descripcion").html($scope.empresa.descripcion)
				$scope.borrar_imagen.push($scope.empresa.id_imagen);
				$scope.activo_img = "activo"
				
				$scope.empresa.imagen = $scope.empresa.ruta
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar empresa";
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.empresa.id_idioma+'"]').prop('selected', true);
				},300);
				//---
				$("#idioma").prop('disabled', true);
			});

			desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		
		
		//---
		//--------------------------------------------------------
		//Cuerpo de llamados
		$scope.consultar_idioma();
		$scope.consultar_galeria_img();
		//--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
		$scope.id_empresa  = $("#id_empresa").val();
		if($scope.id_empresa){
			$scope.consultarempresaIndividual();
		}
		//--------------------------------------------------------

	});
