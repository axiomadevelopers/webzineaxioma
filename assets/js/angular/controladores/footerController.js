angular.module("ContentManagerApp")
	.controller("footerController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,footerFactory){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_contactos").addClass("active");	
        $(".a-menu").removeClass("active");
        $("#footer").addClass("active");		
        $scope.titulo_pagina = "Footer";
		$scope.subtitulo_pagina  = "Registrar footer";
		$scope.activo_img = "inactivo";
		$scope.footer = {
								'id':'',
								'id_idioma':'',
								'correo':'',
								'telefono':'',
								'descripcion':'',
								'estatus':'',
		}
		$scope.id_footer = ""
		$scope.searchFooter = []
		$scope.borrar_imagen = []
		$scope.currentTab = 'datos_basicos'
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_text = "Pulse aquí para ingresar el contenido  del footer"
		$scope.base_url = $("#base_url").val();
		//--------------------------------------------------------
		//Cuerpo de metodos
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;
				//console.log($scope.idioma);
			});
		}
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.footer.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.footer.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.footer.descripcion)
		}
		//--
		$scope.registrarFooter = function(){

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			if($scope.validar_form()==true){
				//Para guardar
				if(($scope.footer.id!="")&&($scope.footer.id!=undefined)){
					$scope.modificar_footer();
				}else{
					$scope.insertar_footer();
				}

			}
			
			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
		}

		$scope.insertar_footer = function(){
			$http.post($scope.base_url+"/Footer/registrarFooter",
			{
				'correo'     : $scope.footer.correo,
				'descripcion': $scope.footer.descripcion,
				'id_idioma'  : $scope.footer.id_idioma.id,
				'telefono': $scope.footer.telefono,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_footer();
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.modificar_footer = function(){
			$http.post($scope.base_url+"/Footer/modificarFooter",
			{
				'id'	     : $scope.footer.id,
				'correo'     : $scope.footer.correo,
				'descripcion': $scope.footer.descripcion,
				'id_idioma'  : $scope.footer.id_idioma,
				'telefono': $scope.footer.telefono,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
				}else if($scope.mensajes.mensaje == "no_existe"){
					mostrar_notificacion("Mensaje","No existe el footer","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.validar_form = function(){
			//console.log($scope.noticias)
			if(($scope.footer.id_idioma=="NULL")||($scope.footer.id_idioma=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
				return false;
			} else if($scope.footer.correo==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el correo","warning");
				return false;
			}else if($scope.footer.descripcion==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
				return false;
			}
			else{
				return true;
			}
		}

		$scope.limpiar_cajas_footer = function(){
			$scope.footer = {
								'id':'',
								'id_idioma':'',
								'correo':'',
								'telefono':'',
								'descripcion':'',
								'estatus':'',
			}

			$('#text_editor').data("wysihtml5").editor.clear();
			$("#div_descripcion").html($scope.titulo_text);
		}
		//--
		$scope.consultarFooterIndividual = function(){

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			footerFactory.asignar_valores("",$scope.id_footer,$scope.base_url)
			footerFactory.cargar_footer(function(data){
				$scope.footer=data[0];
				//console.log(data[0]);
				$("#div_descripcion").html($scope.footer.descripcion)
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar footer";
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.footer.id_idioma+'"]').prop('selected', true);
				},300);
				$("#idioma").prop('disabled', true);
			});

			desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		//---
		//--------------------------------------------------------
		//Cuerpo de llamados
		$scope.consultar_idioma();
		//--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
		$scope.id_footer  = $("#id_footer").val();
		if($scope.id_footer){
			$scope.consultarFooterIndividual();
		}else{
			$("#idioma").prop('disabled', false);
		}
		//--------------------------------------------------------
	});