angular.module("ContentManagerApp")
	.controller("GaleriaMultimediaController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,categoriasFactory,upload){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_configuracion").addClass("active");	
        $(".a-menu").removeClass("active");
        $("#galeria").addClass("active");	
		$scope.titulo_pagina = "Galería Multimedia";
		$scope.subtitulo_pagina  = "Administrar galería";
		$scope.activo_img = "inactivo";
		$scope.galeria = {
							"id":"",
							"descripcion":"",
							"categoria":"",
							"url":""
		}
		$scope.titulo_registrar = "Registrar";
		$scope.categorias_menu = "1";
		$scope.base_url = $("#base_url").val();
		$scope.currentTab = 'datos_basicos'

		//Cuerpo de metodos
		//--
		$scope.consultarCategoriaIndividual = function(){
			categoriasFactory.asignar_valores("","",$scope.base_url)
			categoriasFactory.cargar_categorias(function(data){
				$scope.categorias=data;
				//console.log($scope.categorias);
			});
		}
		$("#formDropZone").dropzone({ url: $scope.base_url+"/GaleriaMultimedia/Upload" });
		//--
		$scope.validar_form = function(){
			console.log($scope.file)
			if($scope.galeria.descripcion==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el título de la imagen","warning");
				return false;
			}else if(($scope.galeria.categoria=="")||($scope.galeria.categoria==undefined)||$scope.galeria.categoria==0){
				mostrar_notificacion("Campos no validos","Debe seleccionar la categoría","warning");
				return false;
			}else if(($scope.file==undefined)||($scope.file=="")){
				mostrar_notificacion("Campos no validos","Debe Seleccionar una imagen","warning");
				return false;
			}
			else{
				return true;
			}
		}
		//--
		$scope.registrarGaleria = function(){

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			if($scope.validar_form()==true){
				//Para guardar
				//alert("id_personas:"+$scope.doctor.id_personas);
				//alert("id_doctor:"+$scope.doctor.id);
				$scope.galeria.id = $scope.id_galeria;
				if(($scope.galeria.id!=undefined)&&($scope.galeria.id!="")){
					$scope.modificar_galeria();	
				}else{
					$scope.insertar_galeria();
				}		
			}

			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		//--
		$scope.insertar_galeria = function (){
			$scope.uploadFile();
		}
		//---
		//---------------------------------------
		//--Metodo para subir acrhivos
		$scope.uploadFile = function(){
			var file = $scope.file;
			var categoria = $scope.galeria.categoria.id;
			var nombre_archivo = $scope.galeria.descripcion;
			var base_url = $scope.base_url;
			//-
			upload.uploadFile(file,categoria,nombre_archivo,base_url).then(function(res){
				console.log(res.data.mensaje);
				if(res.data.mensaje=="registro_procesado"){
					mostrar_notificacion("Mensaje","La imagen fue cargada de manera exitosa!","info");
					//$scope.consultar_galeria();
					$scope.limpiar_imagen();
				}else{
					mostrar_notificacion("Mensaje"," Error al subir tipo de archivo, solo puede subir imagenes .jpg con un peso menor a 1 mb con medidas entre w:5000 h:5000	","warning");
				}
				//--------------------------------
			});
			//-
		}
		//--
		$scope.limpiar_imagen = function(){
			$scope.galeria = {
							"id":"",
							"descripcion":"",
							"categoria":"",
							"url":""
			}
			$("#list").css("display","block");
			$(".imgbiblioteca_principal").css("display","none");
			$("#file").val("");
			$("#categorias").val("");
			$("#previa").css("display","block");
			$scope.file = "";
			//$("#select_categoria").selectpicker('refresh');
		}
		//---------------------------------------
		//---
		//Cuerpo de llamados
		$scope.consultarCategoriaIndividual();
	});	