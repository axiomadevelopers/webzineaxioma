angular.module("ContentManagerApp")
	.controller("DireccionConsultasController", function($scope,$http,$location,serverDataMensajes,sesionFactory,idiomaFactory,direccionFactory){
		$(".li-menu").removeClass("active");
		$("#li_contactos").addClass("active");
		$(".a-menu").removeClass("active");
		$("#direccion").addClass("active");

		$scope.titulo_pagina = "Consulta de Dirección";
		$scope.activo_img = "inactivo";

		$scope.direccion = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'titulo' : '',
						'descripcion' : '',
						'telefono' : [],
						'latitud':'',
						'longitud':'',
						'estatus' : '',
						'orden':'',
		}

		$scope.categorias_menu = "2";
		$scope.id_marca = "";
		$scope.base_url = $("#base_url").val();

		$scope.iniciar_datatable = function(){
			//--Iniciar DataTable
			$('#myTable').DataTable({
			    language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
			    "order": [[ 2, "asc" ]]

			});
			//console.log($scope.direccion);
		}
		//////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_direccion = function(){
			direccionFactory.asignar_valores("","",$scope.base_url)
			direccionFactory.cargar_direccion(function(data){
				$scope.direccion=data;
				//console.log($scope.direccion);
			});
		}
		//////////////////////////////////////////////////////////////////////////////////////
		$scope.ver_direccion = function(valor){
			var id = $("#ver"+valor).attr("data");
			$scope.id_marca = id;
			//console.log($scope.id_marca);
			$("#id_marca").val($scope.id_marca)
			let form = document.getElementById('formConsultaDireccion');
			form.action = "./direccionVer";
			form.submit();
		}
		//////////////////////////////////////////////////////////////////////////////////////
		$scope.activar_registro = function(event){
			$scope.id_seleccionado_direccion = []
			$scope.estatus_seleccionado_direccion = []
			var caja = event.currentTarget.id;
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_direccion = arreglo_atributos[0];
			$scope.estatus_seleccionado_direccion = arreglo_atributos[1];
			$("#cabecera_mensaje").text("Información:");
			//--
			if ($scope.estatus_seleccionado_direccion==0){
				mensaje = "Desea modificar el estatus de este registro a publicado? ";
				$scope.estatus_seleccionado_direccion=1;
			}else{
				mensaje = "Desea modificar el estatus de este registro a inactivo? ";
				$scope.estatus_seleccionado_direccion=0
			}
			//($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

			//--
			$scope.modificar_estatus("activar_inactivar",mensaje)
		}
		//----------------------------------------------------------------
		$scope.modificar_estatus = function(opcion,mensaje){
			 swal({
				  title: 'Esta seguro?',
				  text: mensaje,
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si!',
				  cancelButtonText: 'No',
				}).then((result) => {
					  if (result.value) {
						$scope.accion_estatus()
					  }
				})
		}
		//----------------------------------------------------------------
		$scope.accion_estatus = function(){

			$http.post($scope.base_url+"/Direccion/modificarDireccionEstatus",
			{
				 'id':$scope.id_seleccionado_direccion,
				 'estatus':$scope.estatus_seleccionado_direccion,

			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					Swal(
						  'Realizado!',
						  'El proceso fue ejecutado.',
						  'success'
					).then((result) => {
						  if (result.value) {
								let form = document.getElementById('formConsultaDireccion');
								form.action = $scope.base_url+"cms/direccion/consultarDireccion";
								form.submit();
						  }

					});
				}else{
					Swal(
						  'No realizado!',
						  'El proceso no pudo ser ejecutado.',
						  'warning'
					)
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		//////////////////////////////////////////////////////////////////////////////////////
		$scope.eliminar_registro = function(event){
			$scope.id_seleccionado_direccion = []
			$scope.estatus_seleccionado_direccion = []
			var caja = event.currentTarget.id;
			//alert(caja);
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_direccion = arreglo_atributos[0];
			$scope.estatus_seleccionado_direccion = arreglo_atributos[1];
			//console.log($scope.estatus_seleccionado_direccion);
			$("#cabecera_mensaje").text("Información:");
			if ($scope.estatus_seleccionado_direccion==2){
				mensaje = "Desea eliminar este registro? ";
			}
			$scope.modificar_estatus("eliminar",mensaje)
		}
		//////////////////////////////////////////////////////////////////////////////////////
		setTimeout(function(){
				$scope.iniciar_datatable();
		},500);
		$scope.consultar_direccion();
	})
