angular.module("WebZineApp")
//---------------------------------------------------------------------------------------
//--Bloque de servicios

//--Bloque de factorias

.factory("paginacionFactory",['$http', function($http){
	//--
	return{
			
	}
	//--
}])
.factory("nosotrosFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_nosotros = id;
				else
					id_nosotros = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_nosotros : function(callback){
				$http.post(base_url+"/WebQuienesSomos/consultarNosotros", { id_idioma:id_idioma,id_nosotros:id_nosotros}).success(callback);
			}
	}
}])
.factory("directivaFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_directiva = id;
				else
					id_directiva = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_directiva : function(callback){
				$http.post(base_url+"/WebQuienesSomos/consultarDirectiva", { id_idioma:id_idioma,id_directiva:id_directiva}).success(callback);
			}
	}
}])
.factory("reaseguradorasFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_reaseguradoras = id;
				else
					id_reaseguradoras = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_reaseguradoras : function(callback){
				$http.post(base_url+"WebQuienesSomos/consultarReaseguradoras", { id_idioma:id_idioma,id_reaseguradoras:id_reaseguradoras}).success(callback);
			}
	}
}])
.factory("funcionaFactory",['$http',function($http){
	return{
	//-------------------
		asignar_valores : function (valor_idioma){
			if(valor_idioma!="")
				idioma = valor_idioma;
			else
				idioma = "";
		},
		cargar_funciona: function(callback){
			$http.post("./controladores/nosotrosController.php", {accion:'consultar_funciona','idioma':idioma}).success(callback);
		}
	//-------------------	
	}
	
}])

.factory("negociosFactory",['$http', function($http){
	return{
		asignar_idioma : function (idioma){
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
		},		
		cargar_negocios : function(callback){

			$http.post("./controladores/detallesNegociosController.php", {accion:'consultar_tipos_negocios',id_idioma:id_idioma}).success(callback);
		},
		//--Negocios detalles
		asignar_valores_detalles : function (valor_title_id,valor_tipo_negocio){
			if(valor_title_id!="")
				title_id = valor_title_id;
			else
				title_id = "";

			if(valor_tipo_negocio!="")
				tipo_negocio = valor_tipo_negocio;
			else
				tipo_negocio = "";
		},
		cargar_negocios_detalles : function(callback){
			$http.post("./controladores/detallesNegociosController.php", {accion:'consultar_negocios_detalles',title_id:title_id,tipo_negocio:tipo_negocio}).success(callback);
		}
		//--
	}	
}])
.factory("negociosFiltrosFactory",['$http', function($http){
	return{
		asignar_valores : function (valor_offset,valor_limit){
			if(valor_offset!=" ")
				offset = valor_offset;
			else
				offset = "";

			if(valor_limit!="")
				limit = valor_limit;
			else
				limit = "";
		},

		cargar_negocios : function(callback){
			$http.post("./controladores/detallesNegociosController.php", {accion:'consultar_negocios_filtros',limit:limit,offset:offset}).success(callback);
		}
	}
}])
.factory("tiposNegociosFactory",['$http', function($http){
	return{

		cargar_tipos_negocios : function(callback){
			$http.post("./controladores/detallesNegociosController.php", {accion:'consultar_tipos_negocios'}).success(callback);
		}
	}
}])
.factory("noticiasInicioFactory",['$http', function($http){
	return{

		cargar_noticias_inicio : function(callback){
			$http.post("./controladores/noticiasController.php", {accion:'consultar_noticias_inicio'}).success(callback);
		}
	}
}])
.factory("noticiasPrincipalFactory",['$http', function($http){
	return{
		asignar_valores : function (valor_offset,valor_limit){
			if(valor_offset!="")
				offset= valor_offset;
			else
				offset = "";
			if(valor_limit!="")
				limit= valor_limit;
			else
				limit = "";
		},

		cargar_noticias_principal : function(callback){
			$http.post("./controladores/noticiasController.php", {accion:'consultar_noticias_principal',offset:offset,limit:limit}).success(callback);
		}
	}
}])

.factory("videosFactory",['$http', function($http){
	return{
		asignar_valores : function (valor_x){
			if(valor_x!="")
				x= valor_x;
			else
				x = "";
		},
		cargar_videos : function(callback){
			$http.post("./controladores/videosController.php", {accion:'consultar_videos',x:x}).success(callback);
		}
	}
}])
.factory("videosPrincipalFactory",['$http', function($http){
	return{
		asignar_valores : function (valor_offset,valor_limit){
			if(valor_offset!="")
				offset= valor_offset;
			else
				offset = "";
			if(valor_limit!="")
				limit= valor_limit;
			else
				limit = "";
		},

		cargar_videos_limite : function(callback){
			$http.post("./controladores/videosController.php", {accion:'consultar_videos_principal',offset:offset,limit:limit}).success(callback);
		}
	}
}])
.factory("videosLimiteFactory",['$http', function($http){
	return{
		asignar_valores : function (valor_offset,valor_limit, valor_video_link){
			if(valor_offset!="")
				offset= valor_offset;
			else
				offset = "";
			
			if(valor_limit!="")
				limit= valor_limit;
			else
				limit = "";

			if(valor_video_link!="")
				video_link= valor_video_link;
			else
				video_link = "";
		},

		cargar_videos_limite : function(callback){
			$http.post("./controladores/videosController.php", {accion:'consultar_videos_limite',offset:offset,limit:limit, video_link:video_link}).success(callback);
		}
	}
}])
.factory("videosSubFactory",['$http', function($http){
	return{
		asignar_valores : function (valor_offset,valor_limit,valor_video_link){
			if(valor_offset!="")
				offset= valor_offset;
			else
				offset = "";
			if(valor_limit!="")
				limit= valor_limit;
			else
				limit = "";
			if(valor_video_link!="")
				video_link= valor_video_link;
			else
				video_link = "";
		},

		cargar_videos_limite : function(callback){
			$http.post("./controladores/videosController.php", {accion:'consultar_videos_sub',offset:offset,limit:limit,video_link:video_link}).success(callback);
		}
	}
}])
.factory("imagenFactory",['$http', function($http){
	return{
		asignar_valores : function (valor_offset,valor_limit){
			if(valor_offset!="")
				offset= valor_offset;
			else
				offset = "";
			if(valor_limit!="")
				limit= valor_limit;
			else
				limit = "";
		},

		cargar_imagenes : function(callback){
			$http.post("./controladores/imagenesController.php", {accion:'consultar_imagenes',offset:offset,limit:limit}).success(callback);
		}
	}	
}])
.factory("imagenesLimiteFactory",['$http', function($http){
	return{
		asignar_valores : function (valor_offset,valor_limit){
			if(valor_offset!="")
				offset= valor_offset;
			else
				offset = "";
			if(valor_limit!="")
				limit= valor_limit;
			else
				limit = "";
		},

		cargar_imagenes_limite : function(callback){
			$http.post("./controladores/imagenesController.php", {accion:'consultar_imagenes_limite',offset:offset,limit:limit}).success(callback);
		}
	}
}])
.factory("noticiasPreviewFactory",['$http', function($http){
	return{
		asignar_valores_preview : function (valor_title_id){
			if(valor_title_id!="")
				title_id = valor_title_id;
			else
				title_id = "";
		},
		cargar_noticias_preview : function(callback){
			$http.post("./controladores/noticiasController.php", {accion:'consultar_noticias_preview',title_id:title_id}).success(callback);
		}
	}
}])
.factory("direccionFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,urls){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_direccion = id;
				else
					id_direccion = "";
				if(urls!="")
					base_urls = urls;
				else
					base_urls = "";
			},
			cargar_direccion : function(callback){
				$http.post(base_urls+"WebInicio/consultarDireccion", { id_idioma:id_idioma,id_direccion:id_direccion}).success(callback);
			}
	}
}])
.factory("contactosFactory",['$http', function($http){
	return{
		asignar_valores : function (idioma,valor_offset,valor_limit){
			
			if(idioma!="")
				id_idioma= idioma;
			else
				id_idioma = "";

			if(valor_offset!="")
				offset= valor_offset;
			else
				offset = "";
			
			if(valor_limit!="")
				limit= valor_limit;
			else
				limit = "";

		},

		cargar_preguntas : function(callback){
			$http.post("./controladores/contactosController.php", {accion:'consultar_preguntas',id_idioma:id_idioma,offset:offset,limit:limit}).success(callback);
		}
	}
}])
.factory("redessocialesFactory",['$http', function($http){
	return{
		cargar_redes : function(callback){
			$http.post("./controladores/redesController.php", {accion:'consultar_redes'}).success(callback);
		}
	}
}])
.factory("correosFactory",['$http', function($http){
	return{
		cargar_redes : function(callback){
			$http.post("./controladores/correosController.php", {accion:'consultar_correos'}).success(callback);
		}
	}
}])
.factory('facebookService', function($q) {
    return {
        getMyLastName: function() {
            var deferred = $q.defer();
            FB.api('/me', {
                fields: 'last_name'
            }, function(response) {
                if (!response || response.error) {
                    deferred.reject('Error occured');
                } else {
                    deferred.resolve(response);
                }
            });
            return deferred.promise;
        }
    }
})
.factory("metaFactory",['$http',function($http){
	return{
		cargar_palabras_claves : function(callback){
			$http.post("./controladores/homeController.php", {accion:'consultar_palabras_claves'}).success(callback);
		}
	}
}])
.factory("blogFactory",['$http', function($http){
	return{
		asignar_limites : function (valor_offset,valor_limit,idioma,categoria,tags,valor_fecha){
			
			if(valor_offset!="")
				offset= valor_offset;
			else
				offset = "";
			
			if(valor_limit!="")
				limit= valor_limit;
			else
				limit = "";
			
			if(idioma!="")
				id_idioma= idioma;
			else
				id_idioma = "";

			if(categoria!="")
				id_categoria =	categoria;
			else
				id_categoria = "";

			if(tags!="")
				id_tags = tags;
			else
				id_tags = "";

			if(valor_fecha!="")
				fecha = valor_fecha;
			else
				fecha = valor_fecha;
		},
		asignar_valores_detalles : function(title_id,idioma){
			if(title_id!="")
				slug = title_id;
			else
				slug = "";
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
		},
		cargar_blog : function(callback){
			$http.post("./controladores/blogController.php", {accion:'consultar_blog_limite',offset:offset,limit:limit,id_idioma:id_idioma,id_categoria:id_categoria,id_tags:id_tags,fecha:fecha}).success(callback);
		},
		cargar_fechas : function(callback){
			$http.post("./controladores/blogController.php", {accion:'consultar_fechas',id_idioma:id_idioma}).success(callback);
		},
		cargar_blog_render : function(callback){
			$http.post("./controladores/blogController.php", {accion:'consultar_blog_render',offset:offset,limit:limit,id_idioma:id_idioma,id_categoria:id_categoria,id_tags:id_tags,fecha:fecha}).success(callback);
		},
		cargar_blog_detalles : function(callback){
			$http.post("./controladores/blogController.php", {accion:'consultar_blog_detalle',slug:slug,id_idioma:id_idioma}).success(callback);
		},
		cargar_super_blog : function(callback){
			$http.post("./controladores/blogController.php", {accion:'consultar_super_blog',offset:offset,limit:limit,id_idioma:id_idioma,id_categoria:id_categoria,id_tags:id_tags,fecha:fecha}).success(callback);
		},
	}
}])
.factory("publicidadFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url_base){
				
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				
				if(id!="")
					id_publicidad = id;
				else
					id_publicidad = "";
				
				if(url_base!="")
					bases_url = url_base;
				else
					bases_url = "";
			},
			cargar_publicidad : function(callback){
				$http.post(bases_url+"WebInicio/consultarPublicidad", { id_idioma:id_idioma,id_publicidad:id_publicidad}).success(callback);
			}
	}
}])
.factory("categoriasWebFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url_base){
				
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				
				if(id!="")
					id_categorias = id;
				else
					id_categorias = "";
				
				if(url_base!="")
					bases_url = url_base;
				else
					bases_url = "";
			},
			cargar_categorias : function(callback){
				$http.post(bases_url+"WebInicio/consultarCategorias", { id_idioma:id_idioma,id_categorias:id_categorias}).success(callback);
			}
	}
}])
.factory("empresaFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url_base){
				
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				
				if(id!="")
					id_empresa = id;
				else
					id_empresa = "";
				
				if(url_base!="")
					bases_url = url_base;
				else
					bases_url = "";
			},
			cargar_categorias : function(callback){
				$http.post(bases_url+"WebInicio/consultarEmpresa", { id_idioma:id_idioma,id_empresa:id_empresa}).success(callback);
			}
	}
}])
.factory("portadaFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url_base){
				
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				
				if(url_base!="")
					bases_url = url_base;
				else
					bases_url = "";
			},
			cargar_categorias : function(callback){
				$http.post(bases_url+"WebInicio/consultarPortada", { id_idioma:id_idioma}).success(callback);
			}
	}
}])
.factory("noticiasFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id_n,info_editorial,slug_noticia,url_base){
				
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				
				if(id_n!="")
					id_noticias = id_n; 
				else
					id_noticias = "";
				
				if(info_editorial!="")
					editorial = info_editorial;
				else
					editorial = "";

				if(slug_noticia!="")
					slug = slug_noticia;
				else
					slug = "";
				
				

				if(url_base!="")
					bases_url = url_base;
				else
					bases_url = "";
			},
			cargar_noticias : function(callback){
				$http.post(bases_url+"WebPublicacion/consultarNoticias", { id_idioma:id_idioma, id_noticias: id_noticias, editorial:editorial, slug:slug}).success(callback);
			}
	}
}])
.factory("noticiasLimiteFactory",['$http', function($http){
	return{
		asignar_valores : function (idioma,valor_offset,valor_limit, n_id,url_base){
			
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
			
			if(valor_offset!="")
				offset= valor_offset;
			else
				offset = "";
			
			if(valor_limit!="")
				limit= valor_limit;
			else
				limit = "";

			if(url_base!="")
					bases_url = url_base;
				else
					bases_url = "";

			if(n_id!="")
				no_id = n_id;
			else
				no_id = "";
		},

		cargar_noticias_limite : function(callback){
			$http.post(bases_url+"WebPublicacion/consultarNoticiasFiltros", { id_idioma:id_idioma, offset:offset, limit:limit, no_id:no_id}).success(callback);
		}
	}
}])
.factory("noticiasFactoryBuscador",['$http', function($http){
	return{
		asignar_valores : function (idioma,start_noticia,limit_noticia,tipo_noticia,id_noticia,url_base){
			
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
			
			if(tipo_noticia!="")
				tipo= tipo_noticia;
			else
				tipo = "";
			
			if(id_noticia!="")
				id= id_noticia;
			else
				id = "";

			if(limit_noticia!="")
				limit = limit_noticia;
			else
				limit = "";

			if(start_noticia!="")
				start = start_noticia;
			else
				start = "";

			if(url_base!="")
					bases_url = url_base;
				else
					bases_url = "";
		},

		cargar_noticias_buscador : function(callback){
			$http.post(bases_url+"WebPublicacion/consultarNoticiasBuscador", { id_idioma:id_idioma, tipo:tipo, id:id, limit: limit, start:start }).success(callback);
		}
	}
}])


.factory("categoriasBlogFactory",['$http', function($http){
	return{
			asignar_idioma : function (idioma){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
			},
			cargar_categorias_blog : function(callback){
				$http.post("./controladores/categorias_blogController.php", { accion:'consultar_categorias_blog',id_idioma:id_idioma}).success(callback);
			}
	}
}])

.factory("tagsFactory",['$http', function($http){
	return{
		asignar_idioma : function (idioma){
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
		},
		cargar_tags : function(callback){
			$http.post("./controladores/tagsController.php", {accion:'consultar_tags2',id_idioma:id_idioma}).success(callback);
		}
	}	
}])
.factory("footerFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url_base){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_footer = id;
				else
					id_footer = "";
				
				if(url_base!="")
					base_urls = url_base;
				else
					base_urls = "";
			},
			cargar_footer : function(callback){
				$http.post(base_urls+"/WebInicio/consultarFooter", { id_idioma:id_idioma,id_footer:id_footer}).success(callback);
			},
			cargar_redes : function(callback){
				$http.post(base_urls+"WebInicio/consultarRedes", {id_idioma:id_idioma}).success(callback);
			},
			cargar_meta_tag:function(callback){
				$http.post(base_urls+"WebInicio/consultarMetaTag", {id_idioma:id_idioma}).success(callback);
			},
	}
}])
//--Servicio para compartir datos de mensajes iniciales
.service('serverDataMensajes',[function(){
	var puente = [];
	this.puenteData = function(arreglo){
		puente = arreglo;
		return puente;
	}
	this.obtener_arreglo = function(){
		return puente;
	}
	this.limpiar_arreglo_servicio = function (){
		puente = [];
		return puente;
	}
}])
.service('bucadorTitulo',[function($http){
	this.buscarT = function(base_url){
		let buscar = $("#buscar_texto").val()
		let url_buscar = base_url+"buscador/titulos/"+buscar
		$("#formBuscar").attr("action",url_buscar)
		$("#formBuscar").attr("method","post");
		$("#formBuscar").submit()
	}
}])
.service('upload', ["$http", "$q", function ($http, $q) 
{
	this.uploadFile = function(file, id_empleo,nombre_archivo)
	{
		var deferred = $q.defer();
		var formData = new FormData();
		
		formData.append("id_empleo", id_empleo);
		formData.append("file", file);
		/*formData.append("categoria", categoria);
		formData.append("file", file);*/
		formData.append("nombre_archivo",nombre_archivo);
		return $http.post("./controladores/archivosController.php", formData, {
			headers: {
				"Content-type": undefined
			},
			transformRequest: angular.identity
		})
		.success(function(res)
		{
			deferred.resolve(res);
		})
		.error(function(msg, code)
		{
			deferred.reject(msg);
		})
		return deferred.promise;
	}	
}]).factory("productosFactory",['$http', function($http){
	return{
		asignar_valores : function (idioma,id,url){
			
			if(idioma!="")
				id_idioma= idioma;
			else
				id_idioma = "";

			if(id!="")
				id_productos= id;
			else
				id_productos = "";
			
				if(url!="")
				base_url = url;
			else
				base_url = "";

		},

		cargar_productos : function(callback){
			$http.post(base_url+"/WebProductos/consultarProductosTodas", { id_idioma:id_idioma,id_productos:id_productos}).success(callback);
		},
		cargar_productos_servicios : function(callback){
			$http.post(base_url+"/WebServicios/consultarProductosTodas", { id_idioma:id_idioma,id_productos:id_productos}).success(callback);
		}
	}
}]).factory("tproductosFactory",['$http', function($http){
	return{
		asignar_valores : function (idioma,id,slug,url){
			
			if(idioma!="")
				id_idioma= idioma;
			else
				id_idioma = "";

			if(id!="")
				id_productos= id;
			else
				id_productos = "";

			if(slug!="")
				slug_primario= slug;
			else
				slug_primario = "";

				if(url!="")
				base_url = url;
			else
				base_url = "";

		},

		cargar_productos : function(callback){
			$http.post(base_url+"/WebProductos/consultarTproductosTodas", { id_idioma:id_idioma,id_productos:id_productos,slug_primario:slug_primario}).success(callback);
		}
	}
}]).factory("detalleproductosFactory",['$http', function($http){
	return{
		asignar_valores : function (idioma,slug,slug_base,url){
			
			if(idioma!="")
				id_idioma= idioma;
			else
				id_idioma = "";

			if(slug_base!="")
				slug_primario= slug_base;
			else
				slug_primario = "";

			if(slug!="")
				slug_segundario= slug;
			else
				slug_segundario = "";

			if(url!="")
				base_url = url;
			else
				base_url = "";

		},
		cargar_productos : function(callback){
			$http.post(base_url+"/WebProductos/consultarDetalleTproductos", { id_idioma:id_idioma,slug_primario:slug_primario,slug_segundario:slug_segundario}).success(callback);
		}
	}
}]).factory("otrosproductosFactory",['$http', function($http){
	return{
		asignar_valores : function (producto,tipo_producto,){
			
			
			if(producto!="")
				id_producto= producto;
			else
				id_producto = "";
			if(tipo_producto!="")
				id_tipo_producto= tipo_producto;
			else
				id_tipo_producto = "";

			

		},
		cargar_productos : function(callback){
			$http.post(base_url+"/WebProductos/consultarOtrosTproductos", { id_idioma:id_idioma,id_tipo_producto:id_tipo_producto,id_producto:id_producto}).success(callback);
		}
	}
}]).factory("tserviciosFactory",['$http', function($http){
	return{
		asignar_valores : function (idioma,id,slug,url){
			
			if(idioma!="")
				id_idioma= idioma;
			else
				id_idioma = "";

			if(id!="")
				id_productos= id;
			else
				id_productos = "";

			if(slug!="")
				slug_primario= slug;
			else
				slug_primario = "";

				if(url!="")
				base_url = url;
			else
				base_url = "";

		},

		cargar_servicios : function(callback){
			$http.post(base_url+"/WebServicios/consultarTserviciosTodas", { id_idioma:id_idioma,id_productos:id_productos,slug_primario:slug_primario}).success(callback);
		}
	}
}]).factory("detalleserviciosFactory",['$http', function($http){
	return{
		asignar_valores : function (idioma,slug,slug_base,url){
			
			if(idioma!="")
				id_idioma= idioma;
			else
				id_idioma = "";

			if(slug_base!="")
				slug_primario= slug_base;
			else
				slug_primario = "";

			if(slug!="")
				slug_segundario= slug;
			else
				slug_segundario = "";

			if(url!="")
				base_url = url;
			else
				base_url = "";

		},
		cargar_servicios : function(callback){
			$http.post(base_url+"/WebServicios/consultarDetalleTservicios", { id_idioma:id_idioma,slug_primario:slug_primario,slug_segundario:slug_segundario}).success(callback);
		}
	}
}]).factory("otrosserviciosFactory",['$http', function($http){
	return{
		asignar_valores : function (servicio,tipo_servicio,){
			
			
			if(servicio!="")
				id_servicio= servicio;
			else
				id_servicio = "";
			if(tipo_servicio!="")
				id_tipo_servicio= tipo_servicio;
			else
				id_tipo_servicio = "";

			

		},
		cargar_servicios : function(callback){
			$http.post(base_url+"/WebServicios/consultarOtrosTservicios", { id_idioma:id_idioma,id_tipo_servicio:id_tipo_servicio,id_servicio:id_servicio}).success(callback);
		}
	}
}]).factory("serviciosFactory",['$http', function($http){
	return{
		asignar_valores : function (servicio,tipo_servicio,url){
			
			
			if(servicio!="")
				id_servicio= servicio;
			else
				id_servicio = "";
			if(tipo_servicio!="")
				id_tipo_servicio= tipo_servicio;
			else
				id_tipo_servicio = "";
				if(url!="")
				base_url = url;
			else
				base_url = "";
			

		},
		cargar_servicios : function(callback){
			$http.post(base_url+"/WebProductos/consultarServiciosTodas", { id_idioma:id_idioma,id_tipo_servicio:id_tipo_servicio,id_servicio:id_servicio}).success(callback);
		},
		cargar_servicios_principales : function(callback){
			$http.post(base_url+"/WebServicios/consultarServiciosTodas", { id_idioma:id_idioma,id_tipo_servicio:id_tipo_servicio,id_servicio:id_servicio}).success(callback);
		},
	}
}]).factory("productoshomeFactory",['$http', function($http){
	return{
		asignar_valores : function (idioma,id,url){
			
			if(idioma!="")
				id_idioma= idioma;
			else
				id_idioma = "";

			if(id!="")
				id_productos= id;
			else
				id_productos = "";
			
				if(url!="")
				base_url = url;
			else
				base_url = "";

		},

		cargar_productos_home : function(callback){
			$http.post(base_url+"/WebInicio/consultarProductosTodas", { id_idioma:id_idioma,id_productos:id_productos}).success(callback);
		},
	}
}]).factory("servicioshomeFactory",['$http', function($http){
	return{
		asignar_valores : function (idioma,id,url){
			
			if(idioma!="")
				id_idioma= idioma;
			else
				id_idioma = "";

			if(id!="")
				id_productos= id;
			else
				id_productos = "";
			
				if(url!="")
				base_url = url;
			else
				base_url = "";

		},

		cargar_servicios_home : function(callback){
			$http.post(base_url+"/WebInicio/consultarUniServiciosTodas", { id_idioma:id_idioma,id_productos:id_productos}).success(callback);
		},
	}
}]).factory("cuentaBancariaFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_cuenta = id;
				else
					id_cuenta = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_banco : function(callback){
				$http.post(base_url+"/WebContactos/consultarCuentaTodas", { id_idioma:id_idioma,id_cuenta:id_cuenta}).success(callback);
			},
	}
}]).factory("cargaPdfFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_categoria = id;
				else
					id_categoria = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_banco : function(callback){
				$http.post(base_url+"/WebContactos/consultarPdf", { id_idioma:id_idioma,id_categoria:id_categoria}).success(callback);
			},
	}
}]);