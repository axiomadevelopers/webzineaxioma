angular.module("WebZineApp")
	.controller("inicioController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,portadaFactory,empresaFactory,categoriasWebFactory,publicidadFactory,noticiasFactory,bucadorTitulo){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$scope.idioma = 1
		let img_editorial = $scope.base_url+'assets/web/images/home/slider1.jpeg'
		let img_noticias = $scope.base_url+'assets/web/images/home/slider3.jpeg'
		
		let publicidad = $scope.base_url+'assets/web/images/publicidad/publicidad1.jpg'
		let publicidad2 = $scope.base_url+'assets/web/images/publicidad/publicidad2.png'
		//--Cuerpo de metodos
		$scope.consultar_publicidad = function(){
			publicidadFactory.asignar_valores("","",$scope.base_url)
			publicidadFactory.cargar_publicidad(function(data){
				$scope.publicidades=data;
			});
		}
		//--
		$scope.consultar_categorias = function(){
			categoriasWebFactory.asignar_valores("","",$scope.base_url)
			categoriasWebFactory.cargar_categorias(function(data){
				$scope.categorias_blog=data;
			});
		}
		//--
		$scope.consultar_empresa = function(){
			empresaFactory.asignar_valores("","",$scope.base_url)
			empresaFactory.cargar_categorias(function(data){
				$scope.empresa=data[0];
			});
		}
		//--
		$scope.consultar_portada = function(){
			portadaFactory.asignar_valores("",$scope.base_url)
			portadaFactory.cargar_categorias(function(data){
				$scope.portada=data[0];
			});
		}
		//--
		$scope.consultar_editorial = function(){
			noticiasFactory.asignar_valores("","","1","",$scope.base_url)
			noticiasFactory.cargar_noticias(function(data){
				$scope.editorial=data[0];
				console.log($scope.editorial)
			})
		}
		//--
		$scope.consultar_post = function(){
			noticiasFactory.asignar_valores("","","","",$scope.base_url)
			noticiasFactory.cargar_noticias(function(data){
				$scope.posts=data;
			})
		}
		//--
        $scope.buscarTitulos = function(){
        	bucadorTitulo.buscarT($scope.base_url);
        }
       
		$scope.direccion_mapas = []
		$scope.direccion_mapas_telefono = []
		//---------------------------
		$scope.consultar_publicidad()
		$scope.consultar_categorias()
		$scope.consultar_empresa()
		$scope.consultar_portada()
		$scope.consultar_editorial()
		$scope.consultar_post()
	});