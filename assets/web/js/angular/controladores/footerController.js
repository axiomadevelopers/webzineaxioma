angular.module("WebZineApp")
	.controller("footerController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,footerFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();

		$scope.consultar_redes = function () {
			footerFactory.asignar_valores("","",$scope.base_url);
			footerFactory.cargar_redes(function (data) {
				$scope.redes = data;
				$scope.instagram =$scope.redes[0].url_red;
				$scope.facebook =$scope.redes[1].url_red;
				$scope.twitter =$scope.redes[2].url_red;
				$scope.linkedin =$scope.redes[3].url_red;
			});
		}

		$scope.consultarFooter = function(){
			footerFactory.asignar_valores("","",$scope.base_url)
			footerFactory.cargar_footer(function(data){
				$scope.footer=data[0];
			});
		}
		//---
		$scope.consultar_meta = function () {
			footerFactory.cargar_meta_tag(function (data) {
				$scope.meta_descripcion = data.descripcion;
				$scope.meta_palabras_claves = data.palabras_claves;

				$("meta[name='keywords']").attr('content', $scope.meta_palabras_claves);
				$("meta[name='description']").attr('content', $scope.meta_descripcion);
				//console.log(data);
			});
		}
		//---
		$scope.consultar_direccion = function(){
			direccionFactory.asignar_valores("","",$scope.base_url)
			direccionFactory.cargar_direccion(function(data){
				$scope.direccion=data[0];
				console.log($scope.direccion)
			});
		}
		//--
		$scope.consultar_redes()
		$scope.consultar_direccion()
		$scope.consultarFooter()
		//--
	});