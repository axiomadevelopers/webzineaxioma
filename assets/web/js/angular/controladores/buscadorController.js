angular.module("WebZineApp")
	.controller("buscadorController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,noticiasFactoryBuscador,categoriasWebFactory,publicidadFactory,bucadorTitulo){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$scope.id = $("#id_filtro").val();
		$scope.tipo = $("#tipo_filtro").val();
		$scope.idioma = 1
		$scope.limit = "2"
		$scope.start = "0"
		$scope.mensaje = ""
		$scope.posts_buscador_mas = []
		$scope.posts_buscador =	{
								'titulo':'',
								'descripcion':'',
								'img':'',
								'ruta':'',
								'autor':'',
								'fecha':'',
								'dias':'',
								'mes':'',
								'usuario':'',
						}
		//--Cuerpo de metodos
		$scope.consultar_publicidad = function(){
			publicidadFactory.asignar_valores("","",$scope.base_url)
			publicidadFactory.cargar_publicidad(function(data){
				$scope.publicidades=data;
			});
		}
		//--
		$scope.consultar_categorias = function(){
			categoriasWebFactory.asignar_valores("","",$scope.base_url)
			categoriasWebFactory.cargar_categorias(function(data){
				$scope.categorias_blog=data;
			});
		}
		//--
		$scope.consultar_post_buscador = function(){
			noticiasFactoryBuscador.asignar_valores("1",$scope.start,$scope.limit,$scope.tipo,$scope.id,$scope.base_url)
			noticiasFactoryBuscador.cargar_noticias_buscador(function(data){
				console.log(data[0])
				if(data[0]==undefined){
					$scope.mensaje = "No existen registros asociados!"
				}else{
					$scope.posts_buscador=data;
					console.log($scope.posts_buscador);
					$scope.start += 1;
				}
			})
		}
		//--
		$scope.carga_noticias_leer_mas = function(){
			noticiasFactoryBuscador.asignar_valores("1",$scope.start,$scope.limit,$scope.tipo,$scope.id,$scope.base_url)
			noticiasFactoryBuscador.cargar_noticias_buscador(function(data){
			//----------------------------------------
				console.log(data[0])
				if (data === null || data == "null" || data[0]==undefined) {
						mensaje_alerta("#campo_mensaje_noticias","Todos los post fueron cargadas!","alert-warning");
				} else {
					for (i in data) {
						$scope.posts_buscador_mas.push(data[i]);
					}
					console.log($scope.posts_buscador_mas)
					$scope.start += 1;
				}
			//----------------------------------------
			})
		}
		//--
		$scope.cargar_mas_subnoticias = function(){
			$.when(
				mensaje_alerta("#campo_mensaje_noticias",pre_load,"alert-warning"),
				$scope.carga_noticias_leer_mas()
			).then(function(){
				 $("#campo_mensaje_noticias").fadeOut(1400)
				 $("#campo_mensaje_noticias").show()
			});
		}
		//--
		$scope.buscarTitulos = function(){
        	bucadorTitulo.buscarT($scope.base_url);
        }
		//--
		$scope.contactos = {
                                'id':'',
                                'nombres':'',
                                'email':'',
                                'telefono':'',
                                'mensaje':''
        }
        $scope.consultar_publicidad()
        $scope.consultar_categorias()
		$scope.consultar_post_buscador() 
		$scope.direccion_mapas = []
		$scope.direccion_mapas_telefono = []
		//---------------------------
	});