angular.module("WebZineApp")
	.controller("contactanosController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,nosotrosFactory,directivaFactory,reaseguradorasFactory,cuentaBancariaFactory,cargaPdfFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$("#span-menu4").addClass("subtitulo-menu2")
		$scope.idioma = 1
		$scope.contactos = {
                                'id':'',
                                'nombres':'',
                                'email':'',
                                'telefono':'',
                                'mensaje':''
        }
		//---------------------------------------------------------------------
		//--
		$scope.validar_guardar = function(){
			if($scope.contactos.nombres==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar sus nombres", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter their names", "alert-danger");

				return false;
			}else
			if($("#form-email").val()==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar una dirección de correo v&aacute;lida", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter valid email direction", "alert-danger");
				return false;
			}
			else
			if($("#form-tlf").val()==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar un número de teléfono válido", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter valid telephone number", "alert-danger");
				return false;
			}
			else if($scope.contactos.mensaje==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar un mensaje", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter a message", "alert-danger");
				return false;
			}else
				return true;
		}
		//--
		//--Registrar contactos
		$scope.registrar_contactos = function(){
			if($scope.validar_guardar()==true){
				uploader_reg("#campo_mensaje_clientes",".campos-form");
				$http.post($scope.base_url+"/WebContactos/registrarContactos",{
					'nombres': $scope.contactos.nombres,
					'email': $scope.contactos.email,
					'telefono': $scope.contactos.telefono,
					'mensaje': $scope.contactos.mensaje
				}).success(function(data, estatus, headers, config){
					$scope.mensajes  = data;
					if($scope.mensajes.mensaje == "registro_procesado"){
						if($scope.idioma == "1")
							mensaje_alerta("#campo_mensaje_clientes","Gracias por tu tiempo, tu mensaje ha sido enviado!","alert-success");
						else
							mensaje_alerta("#campo_mensaje_clientes","Thank you for your time, your message has been sent!","alert-success");
						$scope.limpiar_cajas();
					}else if($scope.mensajes.mensaje =="existe"){
						if($scope.idioma == "1")
							mensaje_alerta("#campo_mensaje_clientes","Ya fue registrado un contacto con esa dirección de email","alert-success");
						else
							mensaje_alerta("#campo_mensaje_clientes","A contact has already been registered with that email address","alert-success");
					}
					else{
						mensaje_alerta("#campo_mensaje_clientes","Ocurrió un error inesperado","alert-danger");

					}
					desbloquear_pantalla("#campo_mensaje_clientes",".campos-form")
				}).error(function(data,estatus){
					showErrorMessage("Ocurrió un error inesperado:"+data);
				})
			}
		}

		//--Limpiar cajas
		$scope.limpiar_cajas = function(){
			$scope.contactos = {
									"id":"",
									"nombres":"",
									"email":"",
									"telefono":"",
									"mensaje":""
			}
		}
		//-- consultar_direccion
		$scope.consultar_direccion = function(){
			direccionFactory.asignar_valores("","",$scope.base_url)
			direccionFactory.cargar_direccion(function(data){
				$scope.direccion=data;
				//console.log($scope.direccion);
				$scope.direccion_mapas = $scope.direccion[0]
			  	$scope.direccion_mapas_telefono = $scope.direccion_mapas.telefono
			  	//---------------------------------------------------------------
			  	var latitud= $scope.direccion_mapas.latitud;  
				var longitud= $scope.direccion_mapas.longitud;
				var numero = 0;
				google.maps.event.addDomListener(window, 'load', initialize_standar(latitud,longitud,numero));
				setTimeout(function(){
					$("#listaLocalizacion0").addClass("alert-success")
				},5000)
			});
		}
		/*
		*	preSeleccionarRenglon
		*/
		$scope.preSeleccionarRenglon = function(indice){
			//---
			$( ".lista-localizacion" ).each(function( index ) {
			  	if(!$(this).hasClass("alert-success")){
			  		$(this).removeClass("alert-info")
				}
			});
			//---
			if(!$(this).hasClass("alert-success")){
		  		$("#listaLocalizacion"+indice).addClass("alert-info")
		  	}
		  	//---	
		}
		/*
		*	seleccionaRenglon
		*/
		$scope.seleccionaRenglon = function(indice){
			//---
			$( ".lista-localizacion" ).each(function( index ) {
				$(this).removeClass("alert-info").removeClass("alert-success")
			});
			//---
		  	$("#listaLocalizacion"+indice).addClass("alert-success")
		  	$scope.direccion_mapas = $scope.direccion[indice]
		  	$scope.direccion_mapas_telefono = $scope.direccion_mapas.telefono
		  	//---------------------------------------------------------------
		  	var latitud= $scope.direccion_mapas.latitud;  
			var longitud= $scope.direccion_mapas.longitud;
			var numero = 0;
			google.maps.event.addDomListener(window, 'load', initialize_standar(latitud,longitud,numero));
		  	//---------------------------------------------------------------
		  	//console.log($scope.direccion_mapas)
		}
		/*
		*	Carga de cuentas bancarias
		*/
		$scope.consultar_banco = function(){
			cuentaBancariaFactory.asignar_valores("","",$scope.base_url)
			cuentaBancariaFactory.cargar_banco(function(data){
				$scope.banco=data;
				//console.log($scope.banco);				
			});
		}
		/*
		*	consultar_pdf
		*/
		$scope.consultar_pdf = function(){
			cargaPdfFactory.asignar_valores("","30",$scope.base_url)
			cargaPdfFactory.cargar_banco(function(data){
				$scope.pdf=data[0];
				console.log($scope.pdf);				
			});
		}

		/*
		*	desSeleccionarRenglon
		*/
		$scope.desSeleccionarRenglon = function(indice){
			$("#listaLocalizacion"+indice).removeClass("alert-info")
		}
		//--
		$scope.consultar_direccion()
		$scope.consultar_banco()
		$scope.consultar_pdf()
		//--
		//---------------------------------------------------------------------
	})
	.controller("detallesProductosController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,nosotrosFactory,directivaFactory,reaseguradorasFactory,detalleproductosFactory,otrosproductosFactory,serviciosFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$("#span-menu2").addClass("subtitulo-menu2")
		//--				
		$scope.slug  = $("#slug").val();
		$scope.slug_base  = $("#slug_base").val();
		//console.log($scope.slug);
		//console.log($scope.slug_base);
		$scope.consultar_productos = function(){
			detalleproductosFactory.asignar_valores("",$scope.slug,$scope.slug_base,$scope.base_url)
			detalleproductosFactory.cargar_productos(function(data){
				$scope.producto_base=data.producto_base;
				$scope.tipo_producto=data.tipo_producto;
				$scope.detalles_productos=data.detalles_productos;
				//Creo una variable con el orde inicial '1'
				$scope.orden_detalles_productos = $scope.detalles_productos[0].orden;
				$scope.iniciar_detalle_seleccionado($scope.detalles_productos[0])
				$scope.consultar_otros_tipo_productos($scope.tipo_producto.id,$scope.producto_base.id);

			});
		}
		$scope.iniciar_detalle_seleccionado = function(data){
			//console.log(data);
			$scope.detalles_iniciales = data;
			$scope.titulo_detalle_producto = $scope.detalles_iniciales.titulo;
			$scope.descripcion_detalle_producto = $scope.detalles_iniciales.descripcion;
			$("#descripcion_detalle_producto").empty();
			$("#descripcion_detalle_producto").hide().append($scope.descripcion_detalle_producto).fadeIn(1000);
		}
		$scope.detalle_seleccionado = function(event){
					$scope.titulo_detalle_producto = []
					$scope.descripcion_detalle_producto = []
					
                    var caja = event.currentTarget.id;
                    var atributos = $("#"+caja).attr("data");
                    var arreglo_atributos = atributos.split("|");
					//console.log(arreglo_atributos);

				   $scope.titulo_detalle_producto = arreglo_atributos[0];
				   $scope.descripcion_detalle_producto = arreglo_atributos[1];
				   $("#descripcion_detalle_producto").empty();
				   $("#descripcion_detalle_producto").hide().append($scope.descripcion_detalle_producto).fadeIn(1000);  	

		}
		$scope.consultar_otros_tipo_productos = function(tipo_producto,producto_base){
			$scope.id_tipo_producto = tipo_producto;
			$scope.id_producto = producto_base;
			otrosproductosFactory.asignar_valores($scope.id_producto,$scope.id_tipo_producto)
			otrosproductosFactory.cargar_productos(function(data){
				//console.log(data);
				$scope.otros_productos=data;
				var keys = Object.keys(data);
				$scope.lenOtrosProductos = keys.length;
			});
		}
		
		$scope.consultar_algunos_servicios = function(){
			serviciosFactory.asignar_valores("","",$scope.base_url)
			serviciosFactory.cargar_servicios(function(data){
				
				$scope.algunos_servicios=data;
				//console.log($scope.algunos_servicios)

			});
		}

		$scope.activarAcordeon = function(index){
			$(".btn-link-productos").removeClass("acordion-text-selected");
			$("#acordeon"+index).addClass("acordion-text-selected");
		}

		$scope.consultar_algunos_servicios();
		$scope.consultar_productos();

	})
	.controller("detallesServiciosController", function ($scope, $http, $location, direccionFactory, redessocialesFactory, metaFactory, nosotrosFactory, productosFactory, detalleserviciosFactory,otrosserviciosFactory ) {
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$("#span-menu3").addClass("subtitulo-menu2")
		$scope.slug = $("#slug").val();
		$scope.slug_base = $("#slug_base").val();
		//console.log($scope.slug_base);
		//console.log($scope.slug);
		$scope.consultar_servicios = function () {
			detalleserviciosFactory.asignar_valores("", $scope.slug, $scope.slug_base, $scope.base_url)
			detalleserviciosFactory.cargar_servicios(function (data) {
				//console.log(data);
				$scope.servicio_base = data.servicio_base;
				$scope.tipo_servicio = data.tipo_servicio;
				$scope.detalles_servicios = data.detalles_servicios;
				$scope.orden_detalles_servicios = $scope.detalles_servicios[0].orden;
				$scope.iniciar_detalle_seleccionado($scope.detalles_servicios[0])
				$scope.consultar_otros_tipo_servicios($scope.tipo_servicio.id,$scope.servicio_base.id);


			});
		}

		$scope.iniciar_detalle_seleccionado = function (data) {
			//console.log(data);
			$scope.detalles_iniciales = data;
			$scope.titulo_detalle_servicio = $scope.detalles_iniciales.titulo;
			$scope.descripcion_detalle_servicio = $scope.detalles_iniciales.descripcion;
			$("#descripcion_detalle_servicio").empty();
			$("#descripcion_detalle_servicio").hide().append($scope.descripcion_detalle_servicio).fadeIn(1000);;

		}
		$scope.detalle_seleccionado = function (event) {
			$scope.titulo_detalle_servicio = []
			$scope.descripcion_detalle_servicio = []

			var caja = event.currentTarget.id;
			var atributos = $("#" + caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			//console.log(arreglo_atributos);

			$scope.titulo_detalle_servicio = arreglo_atributos[0];
			$scope.descripcion_detalle_servicio = arreglo_atributos[1];
			$("#descripcion_detalle_servicio").empty();
			$("#descripcion_detalle_servicio").hide().append($scope.descripcion_detalle_servicio).fadeIn(1000);
			
		}
		$scope.consultar_otros_tipo_servicios = function(tipo_servicio,servicio_base){
			$scope.id_tipo_servicio = tipo_servicio;
			$scope.id_servicio = servicio_base;
			otrosserviciosFactory.asignar_valores($scope.id_servicio,$scope.id_tipo_servicio)
			otrosserviciosFactory.cargar_servicios(function(data){
				//console.log(data);
				$scope.otros_servicios=data;


			});
		}
		$scope.consultar_algunos_productos = function(){
			productosFactory.asignar_valores("","",$scope.base_url)
			productosFactory.cargar_productos_servicios(function(data){
				
				$scope.algunos_productos=data;
				//console.log($scope.algunos_productos)

			});
		}

		$scope.activarAcordeon = function(index){
			$(".btn-link-productos").removeClass("acordion-text-selected");
			$("#acordeon"+index).addClass("acordion-text-selected");
		}
		
		$scope.consultar_algunos_productos();
		$scope.consultar_servicios();
	})
	.controller("inicioController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,nosotrosFactory,productoshomeFactory,servicioshomeFactory,cargaPdfFactory,cuentaBancariaFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$scope.idioma = 1
		//--Quienes somos
		$scope.nosotros  = {
									'id' : '',
									'titulo' : '',
									'descripcion' : '',
									'id_idioma' : '',
									'icono':'',
									'idioma' : '',
									'id_imagen':'',
		}
		$scope.contactos = {
                                'id':'',
                                'nombres':'',
                                'email':'',
                                'telefono':'',
                                'mensaje':''
        }
		$scope.direccion_mapas = []
		$scope.direccion_mapas_telefono = []
		//---------------------------
		$scope.consultar_nosotros = function(){
			nosotrosFactory.asignar_valores($scope.id_idioma,"",$scope.base_url)
			nosotrosFactory.cargar_nosotros(function(data){
				$scope.nosotros=data;
				//console.log($scope.nosotros)
			})	
		}
		//--
		$scope.validar_guardar = function(){
			if($scope.contactos.nombres==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar sus nombres", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter their names", "alert-danger");

				return false;
			}else
			if($("#form-email").val()==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar una dirección de correo v&aacute;lida", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter valid email direction", "alert-danger");
				return false;
			}
			else
			if($("#form-tlf").val()==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar un número de teléfono válido", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter valid telephone number", "alert-danger");
				return false;
			}
			else if($scope.contactos.mensaje==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar un mensaje", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter a message", "alert-danger");
				return false;
			}else
				return true;
		}
		//--
		//--Registrar contactos
		$scope.registrar_contactos = function(){
			if($scope.validar_guardar()==true){
				uploader_reg("#campo_mensaje_clientes",".campos-form");
				$http.post($scope.base_url+"/WebContactos/registrarContactos",{
					'nombres': $scope.contactos.nombres,
					'email': $scope.contactos.email,
					'telefono': $scope.contactos.telefono,
					'mensaje': $scope.contactos.mensaje
				}).success(function(data, estatus, headers, config){
					$scope.mensajes  = data;
					if($scope.mensajes.mensaje == "registro_procesado"){
						if($scope.idioma == "1")
							mensaje_alerta("#campo_mensaje_clientes","Gracias por tu tiempo, tu mensaje ha sido enviado!","alert-success");
						else
							mensaje_alerta("#campo_mensaje_clientes","Thank you for your time, your message has been sent!","alert-success");
						$scope.limpiar_cajas();
					}else if($scope.mensajes.mensaje =="existe"){
						if($scope.idioma == "1")
							mensaje_alerta("#campo_mensaje_clientes","Ya fue registrado un contacto con esa dirección de email","alert-success");
						else
							mensaje_alerta("#campo_mensaje_clientes","A contact has already been registered with that email address","alert-success");
					}
					else{
						mensaje_alerta("#campo_mensaje_clientes","Ocurrió un error inesperado","alert-danger");

					}
					desbloquear_pantalla("#campo_mensaje_clientes",".campos-form")
				}).error(function(data,estatus){
					showErrorMessage("Ocurrió un error inesperado:"+data);
				})
			}
		}

		//--Limpiar cajas
		$scope.limpiar_cajas = function(){
			$scope.contactos = {
									"id":"",
									"nombres":"",
									"email":"",
									"telefono":"",
									"mensaje":""
			}
		}
		//-- consultar_direccion
		$scope.consultar_direccion = function(){
			direccionFactory.asignar_valores("","",$scope.base_url)
			direccionFactory.cargar_direccion(function(data){
				$scope.direccion=data;
				//console.log($scope.direccion);
				$scope.direccion_mapas = $scope.direccion[0]
			  	$scope.direccion_mapas_telefono = $scope.direccion_mapas.telefono
			  	//---------------------------------------------------------------
			  	var latitud= $scope.direccion_mapas.latitud;  
				var longitud= $scope.direccion_mapas.longitud;
				var numero = 0;
				google.maps.event.addDomListener(window, 'load', initialize_standar(latitud,longitud,numero));
				setTimeout(function(){
					$("#listaLocalizacion0").addClass("alert-success")
				},5000)
			});
		}
		/*
		*	preSeleccionarRenglon
		*/
		$scope.preSeleccionarRenglon = function(indice){
			//---
			$( ".lista-localizacion" ).each(function( index ) {
			  	if(!$(this).hasClass("alert-success")){
			  		$(this).removeClass("alert-info")
				}
			});
			//---
			if(!$(this).hasClass("alert-success")){
		  		$("#listaLocalizacion"+indice).addClass("alert-info")
		  	}
		  	//---	
		}
		/*
		*	seleccionaRenglon
		*/
		$scope.seleccionaRenglon = function(indice){
			//---
			$( ".lista-localizacion" ).each(function( index ) {
				$(this).removeClass("alert-info").removeClass("alert-success")
			});
			//---
		  	$("#listaLocalizacion"+indice).addClass("alert-success")
		  	$scope.direccion_mapas = $scope.direccion[indice]
		  	$scope.direccion_mapas_telefono = $scope.direccion_mapas.telefono
		  	//---------------------------------------------------------------
		  	var latitud= $scope.direccion_mapas.latitud;  
			var longitud= $scope.direccion_mapas.longitud;
			var numero = 0;
			google.maps.event.addDomListener(window, 'load', initialize_standar(latitud,longitud,numero));
		  	//---------------------------------------------------------------
		  	//console.log($scope.direccion_mapas)
		}
		/*
		*	desSeleccionarRenglon
		*/
		$scope.desSeleccionarRenglon = function(indice){
			$("#listaLocalizacion"+indice).removeClass("alert-info")
		}
		//--
		$scope.consultar_productos = function(){
			productoshomeFactory.asignar_valores("","",$scope.base_url)
			productoshomeFactory.cargar_productos_home(function(data){
				$scope.productos=data;
				//console.log($scope.productos)

			});
		}
		$scope.consultar_servicios = function(){
			servicioshomeFactory.asignar_valores("","",$scope.base_url)
			servicioshomeFactory.cargar_servicios_home(function(data){
				$scope.servicios=data;
				$scope.tservicios=data.tservicios;
				console.log($scope.servicios)
				//console.log($scope.tservicios)

			});
		}

		/*
		*	Carga de cuentas bancarias
		*/
		$scope.consultar_banco = function(){
			cuentaBancariaFactory.asignar_valores("","",$scope.base_url)
			cuentaBancariaFactory.cargar_banco(function(data){
				$scope.banco=data;
				//console.log($scope.banco);				
			});
		}
		/*
		*	consultar_pdf
		*/
		$scope.consultar_pdf = function(){
			cargaPdfFactory.asignar_valores("","30",$scope.base_url)
			cargaPdfFactory.cargar_banco(function(data){
				$scope.pdf=data[0];
				console.log($scope.pdf);				
			});
		}
		//--
		$scope.consultar_banco()
		$scope.consultar_pdf()
		//---------------------------
		$scope.consultar_servicios();
		$scope.consultar_productos();
		$scope.consultar_nosotros()
		$scope.consultar_direccion()
		//---------------------------
	})
	.controller("mainController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,footerFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();

		$scope.consultar_redes = function () {
			footerFactory.asignar_valores("","",$scope.base_url);
			footerFactory.cargar_redes(function (data) {
				$scope.redes = data;
				//console.log($scope.redes);
			});
		}

		$scope.consultarFooter = function(){
			footerFactory.asignar_valores("","",$scope.base_url)
			footerFactory.cargar_footer(function(data){
				$scope.footer=data[0];
				//console.log(data[0]);
			});
		}

		$scope.consultar_meta = function () {
			footerFactory.cargar_meta_tag(function (data) {
				$scope.meta_descripcion = data.descripcion;
				$scope.meta_palabras_claves = data.palabras_claves;

				$("meta[name='keywords']").attr('content', $scope.meta_palabras_claves);
				$("meta[name='description']").attr('content', $scope.meta_descripcion);
				//console.log(data);
			});
		}

		//--
		$scope.consultarFooter()
		$scope.consultar_redes()
		$scope.consultar_meta()
		//--
	})
	.controller("nosotrosController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,nosotrosFactory,directivaFactory,reaseguradorasFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$("#span-menu1").addClass("subtitulo-menu2")
		//--Quienes somos
		$scope.nosotros  = {
									'id' : '',
									'titulo' : '',
									'descripcion' : '',
									'id_idioma' : '',
									'icono':'',
									'idioma' : '',
									'id_imagen':'',
		}
		//---------------------------
		$scope.mostrar_contenido = function(opcion){
			$("#modal-2").modal("show")
			var lista = "";
			if(opcion==1){
				lista = $(".lista-mision ul").html()
				$( ".lista-mision ul" ).each(function( index ) {
					lista+=$( this ).html();
				});

				$("#ul-modal").html(lista);
				$scope.titulo_modal = "Misión, contamos con:"
			}else if(opcion==2){
				lista = $(".lista-vision ul").html()
				$( ".lista-vision ul" ).each(function( index ) {
					lista+=$( this ).html();
				});

				$("#ul-modal").html(lista);
				$scope.titulo_modal = "Visión, contando con:"
			}
		}
		//---------------------------
		$scope.consultar_nosotros = function(){
			nosotrosFactory.asignar_valores($scope.id_idioma,"",$scope.base_url)
			nosotrosFactory.cargar_nosotros(function(data){
				$scope.nosotros=data;
				//console.log($scope.nosotros)
			})	
		}
		$scope.consultar_directiva = function(){
			directivaFactory.asignar_valores("","",$scope.base_url)
			directivaFactory.cargar_directiva(function(data){
				$scope.directiva=data;
				//console.log($scope.directiva);	
				setTimeout(function(){
					//----------------
					$(".carousel-directiva").each(function() {
			            $(this).owlCarousel($.extend({
				                navigation: !1,
				                pagination: !0,
				                autoPlay: 3e3,
				                items: 3,
				                navigationText: ['<span class="arrows arrows-arrows-slim-left"></span>', '<span class="arrows arrows-arrows-slim-right"></span>']
			            }, $(this).data("carousel-options")))
		        	})	
					//----------------
				},500)
						
			});
		}
		$scope.consultar_reaseguradoras = function(){
			reaseguradorasFactory.asignar_valores("","",$scope.base_url)
			reaseguradorasFactory.cargar_reaseguradoras(function(data){
				$scope.reaseguradoras=data;
				//console.log($scope.reaseguradoras);	
				//---------------------------------
				setTimeout(function(){
					$(".carousel-reaseguradoras").each(function() {
			            $(this).owlCarousel($.extend({
			                navigation: !1,
			                pagination: !1,
			                autoPlay: 3e3,
			                items: 4,
			                navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
			            }, $(this).data("carousel-options")))
			        })
			    },500)        
		        //-----------------------------------			
			});
		}
		//---------------------------
		$scope.consultar_nosotros()
		$scope.consultar_directiva()
		$scope.consultar_reaseguradoras();
	})
	.controller("productosController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,nosotrosFactory,directivaFactory,reaseguradorasFactory,productosFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$("#span-menu2").addClass("subtitulo-menu2")
		//--				

		$scope.consultar_productos = function(){
			productosFactory.asignar_valores("","",$scope.base_url)
			productosFactory.cargar_productos(function(data){
				$scope.productos=data;
				//console.log($scope.productos)

			});
		}
		$scope.consultar_productos();
	})
	.controller("serviciosController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,nosotrosFactory,directivaFactory,serviciosFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$("#span-menu3").addClass("subtitulo-menu2")
		$scope.consultar_servicios = function(){
			serviciosFactory.asignar_valores("","",$scope.base_url)
			serviciosFactory.cargar_servicios_principales(function(data){
				$scope.servicios=data;
				//console.log($scope.servicios)
			});
		}
		$scope.consultar_servicios();
	})
	.controller("tiposProductosController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,nosotrosFactory,directivaFactory,reaseguradorasFactory,tproductosFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$("#span-menu2").addClass("subtitulo-menu2")
		//--				
		$scope.slug_producto  = $("#slug_producto").val();
		//console.log($scope.slug_producto);
		
		$scope.consultar_productos = function(){
			tproductosFactory.asignar_valores("","",$scope.slug_producto,$scope.base_url)
			tproductosFactory.cargar_productos(function(data){
				console.log(data)
				$scope.titulo=data.titulo;
				$scope.ruta=data.ruta;
				$scope.slug_base=data.slug;
				$scope.tproductos=data.tproductos;
				$scope.otros_productos=data.otros_productos;
				//console.log(data)

			});
		}
		$scope.consultar_productos();

	})
	.controller("tiposServiciosController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,nosotrosFactory,directivaFactory,reaseguradorasFactory,tserviciosFactory,productosFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$("#span-menu3").addClass("subtitulo-menu2")
		$scope.slug_servicio  = $("#slug_producto").val();
		//console.log($scope.slug_servicio);
		$scope.consultar_servicio = function(){
			tserviciosFactory.asignar_valores("","",$scope.slug_servicio,$scope.base_url)
			tserviciosFactory.cargar_servicios(function(data){
				
				$scope.ruta_servicio=data.ruta_servicio;
				$scope.titulo=data.titulo;
				$scope.slug_base=data.slug;
				$scope.tservicios=data.tservicios;
				//console.log($scope.tservicios)

			});
		}
		$scope.consultar_algunos_productos = function(){
			productosFactory.asignar_valores("","",$scope.base_url)
			productosFactory.cargar_productos_servicios(function(data){
				
				$scope.algunos_productos=data;
				//console.log($scope.algunos_productos)

			});
		}
		$scope.consultar_algunos_productos();
		$scope.consultar_servicio();
	})