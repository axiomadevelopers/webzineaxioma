angular.module("WebZineApp")
//--Para el preloader
    .directive('footerPage', footerPage)
    .directive('superTrunk', function() { 
        return {
            priority: 10, // adjust this value ;)
            link: function(scope,element,attrs) {
                if($("#idioma").hasClass('esta_espaniol')){
                    texto_valor ="Leer más"
                }else
                    texto_valor = "Read more"
                scope.$watch(attrs.ngBind, function(newvalue) {
                    element.trunk8({
                            lines: 8,
                            fill: '&hellip; <a id="read-more" href="#">'+texto_valor+'</a>' 
                    });
                    //console.log('aaaa')
                });           
            }
        };      
    })
    //.directive('otrosArticulos', otrosArticulos)
    .directive('uploaderModel', ["$parse", function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, iElement, iAttrs) 
            {
                iElement.on("change", function(e)
                {
                    $parse(iAttrs.uploaderModel).assign(scope, iElement[0].files[0]);
                });
            }
        };
    }])
    .directive('loading',   ['$http' ,function ($http)
    {
        return {
            restrict: 'A',
            link: function (scope, elm, attrs)
            {
                scope.isLoading = function () {

                    alert($http.pendingRequests.length)

                    return $http.pendingRequests.length > 0;
                };
               
                scope.$watch(scope.isLoading, function (v)
                {
                    //console.log(elm)
                    if(v){
                        //$("#cabecera,#cuerpo_formulario,#footer").css({"display":"none"});
                        elm.show();
                    }else{
                        //$("#cabecera,#cuerpo_formulario,#footer").css({"display":"block"}); 
                        elm.hide();
                        //$("#cabecera,#cuerpo_formulario,#footer").css({"display":"block"});
                    }
                });
            }
        };

    }]).directive('atPortafolio',function(){
        return {
            restrict: 'E',
            templateUrl: 'assets/web/templates/portfolio.html',
            scope:false,
            link: function (scope, element) {
                if (scope.$last) {
                    init_portfolio();
                    setTimeout(function(){
                        element.parent().isotope('layout');
                    },100);
                }
            }
        };
    }).directive('caTegorias', function(){
        return{
            templateUrl: base_url+'/assets/web/templates/categorias.html',
            scope:false,
            link:function(scope, element){

            }
        }
    }).directive('buscaDor', function(){
        return{
            templateUrl: base_url+'/assets/web/templates/buscador.html',
            scope:false,
            link:function(scope, element){

            }
        }
    }).directive('publiCidad', function(){
        return{
            templateUrl: base_url+'/assets/web/templates/publicidad.html',
            scope:false,
            link:function(scope, element){

            }
        }
    })
    //--
    function footerPage($compile){
        //--
        return {
                    restrict: 'E',
                    transclude: true,
                    scope:{
                           direccion:'@'
                    },
                    templateUrl: "./site_media/templates/footerPage.html",
                    controller:"direccionController",
                    replace:true
        }
        //--
    }
    //--

    //--
    /*function otrosArticulos($compile){
        //--
        return {
                    restrict: 'E',
                    transclude: true,
                    scope:{
                        noticias_objeto:'@'
                    },
                    templateUrl: "./site_media/templates/otrosArticulos.html",
                    controller:"noticiasController",
                    replace:true
        }
        //--
    }*/
    //--
//--------------------------------------